CREATE DATABASE  IF NOT EXISTS `sacsa` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sacsa`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: sacsa
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Abonos`
--

DROP TABLE IF EXISTS `Abonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abonos` (
  `abonoID` bigint(20) NOT NULL,
  `abonoActual` double NOT NULL,
  `abonoAnterior` double NOT NULL,
  `abonoFechaRegistro` date NOT NULL,
  `cuentasporCobrarClientescuenta` bigint(20) NOT NULL,
  PRIMARY KEY (`abonoID`),
  KEY `fk_Abonos_CuentasporCobrar1_idx` (`cuentasporCobrarClientescuenta`),
  CONSTRAINT `fk_Abonos_CuentasporCobrar1` FOREIGN KEY (`cuentasporCobrarClientescuenta`) REFERENCES `CuentasporCobrar` (`clienteNumeroCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abonos`
--

LOCK TABLES `Abonos` WRITE;
/*!40000 ALTER TABLE `Abonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Afiliados`
--

DROP TABLE IF EXISTS `Afiliados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Afiliados` (
  `afiliadoID` bigint(20) NOT NULL,
  `fechaAfiliado` date DEFAULT NULL,
  `fechaRetiro` date DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `personaspersonaID` bigint(20) NOT NULL,
  PRIMARY KEY (`afiliadoID`),
  KEY `fk_Afiliados_Personas1_idx` (`personaspersonaID`),
  CONSTRAINT `fk_Afiliados_Personas1` FOREIGN KEY (`personaspersonaID`) REFERENCES `Personas` (`personaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Afiliados`
--

LOCK TABLES `Afiliados` WRITE;
/*!40000 ALTER TABLE `Afiliados` DISABLE KEYS */;
/*!40000 ALTER TABLE `Afiliados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Clientes`
--

DROP TABLE IF EXISTS `Clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Clientes` (
  `clienteNumeroCuenta` bigint(20) NOT NULL,
  `clienteFechaRegistro` date NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaRetiro` date DEFAULT NULL,
  `personaID` bigint(20) NOT NULL,
  PRIMARY KEY (`clienteNumeroCuenta`),
  KEY `fk_Clientes_Personas1_idx` (`personaID`),
  CONSTRAINT `fk_Clientes_Personas1` FOREIGN KEY (`personaID`) REFERENCES `Personas` (`personaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Clientes`
--

LOCK TABLES `Clientes` WRITE;
/*!40000 ALTER TABLE `Clientes` DISABLE KEYS */;
INSERT INTO `Clientes` VALUES (1,'2014-03-07',1,'2014-03-11',2),(2,'2014-03-06',1,'2014-03-09',4);
/*!40000 ALTER TABLE `Clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Colecturias`
--

DROP TABLE IF EXISTS `Colecturias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Colecturias` (
  `colecturiaID` bigint(20) NOT NULL,
  `cargo` double NOT NULL,
  `pagoPendiente` tinyint(1) NOT NULL,
  `pagoCancelado` date DEFAULT NULL,
  `clienteMDeuda` double NOT NULL,
  `clienteMNumMsPagar` int(11) NOT NULL,
  `registroFactura` date NOT NULL,
  `correlativoFactura` varchar(100) NOT NULL,
  `lecturaID` bigint(20) NOT NULL,
  PRIMARY KEY (`colecturiaID`),
  KEY `fk_Colecturias_Lecturas1_idx` (`lecturaID`),
  CONSTRAINT `fk_Colecturias_Lecturas1` FOREIGN KEY (`lecturaID`) REFERENCES `Lecturas` (`lecturaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Colecturias`
--

LOCK TABLES `Colecturias` WRITE;
/*!40000 ALTER TABLE `Colecturias` DISABLE KEYS */;
/*!40000 ALTER TABLE `Colecturias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contadores`
--

DROP TABLE IF EXISTS `Contadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contadores` (
  `contadorID` bigint(20) NOT NULL,
  `numeroContador` varchar(12) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `cuentasporCobrarclienteCuenta` bigint(20) NOT NULL,
  PRIMARY KEY (`contadorID`),
  KEY `fk_Contadores_CuentasporCobrar1_idx` (`cuentasporCobrarclienteCuenta`),
  CONSTRAINT `fk_Contadores_CuentasporCobrar1` FOREIGN KEY (`cuentasporCobrarclienteCuenta`) REFERENCES `CuentasporCobrar` (`clienteNumeroCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contadores`
--

LOCK TABLES `Contadores` WRITE;
/*!40000 ALTER TABLE `Contadores` DISABLE KEYS */;
/*!40000 ALTER TABLE `Contadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CuentasporCobrar`
--

DROP TABLE IF EXISTS `CuentasporCobrar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CuentasporCobrar` (
  `clienteNumeroCuenta` bigint(20) NOT NULL,
  `numeroCuenta` varchar(12) NOT NULL,
  `estadoCuenta` tinyint(1) NOT NULL,
  `saldo` double NOT NULL,
  `clientesclienteNumeroCuenta` bigint(20) NOT NULL,
  PRIMARY KEY (`clienteNumeroCuenta`),
  KEY `fk_CuentasporCobrar_Clientes1_idx` (`clientesclienteNumeroCuenta`),
  CONSTRAINT `fk_CuentasporCobrar_Clientes1` FOREIGN KEY (`clientesclienteNumeroCuenta`) REFERENCES `Clientes` (`clienteNumeroCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CuentasporCobrar`
--

LOCK TABLES `CuentasporCobrar` WRITE;
/*!40000 ALTER TABLE `CuentasporCobrar` DISABLE KEYS */;
INSERT INTO `CuentasporCobrar` VALUES (11111,'50311111',1,200,1),(22222,'50322222',1,500,2);
/*!40000 ALTER TABLE `CuentasporCobrar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Departamentos`
--

DROP TABLE IF EXISTS `Departamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Departamentos` (
  `deptoID` int(11) NOT NULL,
  `deptoNombre` varchar(100) NOT NULL,
  `paisID` int(11) NOT NULL,
  PRIMARY KEY (`deptoID`),
  KEY `fk_Departamentos_Paises1_idx` (`paisID`),
  CONSTRAINT `fk_Departamentos_Paises1` FOREIGN KEY (`paisID`) REFERENCES `Paises` (`paisID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Departamentos`
--

LOCK TABLES `Departamentos` WRITE;
/*!40000 ALTER TABLE `Departamentos` DISABLE KEYS */;
INSERT INTO `Departamentos` VALUES (1,'San Salvador',1),(2,'San Miguel',1),(3,'Santa Ana',1),(4,'Chalatenango',1),(5,'Sonsonate',1),(6,'Ahuchapan',1),(7,'La Libertad',1);
/*!40000 ALTER TABLE `Departamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DetalleGastos`
--

DROP TABLE IF EXISTS `DetalleGastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DetalleGastos` (
  `gastosID` bigint(20) NOT NULL,
  `tipoDocumentoID` int(11) NOT NULL,
  `numeroDocumento` varchar(150) DEFAULT NULL,
  `emision` date NOT NULL,
  `fechaRegistro` date NOT NULL,
  `proveedor` varchar(100) NOT NULL,
  `debe` double NOT NULL,
  `haber` double NOT NULL,
  PRIMARY KEY (`gastosID`,`tipoDocumentoID`),
  KEY `fk_Gastos_has_TiposDocumento_TiposDocumento1_idx` (`tipoDocumentoID`),
  CONSTRAINT `fk_Gastos_has_TiposDocumento_Gastos1` FOREIGN KEY (`gastosID`) REFERENCES `Gastos` (`gastosID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Gastos_has_TiposDocumento_TiposDocumento1` FOREIGN KEY (`tipoDocumentoID`) REFERENCES `TiposDocumento` (`documentoID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DetalleGastos`
--

LOCK TABLES `DetalleGastos` WRITE;
/*!40000 ALTER TABLE `DetalleGastos` DISABLE KEYS */;
/*!40000 ALTER TABLE `DetalleGastos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DetalleServicios`
--

DROP TABLE IF EXISTS `DetalleServicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DetalleServicios` (
  `cobroID` int(11) NOT NULL,
  `colecturiasLecturaID` bigint(20) NOT NULL,
  `valorCobro` double NOT NULL,
  PRIMARY KEY (`cobroID`,`colecturiasLecturaID`),
  KEY `fk_Lectura_has_Cobros_Cobros1_idx` (`cobroID`),
  KEY `fk_DetalleServicios_Colecturias1_idx` (`colecturiasLecturaID`),
  CONSTRAINT `fk_DetalleServicios_Colecturias1` FOREIGN KEY (`colecturiasLecturaID`) REFERENCES `Colecturias` (`colecturiaID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lectura_has_Cobros_Cobros1` FOREIGN KEY (`cobroID`) REFERENCES `Servicios` (`cobroID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DetalleServicios`
--

LOCK TABLES `DetalleServicios` WRITE;
/*!40000 ALTER TABLE `DetalleServicios` DISABLE KEYS */;
/*!40000 ALTER TABLE `DetalleServicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Direcciones`
--

DROP TABLE IF EXISTS `Direcciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Direcciones` (
  `direccionID` bigint(20) NOT NULL,
  `direccionDir` mediumtext NOT NULL,
  `direccionEstado` tinyint(1) NOT NULL,
  `direccionMunicipio` varchar(200) NOT NULL,
  `municipioID` int(11) NOT NULL,
  `personaID` bigint(20) NOT NULL,
  PRIMARY KEY (`direccionID`),
  KEY `fk_Direcciones_Municipios1_idx` (`municipioID`),
  KEY `fk_Direcciones_Personas1_idx` (`personaID`),
  CONSTRAINT `fk_Direcciones_Municipios1` FOREIGN KEY (`municipioID`) REFERENCES `Municipios` (`municipioID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Direcciones_Personas1` FOREIGN KEY (`personaID`) REFERENCES `Personas` (`personaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Direcciones`
--

LOCK TABLES `Direcciones` WRITE;
/*!40000 ALTER TABLE `Direcciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `Direcciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Directiva`
--

DROP TABLE IF EXISTS `Directiva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Directiva` (
  `idCargo` int(11) NOT NULL,
  `cargo` varchar(25) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`idCargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Directiva`
--

LOCK TABLES `Directiva` WRITE;
/*!40000 ALTER TABLE `Directiva` DISABLE KEYS */;
/*!40000 ALTER TABLE `Directiva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DirectivaAfiliados`
--

DROP TABLE IF EXISTS `DirectivaAfiliados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DirectivaAfiliados` (
  `directivaIDCargo` int(11) NOT NULL,
  `afiliadosafiliadoID` bigint(20) NOT NULL,
  PRIMARY KEY (`directivaIDCargo`,`afiliadosafiliadoID`),
  KEY `fk_Directiva_has_Afiliados_Directiva1_idx` (`directivaIDCargo`),
  KEY `fk_DirectivaAfiliados_Afiliados1_idx` (`afiliadosafiliadoID`),
  CONSTRAINT `fk_DirectivaAfiliados_Afiliados1` FOREIGN KEY (`afiliadosafiliadoID`) REFERENCES `Afiliados` (`afiliadoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Directiva_has_Afiliados_Directiva1` FOREIGN KEY (`directivaIDCargo`) REFERENCES `Directiva` (`idCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DirectivaAfiliados`
--

LOCK TABLES `DirectivaAfiliados` WRITE;
/*!40000 ALTER TABLE `DirectivaAfiliados` DISABLE KEYS */;
/*!40000 ALTER TABLE `DirectivaAfiliados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Gastos`
--

DROP TABLE IF EXISTS `Gastos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gastos` (
  `gastosID` bigint(20) NOT NULL,
  `gastoMensual` double NOT NULL,
  `periodoID` bigint(20) NOT NULL,
  PRIMARY KEY (`gastosID`),
  KEY `fk_Gastos_Periodos1_idx` (`periodoID`),
  CONSTRAINT `fk_Gastos_Periodos1` FOREIGN KEY (`periodoID`) REFERENCES `Periodos` (`periodoID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Gastos`
--

LOCK TABLES `Gastos` WRITE;
/*!40000 ALTER TABLE `Gastos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Gastos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Grupos`
--

DROP TABLE IF EXISTS `Grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grupos` (
  `grupoID` int(11) NOT NULL,
  `grupoNombre` varchar(100) NOT NULL,
  `grupoDescripcion` varchar(255) NOT NULL,
  `grupoEstado` tinyint(1) NOT NULL,
  PRIMARY KEY (`grupoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Grupos`
--

LOCK TABLES `Grupos` WRITE;
/*!40000 ALTER TABLE `Grupos` DISABLE KEYS */;
INSERT INTO `Grupos` VALUES (1,'Administradores','Grupo de administradores',1),(2,'Lectores','Lectore4s',1),(101,'Secretarias','Grupo de secretarios',1);
/*!40000 ALTER TABLE `Grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HistoricoLectura`
--

DROP TABLE IF EXISTS `HistoricoLectura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HistoricoLectura` (
  `periodoID` bigint(20) NOT NULL,
  `periodoInicio` date NOT NULL,
  `periodoFinal` date NOT NULL,
  `lecturaAnterior` double NOT NULL,
  `lecturaActual` double NOT NULL,
  `CorrelativoFactura` varchar(100) NOT NULL,
  `clienteMPeriodoID` int(11) NOT NULL,
  `clienteMDeuda` double NOT NULL,
  `clienteMAplicaCorte` tinyint(1) NOT NULL,
  `clienteMNumMsPagar` int(11) NOT NULL,
  `saldo` double DEFAULT NULL,
  `fechaLectura` date DEFAULT NULL,
  `abono` double NOT NULL,
  `cargo` double NOT NULL,
  `pagoCancelado` date DEFAULT NULL,
  `clienteNumCuenta` bigint(20) NOT NULL,
  `numCuenta` bigint(20) NOT NULL,
  `lecturaID` bigint(20) NOT NULL,
  PRIMARY KEY (`periodoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HistoricoLectura`
--

LOCK TABLES `HistoricoLectura` WRITE;
/*!40000 ALTER TABLE `HistoricoLectura` DISABLE KEYS */;
/*!40000 ALTER TABLE `HistoricoLectura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingresos`
--

DROP TABLE IF EXISTS `Ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ingresos` (
  `ingresoID` bigint(20) NOT NULL,
  `totalMensual` double NOT NULL,
  `periodoID` bigint(20) NOT NULL,
  PRIMARY KEY (`ingresoID`),
  KEY `fk_Ingresos_Periodos1_idx` (`periodoID`),
  CONSTRAINT `fk_Ingresos_Periodos1` FOREIGN KEY (`periodoID`) REFERENCES `Periodos` (`periodoID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingresos`
--

LOCK TABLES `Ingresos` WRITE;
/*!40000 ALTER TABLE `Ingresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lecturas`
--

DROP TABLE IF EXISTS `Lecturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lecturas` (
  `lecturaID` bigint(20) NOT NULL,
  `fechaLectura` date DEFAULT NULL,
  `valorLectura` int(11) DEFAULT NULL,
  `periodoInicio` date NOT NULL,
  `periodoFinal` date NOT NULL,
  `iDLector` int(11) NOT NULL,
  `clienteCuenta` bigint(20) NOT NULL,
  PRIMARY KEY (`lecturaID`),
  KEY `fk_Lecturas_CuentasporCobrar1_idx` (`clienteCuenta`),
  CONSTRAINT `fk_Lecturas_CuentasporCobrar1` FOREIGN KEY (`clienteCuenta`) REFERENCES `CuentasporCobrar` (`clienteNumeroCuenta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lecturas`
--

LOCK TABLES `Lecturas` WRITE;
/*!40000 ALTER TABLE `Lecturas` DISABLE KEYS */;
INSERT INTO `Lecturas` VALUES (1,'2015-01-15',15,'2015-01-01','2015-01-30',1,11111),(2,'2015-02-15',10,'2015-02-01','2015-02-28',1,11111);
/*!40000 ALTER TABLE `Lecturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Modificaciones`
--

DROP TABLE IF EXISTS `Modificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modificaciones` (
  `modID` bigint(20) NOT NULL,
  `modTabla` varchar(80) NOT NULL,
  `modFechaActualizacion` date NOT NULL,
  `modUsuarioEditor` int(11) NOT NULL,
  `modDescripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`modID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Modificaciones`
--

LOCK TABLES `Modificaciones` WRITE;
/*!40000 ALTER TABLE `Modificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `Modificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Municipios`
--

DROP TABLE IF EXISTS `Municipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Municipios` (
  `municipioID` int(11) NOT NULL,
  `muniNombre` varchar(45) NOT NULL,
  `deptoID` int(11) NOT NULL,
  PRIMARY KEY (`municipioID`),
  KEY `fk_Municipios_Departamentos1_idx` (`deptoID`),
  CONSTRAINT `fk_Municipios_Departamentos1` FOREIGN KEY (`deptoID`) REFERENCES `Departamentos` (`deptoID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Municipios`
--

LOCK TABLES `Municipios` WRITE;
/*!40000 ALTER TABLE `Municipios` DISABLE KEYS */;
INSERT INTO `Municipios` VALUES (1,'San Salvador',1),(2,'Soyapango',1),(3,'Cuscatancingo',1),(4,'Apopa',1);
/*!40000 ALTER TABLE `Municipios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Paises`
--

DROP TABLE IF EXISTS `Paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Paises` (
  `paisID` int(11) NOT NULL,
  `paisNombre` varchar(100) NOT NULL,
  `paisMoneda` varchar(90) NOT NULL,
  `paisMonedaSimbolo` varchar(8) NOT NULL,
  PRIMARY KEY (`paisID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Paises`
--

LOCK TABLES `Paises` WRITE;
/*!40000 ALTER TABLE `Paises` DISABLE KEYS */;
INSERT INTO `Paises` VALUES (1,'El Salvador','dolar','$');
/*!40000 ALTER TABLE `Paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Parametros`
--

DROP TABLE IF EXISTS `Parametros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parametros` (
  `parametroID` int(11) NOT NULL,
  `InicioLectura` int(11) NOT NULL,
  `FinLectura` int(11) NOT NULL,
  `cuotaBase` double NOT NULL,
  `nombreInstitucion` varchar(100) NOT NULL,
  `fechaSistema` date NOT NULL,
  `fechaAperturaSistema` date NOT NULL,
  `limitedeConsumo` int(11) NOT NULL,
  `valorIncremento` double NOT NULL,
  `diaLimitePago` int(11) NOT NULL,
  `numFactura` varchar(45) NOT NULL,
  PRIMARY KEY (`parametroID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Parametros`
--

LOCK TABLES `Parametros` WRITE;
/*!40000 ALTER TABLE `Parametros` DISABLE KEYS */;
INSERT INTO `Parametros` VALUES (1,10,30,3,'ADESCOCERR','2015-11-06','2015-11-06',15,5,30,'4');
/*!40000 ALTER TABLE `Parametros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Periodos`
--

DROP TABLE IF EXISTS `Periodos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Periodos` (
  `periodoID` bigint(20) NOT NULL,
  `mes` int(11) NOT NULL,
  `anyo` int(11) NOT NULL,
  `Saldo` double NOT NULL,
  PRIMARY KEY (`periodoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Periodos`
--

LOCK TABLES `Periodos` WRITE;
/*!40000 ALTER TABLE `Periodos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Periodos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Personas`
--

DROP TABLE IF EXISTS `Personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Personas` (
  `personaID` bigint(20) NOT NULL,
  `nombre1` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `personaFechNacimiento` date NOT NULL,
  `personaCorreo` varchar(155) DEFAULT NULL,
  `personaFechaRegistro` date NOT NULL,
  `personaEstado` tinyint(1) NOT NULL,
  `idDocumento` varchar(9) NOT NULL,
  `nombre2` varchar(100) DEFAULT NULL,
  `apellido2` varchar(100) DEFAULT NULL,
  `estadoCivil` varchar(25) NOT NULL,
  `genero` varchar(1) DEFAULT NULL,
  `fechaBaja` date DEFAULT NULL,
  PRIMARY KEY (`personaID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Personas`
--

LOCK TABLES `Personas` WRITE;
/*!40000 ALTER TABLE `Personas` DISABLE KEYS */;
INSERT INTO `Personas` VALUES (1,'Guillermo','Castaneda','1990-01-01','guillermo@gmail.com','2000-01-01',1,'1111111-1','Francisco','Salazar','S','M',NULL),(2,'Diana','Cruz','1990-01-02','diana@gmail.com','2000-01-02',1,'222222222','Raquel','Umanzor','S','F',NULL),(3,'Rafael','Paniagua','1980-01-01','rafael@gmail.com','2000-01-03',1,'333333333','Eduardo','Moreno','C','M',NULL),(4,'Juan','Escobar','1980-01-02','juan@gmail.com','2000-01-04',1,'444444444','Miguel','Alfaro','C','M',NULL),(5,'Daniel','Guevara','1995-01-01','daniel@gmail.com','2000-01-05',1,'555555555','Enrique','Gómez','C','M',NULL);
/*!40000 ALTER TABLE `Personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Secuencias`
--

DROP TABLE IF EXISTS `Secuencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Secuencias` (
  `nombreSeq` varchar(80) NOT NULL,
  `valorSeq` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`nombreSeq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Secuencias`
--

LOCK TABLES `Secuencias` WRITE;
/*!40000 ALTER TABLE `Secuencias` DISABLE KEYS */;
INSERT INTO `Secuencias` VALUES ('AbonosSecuencia',100),('AfiliadosSecuencia',100),('ClientesSecuencia',100),('ColecturiasSecuencia',100),('ContadoresSecuencia',100),('CuentasCobrarSecuencia',100),('DeptosSecuencia',100),('DireccionesSecuencia',100),('DirectivasSecuencia',100),('GastosSecuencia',100),('GruposSecuencia',150),('HistoricoLectSecuencia',100),('IngresosSecuencia',100),('LecturasSecuencia',100),('ModificacionesSecuencia',100),('MunicipiosSecuencia',100),('PaisesSecuencia',100),('ParametrosSecuencia',100),('PeriodosSecuencia',100),('PersonasSecuencia',100),('ServiciosSecuencia',100),('TelefonosSecuencia',100),('TiposDocsSecuencia',100),('UsuariosSecuencia',100);
/*!40000 ALTER TABLE `Secuencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Servicios`
--

DROP TABLE IF EXISTS `Servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Servicios` (
  `cobroID` int(11) NOT NULL,
  `cobroTipo` varchar(60) NOT NULL,
  `cobroDescripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cobroID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Servicios`
--

LOCK TABLES `Servicios` WRITE;
/*!40000 ALTER TABLE `Servicios` DISABLE KEYS */;
/*!40000 ALTER TABLE `Servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Telefonos`
--

DROP TABLE IF EXISTS `Telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Telefonos` (
  `telID` bigint(20) NOT NULL,
  `telNumero` char(9) NOT NULL,
  `telEstado` tinyint(1) NOT NULL,
  `personaID` bigint(20) NOT NULL,
  PRIMARY KEY (`telID`),
  KEY `fk_Telefonos_Personas1_idx` (`personaID`),
  CONSTRAINT `fk_Telefonos_Personas1` FOREIGN KEY (`personaID`) REFERENCES `Personas` (`personaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Telefonos`
--

LOCK TABLES `Telefonos` WRITE;
/*!40000 ALTER TABLE `Telefonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `Telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TiposDocumento`
--

DROP TABLE IF EXISTS `TiposDocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TiposDocumento` (
  `documentoID` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`documentoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TiposDocumento`
--

LOCK TABLES `TiposDocumento` WRITE;
/*!40000 ALTER TABLE `TiposDocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `TiposDocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuarios`
--

DROP TABLE IF EXISTS `Usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuarios` (
  `usuarioID` bigint(20) NOT NULL,
  `usuarioNombre` varchar(40) NOT NULL,
  `usuarioPassword` varchar(255) NOT NULL,
  `usuarioEstado` tinyint(1) NOT NULL,
  `grupoID` int(11) NOT NULL,
  `Personas_personaID` bigint(20) NOT NULL,
  PRIMARY KEY (`usuarioID`),
  KEY `fk_Usuarios_Grupos1_idx` (`grupoID`),
  KEY `fk_Usuarios_Personas1_idx` (`Personas_personaID`),
  CONSTRAINT `fk_Usuarios_Grupos1` FOREIGN KEY (`grupoID`) REFERENCES `Grupos` (`grupoID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_Personas1` FOREIGN KEY (`Personas_personaID`) REFERENCES `Personas` (`personaID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuarios`
--

LOCK TABLES `Usuarios` WRITE;
/*!40000 ALTER TABLE `Usuarios` DISABLE KEYS */;
INSERT INTO `Usuarios` VALUES (1,'deguevara','deguevara',1,1,1),(2,'user2','useer2',1,2,3),(3,'deguevara','12345',1,2,4);
/*!40000 ALTER TABLE `Usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-06 21:18:03

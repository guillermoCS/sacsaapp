/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Abono;
import org.sacsaapp.entities.CuentaPorCobrar;
import org.sacsaapp.entities.Persona;
import org.sacsaapp.entities.Usuario;
import org.sacsaapp.interfaces.AbonoFacadeLocal;

/**
 *
 * @author Diana
 */
@ManagedBean
@ViewScoped
public class AbonoMB implements Serializable {

    @EJB
    private AbonoFacadeLocal abonoFacade;
    private Abono a;
    private Abono abonoAgregar;
    private List<Abono> lsabo;
    private Date fechAbActual;
    private List<Abono> abonosAnteriores;
    private List<Abono> abonoActual;
    private List<Abono> numeroDeCuenta;

    public List<Abono> getNumeroDeCuenta() {
        return numeroDeCuenta;
    }

    public void setNumeroDeCuenta(List<Abono> numeroDeCuenta) {
        this.numeroDeCuenta = numeroDeCuenta;
    }

    public AbonoMB() {
    }

    @PostConstruct
    public void init() {
                a = new Abono();

    
        abonoAgregar = new Abono();
        lsabo = new ArrayList<>();
        abonosAnteriores = new ArrayList<>();
        llenarLista();
        // obtenerAbonosAnteriores();
    }

    public String filtrarPorNumeroCuenta() {
        abonoFacade.filtrarPorNumeroCuenta(a);
        return "Abonos/Abonos";
    }

    public void llenarLista() {
        System.out.println("cargando lista");
        lsabo = abonoFacade.findAll();
        System.out.println("lista: " + lsabo.size());

    }

    public void obtenerListas() {
        System.out.println("Entramos a cargar Lista !! ");
        lsabo = abonoFacade.findAll();
        System.out.println(" persona: " + lsabo.size());
    }

    /*public void agregarAbonos() {

        abonoAgregar.setAbonoAnterior(5.00);
        //CuentaPorCobrar cta=new  CuentaPorCobrar(Long.parseLong(String.valueOf(2233)));
        //abonoAgregar.setCuentasporCobrarClientescuenta(cta);
        abonoFacade.create(abonoAgregar);
        abonoAgregar = new Abono();
        a = new Abono();
        llenarLista();
        FacesMessage msg = new FacesMessage("Éxito", "Abono Agregado satisfactoriamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        // RequestContext context = RequestContext.getCurrentInstance();
        //
        
      */  
    public void AddAbonos() {
      abonoFacade.create(a);
    }

    public void obtenerAbonosAnteriores() {

        Date fechaActual = new Date();
        a.setAbonoFechaRegistro(fechaActual);
        abonoFacade.setNumQuery(5);
        abonosAnteriores = abonoFacade.findwithParameters(a);

    }

    public Double obtenerAbonoActual(Long numCuenta, Double valorIngresado) {
        abonoFacade.setNumQuery(6);
        Abono abonoBuscar = new Abono();
        CuentaPorCobrar cta = new CuentaPorCobrar();
        cta.setClienteNumeroCuenta(numCuenta);
        abonoBuscar.setCuentasporCobrarClientescuenta(cta);

        Abono abono = abonoFacade.findOnewithParameters(abonoBuscar);

        Double nuevoAbono = valorIngresado + abono.getAbonoActual();
        System.out.println("El abono actual a enviar: " + nuevoAbono);
//        Abono abonoNuevo = new Abono(numCuenta, nuevoAbono, abono.getAbonoActual(), new Date());
//        abonoFacade.create(abonoNuevo);

        return nuevoAbono;

    }

    /*public void agregarAbonos(){
        
     Date fechaActual  = new Date();
     a.setAbonoFechaRegistro(fechaActual);
    
     abonoFacade.create(a);
     a.setAbonoAnterior(abonoAnterior);
     abonoFacade.setNumQuery(3);
     List<Abono>lst= abonoFacade.findwithParameters(a);
     Double totalAnteriores=0.00;
     for (Abono lst1 : lst) {
     totalAnteriores=totalAnteriores+lst1.getAbonoAnterior();
     }

     }*/
    /**
     * @return the abonoAgregar
     */
    public List<Abono> getAbonoActual() {
        return abonoActual;
    }

    public void setAbonoActual(List<Abono> abonoActual) {
        this.abonoActual = abonoActual;
    }

    public List<Abono> getAbonosAnteriores() {
        return abonosAnteriores;
    }

    public void setAbonosAnteriores(List<Abono> abonosAnteriores) {
        this.abonosAnteriores = abonosAnteriores;
    }

    public List<Abono> getLsabo() {
        return lsabo;
    }

    public void setLsabo(List<Abono> lsabo) {
        this.lsabo = lsabo;
    }

    public Abono getAbonoAgregar() {
        return abonoAgregar;
    }

    /**
     * @param abonoAgregar the abonoAgregar to set
     */
    public void setAbonoAgregar(Abono abonoAgregar) {
        this.abonoAgregar = abonoAgregar;
    }

}

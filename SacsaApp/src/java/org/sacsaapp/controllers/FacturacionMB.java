/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.sacsaapp.entities.Colecturia;
import org.sacsaapp.entities.DetalleServicio;
import org.sacsaapp.entities.DetalleServicioPK;
import org.sacsaapp.entities.HistoricoLectura;
import org.sacsaapp.entities.Lectura;
import org.sacsaapp.entities.Parametro;
import org.sacsaapp.entities.Servicio;
import org.sacsaapp.interfaces.ColecturiaFacadeLocal;
import org.sacsaapp.interfaces.DetalleServicioFacadeLocal;
import org.sacsaapp.interfaces.HistoricoLecturaFacadeLocal;
import org.sacsaapp.interfaces.LecturaFacadeLocal;
import org.sacsaapp.interfaces.ParametroFacadeLocal;
import org.sacsaapp.interfaces.ServicioFacadeLocal;
import org.sacsaapp.procesos.ProcesoFacturacionAutoLocal;

/**
 *
 * @author guillermo
 */
@ManagedBean
@ApplicationScoped
public class FacturacionMB {
    
    @EJB
    private ParametroFacadeLocal paramsF;
    
    @EJB 
    private ProcesoFacturacionAutoLocal procesoAutoFl;
            
    private int horaInicio;
    private int diaEjecucion;
    
    /**
     * Creates a new instance of FacturacionMB
     */
    public FacturacionMB() {
    }
    
    public void iniciarProcesoAutomatico(){
        if(horaInicio == 24){
            horaInicio = 0;
        }
        System.out.println("Vals: " + horaInicio + " " + diaEjecucion);
        
        Parametro p = paramsF.find(1);
        int diaMes = p.getLimitedeConsumo();
        procesoAutoFl.setDiaDelMes(diaEjecucion);
        procesoAutoFl.setHoraInicio(horaInicio);
        System.out.println("Iniciando Proceso");
        procesoAutoFl.programarProceso();
        this.aplicaEventosPagina("Éxito", "Proceso Iniciado", null, null);
    }  
    
    public void detenerProcesoAutomatico(){
        if (procesoAutoFl.terminarProceso("ProcesoCobrosFacturas")){
            System.out.println("Terminando Proceso");
            this.aplicaEventosPagina("Éxito", "Proceso Terminado", null, null);
        }
    }    
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        org.primefaces.context.RequestContext context = org.primefaces.context.RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getDiaEjecucion() {
        return diaEjecucion;
    }

    public void setDiaEjecucion(int diaEjecucion) {
        this.diaEjecucion = diaEjecucion;
    }
    
}

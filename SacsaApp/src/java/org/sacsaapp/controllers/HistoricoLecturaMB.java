/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.sacsaapp.entities.Cliente;
import org.sacsaapp.entities.HistoricoLectura;
import org.sacsaapp.interfaces.ClienteFacadeLocal;
import org.sacsaapp.interfaces.HistoricoLecturaFacadeLocal;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import java.io.IOException;

/**
 *
 * @author deguevara
 * @date 11-22-2015
 * @time 07:17:29 PM
 */
@ManagedBean
@ViewScoped
public class HistoricoLecturaMB {

    @EJB
    private HistoricoLecturaFacadeLocal historicoFacade;

    @EJB
    private ClienteFacadeLocal clienteFacade;
    private List<HistoricoLectura> listaHistorico;

    private HistoricoLectura historicoLectura;
    private Cliente clienteLecturas;

    private Date desde;
    private Date hasta;
    private boolean mostrarInfoCliente;

    public HistoricoLecturaMB() {
    }

    @PostConstruct
    public void iniciar() {
        System.out.println("Iniciando");
        listaHistorico = new ArrayList<>();
        historicoLectura = new HistoricoLectura();
        mostrarInfoCliente = false;
        desde = new Date();
        hasta = new Date();
    }

    public void llenarLista() {
        listaHistorico = historicoFacade.findAll();
        System.out.println("se lleno la lista : " + listaHistorico.size());
    }

    public void filtrarPorNumerDeCuenta() {
        System.out.println("filtrar por " + historicoLectura.getNumCuenta());
        HistoricoLectura historicoLecturaFiltro = new HistoricoLectura();
        historicoFacade.setNumQuery(5);
        ArrayList<String> arrayOne = new ArrayList();
        arrayOne.add("numCuenta");
        arrayOne.add("desde");
        arrayOne.add("hasta");
        // Lista de valores a poner en la named Query
        ArrayList arrayTwo = new ArrayList();
        arrayTwo.add(historicoLectura.getNumCuenta());
        arrayTwo.add(desde);
        arrayTwo.add(hasta);
        System.out.println("array " + arrayTwo);
        System.out.println("array " + arrayOne);
        listaHistorico = historicoFacade.findwithParameters(historicoLecturaFiltro, arrayOne, arrayTwo);
        System.out.println("Total de historicos: " + listaHistorico.size());
        if (listaHistorico.size() > 0) {
            HistoricoLectura historico = new HistoricoLectura();
            historico = listaHistorico.get(0);
            System.out.println("Cliente a buscar: " + historico.getClienteNumCuenta());
            clienteLecturas = clienteFacade.find(historico.getClienteNumCuenta());
            mostrarInfoCliente = true;
        }

        System.out.println("se lleno la lista : " + listaHistorico.size());
    }

    public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.open();
        pdf.setPageSize(PageSize.A4);
//            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
//        String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "demo" + File.separator + "images" + File.separator + "prime_logo.png";
        pdf.addTitle("Listado de Historico");
//        pdf.add(Image.getInstance(logo));
    }

    public List<HistoricoLectura> getListaHistorico() {
        return listaHistorico;
    }

    public void setListaHistorico(List<HistoricoLectura> listaHistorico) {
        this.listaHistorico = listaHistorico;
    }

    public HistoricoLectura getHistoricoLectura() {
        return historicoLectura;
    }

    public void setHistoricoLectura(HistoricoLectura historicoLectura) {
        this.historicoLectura = historicoLectura;
    }

    public Cliente getClienteLecturas() {
        return clienteLecturas;
    }

    public void setClienteLecturas(Cliente clienteLecturas) {
        this.clienteLecturas = clienteLecturas;
    }

    public boolean isMostrarInfoCliente() {
        return mostrarInfoCliente;
    }

    public void setMostrarInfoCliente(boolean mostrarInfoCliente) {
        this.mostrarInfoCliente = mostrarInfoCliente;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

}

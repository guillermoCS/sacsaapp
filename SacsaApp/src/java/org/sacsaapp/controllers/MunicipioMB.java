/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.jboss.weld.context.RequestContext;
import org.sacsaapp.entities.Departamento;
import org.sacsaapp.entities.Municipio;
import org.sacsaapp.entities.Pais;
import org.sacsaapp.interfaces.DepartamentoFacadeLocal;
import org.sacsaapp.interfaces.MunicipioFacadeLocal;
import org.sacsaapp.interfaces.PaisFacadeLocal;


/**
 *
 * @author merlin
 */
@ManagedBean
@ViewScoped
public class MunicipioMB implements Serializable {
    @EJB
    private MunicipioFacadeLocal pf;
    
    @EJB
    private DepartamentoFacadeLocal df;
    
    @EJB
    private PaisFacadeLocal paisf;
    
    private int depto;
    
    private Municipio p;
    private Municipio pSeleccionado;
//    private Pais pSeleccionadaM;
    private List<Municipio> lsp;
    private List<Departamento> lp;
    private List<Pais> lpaises;
    
    private int idPaisSeleccionado;
    private boolean mostrarDeptos;
    
    /**
     * Creates a new instance of PaisMB
     */
    public MunicipioMB() {
    }
    
    @PostConstruct
    public void init() {
        p = new Municipio();
        pSeleccionado = new Municipio();
//        pSeleccionadaM = new Pais();
        lsp = new ArrayList<>();
        llenarLista();
    }
    
     public void llenarLista() {
        lsp = pf.findAll();
    }
     
      public void agregarMunicipio() {
        Departamento d = new Departamento();
        d.setDeptoID(depto); // se tea valores del la entidad departamento
        p.setDeptoID(d);// se tea valores de de municipio en objetos
        pf.create(p);
        p = new Municipio();
        llenarLista();

        this.aplicaEventosPagina("Éxito", "Municipio Agregado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialog').hide();");
        this.aplicaEventosPagina(null, null, "dialogdd", null);
        this.aplicaEventosPagina(null, null, "frmAdd:selectGrupo", null);
    }
      
      public void modificarMunicipio(Municipio usVista) {
        depto = usVista.getDeptoID().getDeptoID();
        idPaisSeleccionado = usVista.getDeptoID().getPaisID().getPaisID();
        pSeleccionado = new Municipio();
        pSeleccionado = usVista;
//        habilitarCambioPass = false;
//        habilitarCambioPasswd();
//                
//        this.lsgp = this.obtenerListaGruposMod();
          obtenerDeptoPorPais(true);
       this.aplicaEventosPagina(null, null, "frmMod:usDetailMod", "PF('usDialogMod').show();");
       
    }
    
    public void aplicarCambiosMunicipio(){
//        System.out.println("usuario: " + pSeleccionado.getPaisNombre());
//        try {
//            // Verificamos si se ha cambiado el grupo para el usuario.
//            if(usSeleccionado.getGrupoID().getGrupoID() != Integer.valueOf(idGrupo)){
//                grupoSeleccionado.setGrupoID(Integer.valueOf(idGrupo));
//                usSeleccionado.setGrupoID(grupoSeleccionado);
//            }
            // verificamos si se quiere cambiar la contraseña del usuario.
//            if (!habilitado){                
//                if(this.validarPasswordMod()){                    
//                    usl.edit(usSeleccionado); 
//                    idGrupo = "0";
//                    llenarLista();
//                    this.aplicaEventosPagina("Éxito", "Usuario Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
//                    
//                }
//            }else{
        Departamento d = new Departamento();
        d.setDeptoID(depto); // se tea valores del la entidad departamento
        pSeleccionado.setDeptoID(d);
            
                pf.edit(pSeleccionado); 
//                idGrupo = "0";
                llenarLista();
                this.aplicaEventosPagina("Éxito", "Municipio Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
//            }
            
            
//        } catch (Exception e) {
//            e.printStackTrace();
//        }       

    }

    public void eliminarMunicipio(Municipio usVista) {
        System.out.println("Entramos a Eliminar ");
        System.out.println("usVista: " + usVista.getMuniNombre());
//        try {
//            usVista.setUsuarioEstado(false);
            pf.remove(usVista);
            this.llenarLista();
            this.aplicaEventosPagina("Éxito", "Municipio Eliminado satisfactoriamente", "frmUs:tabUsuarios", null);
            
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    
    public void reestablecerFormato(){
        obtenerDeptoPorPais(false);
        lp = df.findAll();
        mostrarDeptos = true;
        p = new Municipio();
        idPaisSeleccionado = 0;
        depto = 0;
        lpaises = new ArrayList<>();
        lpaises = paisf.findAll();
                
        this.aplicaEventosPagina(null, null, "frmAdd:usDetail", null); 
    }
    
    
    public void obtenerDeptoPorPais(boolean esModificacion){
        System.out.println("id Pais: " + idPaisSeleccionado);
        if(!esModificacion){
            depto = 0;                       
        }
        
        Pais p = new Pais();
        p.setPaisID(idPaisSeleccionado);
        Departamento dept = new Departamento();
        dept.setPaisID(p);
        df.setNumQuery(2);
        lp = new ArrayList<>();
        lp = df.findwithParameters(dept);
        mostrarDeptos = false;
        
        System.out.println("Lista depto Size: " + lp.size());
        
        this.aplicaEventosPagina(null, null, "frmAdd:depto", null);
    }

    /*
    *   Método para obtener la lista de usuarios que tengan un estado = true
    *   así se muestra como si los usarios con estado false han sido borrados.
    */
//    public void obtenerListas() {
//        
//        lsp = gpl.findAll();
//        lspersona = pl.findByNameQuery("Persona.findByUsuario");
//        
//    }
    
//    public List<Grupo> obtenerListaGruposMod(){
//        List<Grupo> lsgpMod = new ArrayList<>();
//        int valorPosicion = 0;
//        Grupo grupoPosicionA = new Grupo();
//        Grupo gpSeleccionado = new Grupo();
//        
//        
//        lsgpMod = gpl.findAll();
//        grupoPosicionA = lsgpMod.get(0);
//        gpSeleccionado = usSeleccionado.getGrupoID();        
//        
//        /*
//        *   Recorremos la lista de grupos para saber la posición del grupo al que el usuario pertenece.
//        *   Si está en primer lugar dejamos la lista igual.
//        *   Si está en otra posición guardamos el valor de esa posción en una variable (valorPosicion).
//        */
//        for (int i = 0; i < lsgpMod.size(); i++) {
//            
//            if (i == 0 && lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
//                break;
//            }
//            if (lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
//                valorPosicion = i;
//            }
//        }
//        // validamos que el valor de la posicion del objeto buscado sea distinta de la posicion inicial.
//        if(!(valorPosicion == 0)){
//                                  
//            lsgpMod.remove(valorPosicion); // removemos el objeto duplicado.
//            lsgpMod.add(grupoPosicionA); // agregamos el objeto de la posicion inicial a la posicion final de la lista.
//            lsgpMod.set(0, gpSeleccionado); // modificamos el objeto de la posicion inicial para setear el objeto que relaciona al grupo con el usuario.
//        }
//               
//        return lsgpMod;
//    }
    
//    public boolean validarPassword(){        
//        System.out.println("campo comprobacion: " + this.passMatch);        
//        if (this.passMatch==null || this.passMatch.trim().equals("")){
//
//            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, "PF('usDialog').show();");
//            
//        }else if(us.getUsuarioPassword().trim().equals(passMatch)){
//            return true;
//            
//        }else{
//            System.out.println("Contraseñas no coinciden !!");
//            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, "PF('usDialog').show();");
//        }
//        return false;        
//    }
//    
//    public boolean validarPasswordMod(){        
//        System.out.println("campo comprobacion: " + this.passMatch);        
//        if (this.passMatch==null || this.passMatch.trim().equals("")){
//
//            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, null);
//            
//        }else if(usSeleccionado.getUsuarioPassword().trim().equals(passMatch)){
//            return true;
//            
//        }else{
//            System.out.println("Contraseñas no coinciden !!");
//            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, null);
//        }
//        return false;        
//    }
//    
//    /*
//    *   Método para validar que tanto el SelectOneMenu de Grupo como el de Persona hayan sido cargadas con valores.
//    *   Retorna un boolean (true = han sido cargados | false = no han sido cargados)
//    */
//    public boolean validarGrupoSelects(){
//        
//        if(this.idGrupo.trim().equals("0")){ 
//            this.aplicaEventosPagina("Campo grupo", "El campo grupo es requerido", null, "PF('usDialog').show();");
//            
//        }else{
//            return true;
//        }
//        
//        return false;
//    }
//    public boolean validarPersonaSelects(){
//        
//        if(this.idPersona.trim().equals("0")){
//            this.aplicaEventosPagina("Campo Persona", "El campo persona es requerido", null, "PF('usDialog').show();");
//        }else{
//            return true;
//        }
//        
//        return false;
//    }
    
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        org.primefaces.context.RequestContext context = org.primefaces.context.RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
}
    
//    public void habilitarCambioPasswd(){
//        if (habilitarCambioPass){
//            habilitado = false;
//        }else{
//            habilitado = true;
//        }
//    }

    public Municipio getP() {
        return p;
    }

    public void setP(Municipio p) {
        this.p = p;
    }

    public List<Municipio> getLsp() {
        return lsp;
    }

    public void setLsp(List<Municipio> lsp) {
        this.lsp = lsp;
    }

    public Municipio getPSeleccionado() {
        return pSeleccionado;
    }

    public void setPSeleccionado(Municipio pSeleccionado) {
        this.pSeleccionado = pSeleccionado;
    }

//    public List<Grupo> getLsgp() {
//        return lsgp;
//    }
//
//    public void setLsgp(List<Grupo> lsgp) {
//        this.lsgp = lsgp;
//    }

//    public List<Persona> getLspersona() {
//        return lspersona;
//    }
//
//    public void setLspersona(List<Persona> lspersona) {
//        this.lspersona = lspersona;
//    }

//    public Grupo getIdGrupoSeleccionado() {
//        return grupoSeleccionado;
//    }
//
//    public void setIdGrupoSeleccionado(Grupo idGrupo) {
//        this.grupoSeleccionado = idGrupo;
//    }
//
//    public Persona getIdPersonaSeleccionada() {
//        return personaSeleccionada;
//    }
//
//    public void setIdPersonaSeleccionada(Persona idPersona) {
//        this.personaSeleccionada = idPersona;
//    }

//    public String getIdGrupo() {
//        if(idGrupo == null){
//            setIdGrupo(new String());
//        }
//        return idGrupo;
//    }

//    public void setIdGrupo(String idGrupo) {
//        this.idGrupo = idGrupo;
//    }
//
//    public String getIdPersona() {
//        return idPersona;
//    }
//
//    public void setIdPersona(String idPersona) {
//        this.idPersona = idPersona;
//    }

//    public String getPassMatch() {
//        return passMatch;
//    }
//
//    public void setPassMatch(String passMatch) {
//        this.passMatch = passMatch;
//    }
//
//    public boolean isHabilitarCambioPass() {
//        return habilitarCambioPass;
//    }

//    public void setHabilitarCambioPass(boolean habilitarCambioPass) {
//        this.habilitarCambioPass = habilitarCambioPass;
//    }
//
//    public boolean isHabilitado() {
//        return habilitado;
//    }
//
//    public void setHabilitado(boolean habilitado) {
//        this.habilitado = habilitado;
//    }

    public int getDepto() {
        return depto;
    }

    public void setDepto(int depto) {
        this.depto = depto;
    }

    public List<Departamento> getLp() {
        return lp;
    }

    public void setLp(List<Departamento> lp) {
        this.lp = lp;
    }

    public int getIdPaisSeleccionado() {
        return idPaisSeleccionado;
    }

    public void setIdPaisSeleccionado(int idPaisSeleccionado) {
        this.idPaisSeleccionado = idPaisSeleccionado;
    }

    public List<Pais> getLpaises() {
        return lpaises;
    }

    public void setLpaises(List<Pais> lpaises) {
        this.lpaises = lpaises;
    }

    public boolean isMostrarDeptos() {
        return mostrarDeptos;
    }

    public void setMostrarDeptos(boolean mostrarDeptos) {
        this.mostrarDeptos = mostrarDeptos;
    }
    
    
    
}

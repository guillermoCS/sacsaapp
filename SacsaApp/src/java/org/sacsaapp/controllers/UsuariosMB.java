/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.sound.midi.Soundbank;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Grupo;
import org.sacsaapp.entities.Persona;
import org.sacsaapp.entities.Usuario;
import org.sacsaapp.interfaces.GrupoFacadeLocal;
import org.sacsaapp.interfaces.PersonaFacadeLocal;
import org.sacsaapp.interfaces.UsuarioFacadeLocal;

/**
 *
 * @author guillermo
 */
@ManagedBean(name = "usuariosMB")
@ViewScoped
public class UsuariosMB implements Serializable {

    @EJB
    private UsuarioFacadeLocal usl;
    @EJB
    private GrupoFacadeLocal gpl;
    @EJB
    private PersonaFacadeLocal pl;

    private Usuario us;
    private List<Usuario> lsus;
    private Usuario usSeleccionado;
    private List<Grupo> lsgp;
    private List<Persona> lspersona;

    private Grupo grupoSeleccionado;
    private Persona personaSeleccionada;

    private String idGrupo;
    private String idPersona;
    
    private String passMatch;
    private boolean habilitarCambioPass;
    private boolean habilitado;

    /**
     * Creates a new instance of UsuariosMB
     */
    public UsuariosMB() {
    }

    @PostConstruct
    public void init() {
        us = new Usuario();
        lsus = new ArrayList<>();
        lsgp = new ArrayList<>();
        lspersona = new ArrayList<>();
        usSeleccionado = new Usuario();
        grupoSeleccionado = new Grupo();
        personaSeleccionada = new Persona();
        llenarLista();
        habilitarCambioPass = false;
        habilitado = true;
    }

    public void llenarLista() {
        usl.setNumQuery(4);
        Usuario usStatus = new Usuario();
        usStatus.setUsuarioEstado(true);
        lsus = usl.findwithParameters(usStatus);
    }

    public void agregarUsuario() {        
        try {
            // Validamos que los dos campos de contraseña contengan el mismo valor ó no estén vacíos.
            if (this.validarPassword()) {
                // Validamos que se haya seleccionado un grupo y una persona.
                if (validarGrupoSelects() && validarPersonaSelects()) {
                                        
                    grupoSeleccionado.setGrupoID(Integer.valueOf(idGrupo));
                    personaSeleccionada.setPersonaID(Long.valueOf(idPersona));
                    
                    us.setGrupoID(grupoSeleccionado);
                    us.setPersonaspersonaID(personaSeleccionada);
                    us.setUsuarioEstado(true);
                    
                    usl.create(us);
                    
                    // reseteamos los valores para que el formulario esté vació siempre que lo invoquen para agregar.
                    us = new Usuario();
                    idGrupo = "0";
                    llenarLista();

                    this.aplicaEventosPagina("Éxito", "Usuario Agregado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialog').hide();");
                    this.aplicaEventosPagina(null, null, "dialogdd", null);
                    this.aplicaEventosPagina(null, null, "frmAdd:selectGrupo", null);
                }

            } 
        } catch (Exception e) {
            e.printStackTrace();
        }       
    }

    public void modificarUsuario(Usuario usVista) {
        
        usSeleccionado = new Usuario();
        usSeleccionado = usVista;
        habilitarCambioPass = false;
        habilitarCambioPasswd();
                
        this.lsgp = this.obtenerListaGruposMod();
        
       this.aplicaEventosPagina(null, null, "frmMod:usDetailMod", "PF('usDialogMod').show();");
       
    }
    
    public void aplicarCambiosUsuario(){
        System.out.println("usuario: " + usSeleccionado.getUsuarioNombre());
        try {
            // Verificamos si se ha cambiado el grupo para el usuario.
            if(usSeleccionado.getGrupoID().getGrupoID() != Integer.valueOf(idGrupo)){
                grupoSeleccionado.setGrupoID(Integer.valueOf(idGrupo));
                usSeleccionado.setGrupoID(grupoSeleccionado);
            }
            // verificamos si se quiere cambiar la contraseña del usuario.
            if (!habilitado){                
                if(this.validarPasswordMod()){                    
                    usl.edit(usSeleccionado); 
                    idGrupo = "0";
                    llenarLista();
                    this.aplicaEventosPagina("Éxito", "Usuario Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
                    
                }
            }else{
                usl.edit(usSeleccionado); 
                idGrupo = "0";
                llenarLista();
                this.aplicaEventosPagina("Éxito", "Usuario Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }       

    }

    public void eliminarUsuario(Usuario usVista) {
        System.out.println("Entramso a Eliminar ");
        System.out.println("usVista: " + usVista.getUsuarioNombre());
        try {
            usVista.setUsuarioEstado(false);
            usl.edit(usVista);
            this.llenarLista();
            this.aplicaEventosPagina("Éxito", "Usuario Eliminado satisfactoriamente", "frmUs:tabUsuarios", null);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    *   Método para obtener la lista de usuarios que tengan un estado = true
    *   así se muestra como si los usarios con estado false han sido borrados.
    */
    public void obtenerListas() {
        
        lsgp = gpl.findAll();
        lspersona = pl.findByNameQuery("Persona.findByUsuario");
        
    }
    
    public List<Grupo> obtenerListaGruposMod(){
        List<Grupo> lsgpMod = new ArrayList<>();
        int valorPosicion = 0;
        Grupo grupoPosicionA = new Grupo();
        Grupo gpSeleccionado = new Grupo();
        
        
        lsgpMod = gpl.findAll();
        grupoPosicionA = lsgpMod.get(0);
        gpSeleccionado = usSeleccionado.getGrupoID();        
        
        /*
        *   Recorremos la lista de grupos para saber la posición del grupo al que el usuario pertenece.
        *   Si está en primer lugar dejamos la lista igual.
        *   Si está en otra posición guardamos el valor de esa posción en una variable (valorPosicion).
        */
        for (int i = 0; i < lsgpMod.size(); i++) {
            
            if (i == 0 && lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
                break;
            }
            if (lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
                valorPosicion = i;
            }
        }
        // validamos que el valor de la posicion del objeto buscado sea distinta de la posicion inicial.
        if(!(valorPosicion == 0)){
                                  
            lsgpMod.remove(valorPosicion); // removemos el objeto duplicado.
            lsgpMod.add(grupoPosicionA); // agregamos el objeto de la posicion inicial a la posicion final de la lista.
            lsgpMod.set(0, gpSeleccionado); // modificamos el objeto de la posicion inicial para setear el objeto que relaciona al grupo con el usuario.
        }
               
        return lsgpMod;
    }
    
    public boolean validarPassword(){        
        System.out.println("campo comprobacion: " + this.passMatch);        
        if (this.passMatch==null || this.passMatch.trim().equals("")){

            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, "PF('usDialog').show();");
            
        }else if(us.getUsuarioPassword().trim().equals(passMatch)){
            return true;
            
        }else{
            System.out.println("Contraseñas no coinciden !!");
            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, "PF('usDialog').show();");
        }
        return false;        
    }
    
    public boolean validarPasswordMod(){        
        System.out.println("campo comprobacion: " + this.passMatch);        
        if (this.passMatch==null || this.passMatch.trim().equals("")){

            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, null);
            
        }else if(usSeleccionado.getUsuarioPassword().trim().equals(passMatch)){
            return true;
            
        }else{
            System.out.println("Contraseñas no coinciden !!");
            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, null);
        }
        return false;        
    }
    
    /*
    *   Método para validar que tanto el SelectOneMenu de Grupo como el de Persona hayan sido cargadas con valores.
    *   Retorna un boolean (true = han sido cargados | false = no han sido cargados)
    */
    public boolean validarGrupoSelects(){
        
        if(this.idGrupo.trim().equals("0")){ 
            this.aplicaEventosPagina("Campo grupo", "El campo grupo es requerido", null, "PF('usDialog').show();");
            
        }else{
            return true;
        }
        
        return false;
    }
    public boolean validarPersonaSelects(){
        
        if(this.idPersona.trim().equals("0")){
            this.aplicaEventosPagina("Campo Persona", "El campo persona es requerido", null, "PF('usDialog').show();");
        }else{
            return true;
        }
        
        return false;
    }
       
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
        
    }
    
    public void habilitarCambioPasswd(){
        if (habilitarCambioPass){
            habilitado = false;
        }else{
            habilitado = true;
        }
    }

    public Usuario getUs() {
        return us;
    }

    public void setUs(Usuario us) {
        this.us = us;
    }

    public List<Usuario> getLsus() {
        return lsus;
    }

    public void setLsus(List<Usuario> lsus) {
        this.lsus = lsus;
    }

    public Usuario getUsSeleccionado() {
        return usSeleccionado;
    }

    public void setUsSeleccionado(Usuario usSeleccionado) {
        this.usSeleccionado = usSeleccionado;
    }

    public List<Grupo> getLsgp() {
        return lsgp;
    }

    public void setLsgp(List<Grupo> lsgp) {
        this.lsgp = lsgp;
    }

    public List<Persona> getLspersona() {
        return lspersona;
    }

    public void setLspersona(List<Persona> lspersona) {
        this.lspersona = lspersona;
    }

    public Grupo getIdGrupoSeleccionado() {
        return grupoSeleccionado;
    }

    public void setIdGrupoSeleccionado(Grupo idGrupo) {
        this.grupoSeleccionado = idGrupo;
    }

    public Persona getIdPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setIdPersonaSeleccionada(Persona idPersona) {
        this.personaSeleccionada = idPersona;
    }

    public String getIdGrupo() {
        if(idGrupo == null){
            setIdGrupo(new String());
        }
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public String getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }

    public String getPassMatch() {
        return passMatch;
    }

    public void setPassMatch(String passMatch) {
        this.passMatch = passMatch;
    }

    public boolean isHabilitarCambioPass() {
        return habilitarCambioPass;
    }

    public void setHabilitarCambioPass(boolean habilitarCambioPass) {
        this.habilitarCambioPass = habilitarCambioPass;
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Servicio;
import org.sacsaapp.interfaces.ServicioFacadeLocal;

/**
 *
 * @author guillermo
 */
@ManagedBean
@ViewScoped
public class ServiciosMB {

    @EJB
    private ServicioFacadeLocal serviciolf;
    
    private Servicio servicio;
    private Servicio servicioSeleccionado;
    private List<Servicio> lsServicios;
    
    
    
    /**
     * Creates a new instance of ServiciosMB
     */
    public ServiciosMB() {
    }
    
    @PostConstruct
    public void init(){
        servicio = new Servicio();
        servicioSeleccionado = new Servicio();
        lsServicios = new ArrayList<>();
        llenarListaServicios();
        
    }
    
    public void llenarListaServicios(){
        lsServicios = serviciolf.findAll();
    }

    
    public void agregarServicio(){
        System.out.println("Agregando Servicio");
        serviciolf.create(servicio);
        
        servicio = new Servicio();
        llenarListaServicios();
        this.aplicaEventosPagina("Éxito", "Servicio Agregado con Satisfacción", "frmServicios:tabServicios", "PF('servicioDialog').hide();");        
        this.aplicaEventosPagina(null, null, "frmAddServicio:ServicioDetail", null);
    }
    
    public void modificarServicio(Servicio s){
        System.out.println("Modificando Servicio");
    }
    
    public void aplicandoCmabiosServicio(){
        
    }
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
        
    }
    
    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Servicio getServicioSeleccionado() {
        return servicioSeleccionado;
    }

    public void setServicioSeleccionado(Servicio servicioSeleccionado) {
        this.servicioSeleccionado = servicioSeleccionado;
    }

    public List<Servicio> getLsServicios() {
        return lsServicios;
    }

    public void setLsServicios(List<Servicio> lsServicios) {
        this.lsServicios = lsServicios;
    }
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import com.oracle.xmlns.webservices.jaxws_databinding.SoapBindingParameterStyle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.sacsaapp.entities.Departamento;
import org.sacsaapp.entities.Direccion;
import org.sacsaapp.entities.Municipio;
import org.sacsaapp.entities.Pais;
import org.sacsaapp.entities.Persona;
import org.sacsaapp.entities.Telefono;
import org.sacsaapp.interfaces.DepartamentoFacadeLocal;
import org.sacsaapp.interfaces.DireccionFacadeLocal;
import org.sacsaapp.interfaces.MunicipioFacadeLocal;
import org.sacsaapp.interfaces.PaisFacadeLocal;
import org.sacsaapp.interfaces.PersonaFacadeLocal;
import org.sacsaapp.interfaces.TelefonoFacadeLocal;

/**
 *
 * @author guillermo
 */

@ManagedBean
@ViewScoped
public class PersonasMB implements Serializable {

    @EJB
    private PersonaFacadeLocal pf;

    @EJB
    private TelefonoFacadeLocal telefonolf;
    
    @EJB
    private DireccionFacadeLocal direccionfl;
    
    @EJB
    private PaisFacadeLocal paisfl;
    
    @EJB
    private DepartamentoFacadeLocal deptofl;
    
    @EJB
    private MunicipioFacadeLocal municipiofl;
    
    // Datos para Persona
    private Persona p;
    private Persona pSeleccionada;
    private Persona pSeleccionadaM;
    private List<Persona> lsp;
    private boolean skip;
    
    // Datos para Telefono
    private Telefono tel;
    
    // Datos para Direccion
    private Direccion dir;
    private List<Pais> lsPaises; 
    private List<Departamento> lsDepto;
    private List<Municipio> lsMunicipio;
    private int idPaisSeleccionado;
    private int idDeptoSeleccionado;
    private int idMunicipioSeleccionado;
    private boolean mostrarDeptos;
    private boolean mostrarMunicipios;
    private boolean mostrarBotonAgregarDir;
    private boolean mostrarBotonModificarDir;
    
    /**
     * Creates a new instance of PersonasMB
     */
    public PersonasMB() {
    }

    @PostConstruct
    public void init() {
        p = new Persona();
        pSeleccionada = new Persona();
        pSeleccionadaM = new Persona();
        tel = new Telefono();
        
        lsp = new ArrayList<>();
        llenarLista();
    }

    public void llenarLista() {
        //lsp = pf.findAll();
        pf.setNumQuery(12);
        Persona pfiltro = new Persona();
        pfiltro.setPersonaEstado(true);
        lsp = pf.findAll();
        lsp = pf.findwithParameters(pfiltro);
               
        int i = 0;
        for (Persona persona : lsp) {
            System.out.println("Persona: " + persona.getPersonaID() + " : " + persona.getTelefonoList().size());
            i++;
        }
        
    }
    
    public void limpiarFormAddPersona(){
        p = new Persona();
        skip = false;
    }

    public void agregarPersona() {
        System.out.println("Entramos a agregar !!");
        try {
            System.out.println("P: " + p.getNombre1() + " " + p.getIdDocumento());
            Date today = new Date();
            p.setPersonaFechaRegistro(today);
            p.setFechaBaja(null);
            p.setPersonaEstado(true);
            pf.create(p);

            p = new Persona();
            llenarLista();

            FacesMessage msg = new FacesMessage("Éxito", "Persona ingresada satisfactoriamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("frmPersona:tabpersonas");
            context.execute("PF('dlg1').hide();");
        } catch (Exception e) {
            e.printStackTrace();
        }            
    }

    public void cargarPersona() {
        System.out.println("Cargamos un OBJ");
        System.out.println("pseleccionada" + pSeleccionadaM.toString());
        pSeleccionadaM = pSeleccionadaM;
        System.out.println("pseleccionada" + p.toString());
    }

    public void modificarPersona() {
        
        try {
            pf.edit(pSeleccionadaM);
            pSeleccionadaM = new Persona();
            llenarLista();

            FacesMessage msg = new FacesMessage("Éxito", "Persona Modificada satisfactoriamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('dlg2').hide();");
            context.update("frmPersona:tabpersonas"); 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminarPersona() {
        System.out.println("Hola mundo ");
        System.out.println("persona selected:" + pSeleccionada.toString());
        try {
            pSeleccionada.setPersonaEstado(false);
            pf.edit(pSeleccionada);
            //pf.remove(pSeleccionada);
            pSeleccionada = new Persona();
            llenarLista();
            this.aplicaEventosPagina("Éxito", "Persona eliminada Satisfactoriamente!", "frmPersona:tabpersonas", null);
        } catch (Exception e) {
            e.printStackTrace();
        }       
    }

    public void cargarPersonaID(Persona pID, boolean esDireccion){
        System.out.println("El ID de persona es: " + pID.getPersonaID());
        pSeleccionada = new Persona();
        pSeleccionada = pID;
        if (esDireccion){
            llenarListaPaises();
            reestablecerValoresDireccion();
            habilitarBotonAgregarDir();
        }
    }
    
    public void habilitarBotonAgregarDir(){
        mostrarBotonAgregarDir = true;
        mostrarBotonModificarDir = false;
    }
    public void reestablecerValoresDireccion(){
        dir = new Direccion();
        mostrarDeptos = true;
        mostrarMunicipios = true;
        idDeptoSeleccionado = 0;
        idMunicipioSeleccionado = 0;
        idPaisSeleccionado = 0;        
    }
    
/**************************************************************************/
    
    //  Seccion de Edición de Teléfonos de Persona
    
    public void agregaTelefono(){
        System.out.println("Entramos a agregar un teléfono");
        System.out.println("El ID de persona es: " + pSeleccionada.getPersonaID());
        try {
            tel.setPersonaID(pSeleccionada);
            tel.setTelEstado(true);
            telefonolf.create(tel);        

            llenarLista();
            
            pSeleccionada = new Persona();
            tel = new Telefono();        

            this.aplicaEventosPagina(null, null, "frmPersona:tabpersonas", null);
            this.aplicaEventosPagina(null, null, "frmPersona:growl", null);
            this.aplicaEventosPagina(null, null, "frmPersona:tabTelefonos", null);
            this.aplicaEventosPagina(null, null, "dialogTel", null);
            this.aplicaEventosPagina("Éxito", "Teléfono agregado !", "frmPersona:tabpersonas", "PF('TelDialog').hide();");
        } catch (Exception e) {
            e.printStackTrace();
        }            
        
    }
    
    public void eliminarTelefono(){
        System.out.println("Elimando Teléfono " + tel.getTelNumero());
        try {
            tel.setTelEstado(false);
            telefonolf.edit(tel);

            tel = new Telefono();
            llenarLista();
            this.aplicaEventosPagina(null, null, "frmPersona:tabpersonas", null);
            this.aplicaEventosPagina(null, null, "frmPersona:growl", null);
            this.aplicaEventosPagina(null, null, "frmPersona:tabTelefonos", null);
            this.aplicaEventosPagina("Ëxito", "Teléfono Eliminado Satisfactoriamente !", "frmPersona:tabpersonas", null);
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
    
    // Fin edicion de Telefono
    
/**************************************************************************/
    
    //  Seccion de Edición de Direcciones de Persona
    
    public void agregarDireccion(){
        try {
            Municipio m = new Municipio();
            m.setMunicipioID(idMunicipioSeleccionado);
            dir.setMunicipioID(m);
            dir.setPersonaID(pSeleccionada);
            dir.setDireccionMunicipio("x");
            dir.setDireccionEstado(true);
            System.out.println("Direccion: " + dir.getDireccionDir() + ", " + dir.getMunicipioID().getMunicipioID());

            direccionfl.create(dir);
            
            pSeleccionada = new Persona();
            reestablecerValoresDireccion();
            this.aplicaEventosPagina(null, null, "frmPersona:growl", null);
            this.aplicaEventosPagina("Ëxito", "Dirección agregada Satisfactoriamente !", "frmPersona:tabpersonas", "PF('DirccDialog').hide();");
        } catch (Exception e) {
            e.printStackTrace();
        }            
    }
    
    public void modificarDireccion(){
        try {
            Municipio m = new Municipio();
            m.setMunicipioID(idMunicipioSeleccionado);
            dir.setMunicipioID(m);
            direccionfl.edit(dir);
            
            reestablecerValoresDireccion();
            this.aplicaEventosPagina(null, null, "frmPersona:growl", null);
            this.aplicaEventosPagina("Ëxito", "Dirección Modificada Satisfactoriamente !", "frmPersona:tabpersonas", "PF('DirccDialog').hide();");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void cargarDatosDireccionEnModal(){
        mostrarBotonAgregarDir = false;
        mostrarBotonModificarDir = true;
        mostrarDeptos = false;
        mostrarMunicipios = false;
        
        idDeptoSeleccionado = dir.getMunicipioID().getDeptoID().getDeptoID();
        idMunicipioSeleccionado = dir.getMunicipioID().getMunicipioID();
        idPaisSeleccionado = dir.getMunicipioID().getDeptoID().getPaisID().getPaisID();
        
        llenarListaPaises();
        obtenerDeptoPorPais(true);
        obtenerMunicipiosPorDepto(true);
    }
    
    public void eliminarDireccion(){
        System.out.println("Elimando dir: " + dir.getDireccionID());
        try {         
            dir.setDireccionEstado(false);
            direccionfl.edit(dir);
            this.aplicaEventosPagina(null, null, "frmPersona:growl", null);
            this.aplicaEventosPagina("Ëxito", "Dirección Eliminado Satisfactoriamente !", "frmPersona:tabpersonas", null);
        } catch (Exception e) {
            e.printStackTrace();
        }       
        
    }
        
    public void llenarListaPaises(){
        lsPaises = new ArrayList<>();
        lsPaises = paisfl.findAll();
    }
    
    public void obtenerDeptoPorPais(boolean esModificacion){
        System.out.println("id Pais: " + idPaisSeleccionado);
        if(!esModificacion){
            idDeptoSeleccionado = 0;
            idMunicipioSeleccionado = 0;
            
        }
        
        lsMunicipio = new ArrayList<>();
        Pais p = new Pais();
        p.setPaisID(idPaisSeleccionado);
        Departamento dept = new Departamento();
        dept.setPaisID(p);
        deptofl.setNumQuery(2);
        lsDepto = new ArrayList<>();
        lsDepto = deptofl.findwithParameters(dept);
        mostrarDeptos = false;
        mostrarMunicipios = true;
        System.out.println("Lista depto Size: " + lsDepto.size());
        
        this.aplicaEventosPagina(null, null, "frmAddDireccion:depto", null);
        this.aplicaEventosPagina(null, null, "frmAddDireccion:municipio", null);
    }
    
    public void obtenerMunicipiosPorDepto(boolean esModificacion){
        System.out.println("id Depto: " + idDeptoSeleccionado);
        if(!esModificacion){
            idMunicipioSeleccionado = 0;
        }
        
        Departamento d = new Departamento();
        d.setDeptoID(idDeptoSeleccionado);
        Municipio m = new Municipio();
        m.setDeptoID(d);
        municipiofl.setNumQuery(2);
        lsMunicipio = new ArrayList<>();
        lsMunicipio = municipiofl.findwithParameters(m);
        mostrarMunicipios = false;
        System.out.println("Size Muni: " + lsMunicipio.size());
        
        this.aplicaEventosPagina(null, null, "frmAddDireccion:municipio", null);
    }
    
    // Fin de edicion de Direccion
    
/**************************************************************************/
       
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
        
    }
    
    // Funciona para el formulario de Persona
    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }

    public List<Persona> getLsp() {
        return lsp;
    }

    public void setLsp(List<Persona> lsp) {
        this.lsp = lsp;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public Persona getpSeleccionada() {
        return pSeleccionada;
    }

    public void setpSeleccionada(Persona pSeleccionada) {
        this.pSeleccionada = pSeleccionada;
    }

    public Persona getpSeleccionadaM() {
        return pSeleccionadaM;
    }

    public void setpSeleccionadaM(Persona pSeleccionadaM) {
        this.pSeleccionadaM = pSeleccionadaM;
    }

    public Telefono getTel() {
        return tel;
    }

    public void setTel(Telefono tel) {
        this.tel = tel;
    }

    public Direccion getDir() {
        return dir;
    }

    public void setDir(Direccion dir) {
        this.dir = dir;
    }
    
    public List<Pais> getLsPaises() {
        return lsPaises;
    }

    public void setLsPaises(List<Pais> lsPaises) {
        this.lsPaises = lsPaises;
    }

    public List<Departamento> getLsDepto() {
        return lsDepto;
    }

    public void setLsDepto(List<Departamento> lsDepto) {
        this.lsDepto = lsDepto;
    }

    public List<Municipio> getLsMunicipio() {
        return lsMunicipio;
    }

    public void setLsMunicipio(List<Municipio> lsMunicipio) {
        this.lsMunicipio = lsMunicipio;
    }

    public int getIdPaisSeleccionado() {
        return idPaisSeleccionado;
    }

    public void setIdPaisSeleccionado(int idPaisSeleccionado) {
        this.idPaisSeleccionado = idPaisSeleccionado;
    }

    public int getIdDeptoSeleccionado() {
        return idDeptoSeleccionado;
    }

    public void setIdDeptoSeleccionado(int idDeptoSeleccionado) {
        this.idDeptoSeleccionado = idDeptoSeleccionado;
    }

    public int getIdMunicipioSeleccionado() {
        return idMunicipioSeleccionado;
    }

    public void setIdMunicipioSeleccionado(int idMunicipioSeleccionado) {
        this.idMunicipioSeleccionado = idMunicipioSeleccionado;
    }

    public boolean isMostrarDeptos() {
        return mostrarDeptos;
    }

    public void setMostrarDeptos(boolean mostrarDeptos) {
        this.mostrarDeptos = mostrarDeptos;
    }

    public boolean isMostrarMunicipios() {
        return mostrarMunicipios;
    }

    public void setMostrarMunicipios(boolean mostrarMunicipios) {
        this.mostrarMunicipios = mostrarMunicipios;
    }    

    public boolean isMostrarBotonAgregarDir() {
        return mostrarBotonAgregarDir;
    }

    public void setMostrarBotonAgregarDir(boolean mostrarBotonAgregarDir) {
        this.mostrarBotonAgregarDir = mostrarBotonAgregarDir;
    }

    public boolean isMostrarBotonModificarDir() {
        return mostrarBotonModificarDir;
    }

    public void setMostrarBotonModificarDir(boolean mostrarBotonModificarDir) {
        this.mostrarBotonModificarDir = mostrarBotonModificarDir;
    }
    
}

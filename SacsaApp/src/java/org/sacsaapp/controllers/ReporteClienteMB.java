/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.sacsaapp.entities.Cliente;
import org.sacsaapp.interfaces.ClienteFacadeLocal;

/**
 *
 * @author deguevara
 * @date 10-25-2015
 * @time 02:56:26 PM
 */
@ManagedBean
@SessionScoped
public class ReporteClienteMB implements Serializable {

    @EJB
    private ClienteFacadeLocal pf;
    private List<Cliente> lsp;
    private StreamedContent streamedContent;
    private Date fechaInicio;
    private Date fechaFin;

    public ReporteClienteMB() {
    }

    @PostConstruct
    public void iniciar() {
        System.out.println("Iniciando");
        lsp = new ArrayList<>();
    }

    public void llenarLista() {
        lsp = pf.findAll();
        System.out.println("se lleno la lista : " + lsp.size());
    }

    public void llenarListaFiltrada() {
        pf.setNumQuery(1);
        Cliente clienteFiltro = new Cliente();
        clienteFiltro.setClienteFechaRegistro(fechaInicio);
        clienteFiltro.setFechaRetiro(fechaFin);
//        Map<String,String> parametrosDeQuery=new HashMap<>();
//        parametrosDeQuery.put("fechaInicio", fechaInicio);
//        pf.findwithParameters(clienteFiltro, null);
        lsp = pf.findwithParameters(clienteFiltro);
//        lsp = pf.findAll();
        System.out.println("se lleno la lista : " + lsp.size());
    }

    public void filtrarReporte() {
        lsp = new ArrayList<>();
        llenarListaFiltrada();
        mostrar();
    }

    public void noFiltrarReporte() {
        lsp = new ArrayList<>();
        llenarLista();
        mostrar();
    }

    private Cliente[] initializeBeanArray() {
        System.out.println("Tratando de obtener array de personas");
        Cliente[] reportRows = lsp.toArray(new Cliente[lsp.size()]);
        System.out.println("se obtuvo el array");
        System.out.println(" Tamaño : " + reportRows.length);

        return reportRows;

    }

    private JRDataSource createReportDataSource() {
        JRBeanArrayDataSource dataSource;
        Cliente[] grupoRows = initializeBeanArray();
        System.out.println("Numero total: " + grupoRows.length);
        dataSource = new JRBeanArrayDataSource(grupoRows);

        return dataSource;
    }

    private JRBeanCollectionDataSource createBeanReportDataSource() {
        JRBeanCollectionDataSource dataSource;
        dataSource = new JRBeanCollectionDataSource(this.lsp);
        return dataSource;
    }

    public void mostrar() {
        try {
            InputStream stream;
            FacesContext context = FacesContext.getCurrentInstance();
            InputStream reportStream = context.getExternalContext().getResourceAsStream("/reportes/rptClientes.jasper");
            InputStream logoStream = context.getExternalContext().getResourceAsStream("/resources/imagenes/sacsa.jpg");
            System.out.println("obteniendo ");
            System.out.println("se obtuvo ");
//            JRDataSource dataSource = createReportDataSource();
            System.out.println("datasource creado 1");
            Map parametros = new HashMap();
            parametros.put("logo", logoStream);
            System.out.println("Agregando logo");
            byte[] b = JasperRunManager.runReportToPdf(reportStream, parametros, createBeanReportDataSource());
            stream = new ByteArrayInputStream(b);
            stream.mark(0); //remember to this position!
            streamedContent = new DefaultStreamedContent(stream, "application/pdf");
        } catch (JRException ex) {
            Logger.getLogger(ReporteClienteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public StreamedContent getStreamedContent() {
        try {
            if (streamedContent != null) {
                streamedContent.getStream().reset(); //reset stream to the start position!
            }
            return streamedContent;
        } catch (IOException ex) {
            Logger.getLogger(ReporteClienteMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new DefaultStreamedContent();
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}

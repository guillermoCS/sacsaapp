/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.CuentaPorCobrar;
import org.sacsaapp.entities.Lectura;
import org.sacsaapp.interfaces.LecturaFacadeLocal;

/**
 *
 * @author merlin
 */
@ManagedBean
@ViewScoped
public class LecturaMB {
 @EJB
    private LecturaFacadeLocal pf;
    
    private Lectura p;
    private Long valorCuenta;
    private Lectura pSeleccionado;
//    private Pais pSeleccionadaM;
    private List<Lectura> lsp;
    
    /**
     * Creates a new instance of LecturaMB
     */
    public LecturaMB() {
    }
    
    @PostConstruct
    public void init() {
        p = new Lectura();
        pSeleccionado = new Lectura();
//        pSeleccionadaM = new Pais();
        lsp = new ArrayList<>();
        llenarLista();
    }
    
    public void llenarLista() {
        lsp = pf.findAll();
    }
    
    public void agregarLectura() {
               
        CuentaPorCobrar c = new CuentaPorCobrar();
        c.setClienteNumeroCuenta(valorCuenta);
        p.setClienteCuenta(c);
        pf.create(p);
        p = new Lectura();
        llenarLista();

        this.aplicaEventosPagina("Éxito", "Lectura Agregada satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialog').hide();");
        this.aplicaEventosPagina(null, null, "dialogdd", null);
        this.aplicaEventosPagina(null, null, "frmAdd:selectGrupo", null);
    }
    
    
    public void modificarLectura(Lectura usVista) {
        
        pSeleccionado = new Lectura();
        pSeleccionado = usVista;
//        habilitarCambioPass = false;
//        habilitarCambioPasswd();
//                
//        this.lsgp = this.obtenerListaGruposMod();
        
       this.aplicaEventosPagina(null, null, "frmMod:usDetailMod", "PF('usDialogMod').show();");
       
    }
    
     public void aplicarCambiosLectura(){
//        System.out.println("usuario: " + pSeleccionado.getPaisNombre());
//        try {
//            // Verificamos si se ha cambiado el grupo para el usuario.
//            if(usSeleccionado.getGrupoID().getGrupoID() != Integer.valueOf(idGrupo)){
//                grupoSeleccionado.setGrupoID(Integer.valueOf(idGrupo));
//                usSeleccionado.setGrupoID(grupoSeleccionado);
//            }
            // verificamos si se quiere cambiar la contraseña del usuario.
//            if (!habilitado){                
//                if(this.validarPasswordMod()){                    
//                    usl.edit(usSeleccionado); 
//                    idGrupo = "0";
//                    llenarLista();
//                    this.aplicaEventosPagina("Éxito", "Usuario Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
//                    
//                }
//            }else{
                pf.edit(pSeleccionado); 
//                idGrupo = "0";
                llenarLista();
                this.aplicaEventosPagina("Éxito", "Lectura Modificado satisfactoriamente", "frmUs:tabUsuarios", "PF('usDialogMod').hide();");
//            }
            
            
//        } catch (Exception e) {
//            e.printStackTrace();
//        }       

    }

    public void eliminarLectura(Lectura usVista) {
        System.out.println("Entramso a Eliminar ");
        System.out.println("usVista: " + usVista.getClienteCuenta());
//        try {
//            usVista.setUsuarioEstado(false);
            pf.remove(usVista);
            this.llenarLista();
            this.aplicaEventosPagina("Éxito", "Lectura Eliminado satisfactoriamente", "frmUs:tabUsuarios", null);
            
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /*
    *   Método para obtener la lista de usuarios que tengan un estado = true
    *   así se muestra como si los usarios con estado false han sido borrados.
    */
//    public void obtenerListas() {
//        
//        lsp = gpl.findAll();
//        lspersona = pl.findByNameQuery("Persona.findByUsuario");
//        
//    }
    
//    public List<Grupo> obtenerListaGruposMod(){
//        List<Grupo> lsgpMod = new ArrayList<>();
//        int valorPosicion = 0;
//        Grupo grupoPosicionA = new Grupo();
//        Grupo gpSeleccionado = new Grupo();
//        
//        
//        lsgpMod = gpl.findAll();
//        grupoPosicionA = lsgpMod.get(0);
//        gpSeleccionado = usSeleccionado.getGrupoID();        
//        
//        /*
//        *   Recorremos la lista de grupos para saber la posición del grupo al que el usuario pertenece.
//        *   Si está en primer lugar dejamos la lista igual.
//        *   Si está en otra posición guardamos el valor de esa posción en una variable (valorPosicion).
//        */
//        for (int i = 0; i < lsgpMod.size(); i++) {
//            
//            if (i == 0 && lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
//                break;
//            }
//            if (lsgpMod.get(i).getGrupoID() == gpSeleccionado.getGrupoID()){
//                valorPosicion = i;
//            }
//        }
//        // validamos que el valor de la posicion del objeto buscado sea distinta de la posicion inicial.
//        if(!(valorPosicion == 0)){
//                                  
//            lsgpMod.remove(valorPosicion); // removemos el objeto duplicado.
//            lsgpMod.add(grupoPosicionA); // agregamos el objeto de la posicion inicial a la posicion final de la lista.
//            lsgpMod.set(0, gpSeleccionado); // modificamos el objeto de la posicion inicial para setear el objeto que relaciona al grupo con el usuario.
//        }
//               
//        return lsgpMod;
//    }
    
//    public boolean validarPassword(){        
//        System.out.println("campo comprobacion: " + this.passMatch);        
//        if (this.passMatch==null || this.passMatch.trim().equals("")){
//
//            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, "PF('usDialog').show();");
//            
//        }else if(us.getUsuarioPassword().trim().equals(passMatch)){
//            return true;
//            
//        }else{
//            System.out.println("Contraseñas no coinciden !!");
//            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, "PF('usDialog').show();");
//        }
//        return false;        
//    }
//    
//    public boolean validarPasswordMod(){        
//        System.out.println("campo comprobacion: " + this.passMatch);        
//        if (this.passMatch==null || this.passMatch.trim().equals("")){
//
//            this.aplicaEventosPagina("Contraseña incorrecta", "El campo de comprobación de contraseña está vacío", null, null);
//            
//        }else if(usSeleccionado.getUsuarioPassword().trim().equals(passMatch)){
//            return true;
//            
//        }else{
//            System.out.println("Contraseñas no coinciden !!");
//            this.aplicaEventosPagina("Contraseña Incorrecta", "El campo de comprobación de contraseña no coincide con el campo contraseña", null, null);
//        }
//        return false;        
//    }
//    
//    /*
//    *   Método para validar que tanto el SelectOneMenu de Grupo como el de Persona hayan sido cargadas con valores.
//    *   Retorna un boolean (true = han sido cargados | false = no han sido cargados)
//    */
//    public boolean validarGrupoSelects(){
//        
//        if(this.idGrupo.trim().equals("0")){ 
//            this.aplicaEventosPagina("Campo grupo", "El campo grupo es requerido", null, "PF('usDialog').show();");
//            
//        }else{
//            return true;
//        }
//        
//        return false;
//    }
//    public boolean validarPersonaSelects(){
//        
//        if(this.idPersona.trim().equals("0")){
//            this.aplicaEventosPagina("Campo Persona", "El campo persona es requerido", null, "PF('usDialog').show();");
//        }else{
//            return true;
//        }
//        
//        return false;
//    }
    
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
}
    
//    public void habilitarCambioPasswd(){
//        if (habilitarCambioPass){
//            habilitado = false;
//        }else{
//            habilitado = true;
//        }
//    }

    public Lectura getP() {
        return p;
    }

    public void setP(Lectura p) {
        this.p = p;
    }

    public List<Lectura> getLsp() {
        return lsp;
    }

    public void setLsp(List<Lectura> lsp) {
        this.lsp = lsp;
    }

    public Lectura getPSeleccionado() {
        return pSeleccionado;
    }

    public void setPSeleccionado(Lectura pSeleccionado) {
        this.pSeleccionado = pSeleccionado;
    }

//    public List<Grupo> getLsgp() {
//        return lsgp;
//    }
//
//    public void setLsgp(List<Grupo> lsgp) {
//        this.lsgp = lsgp;
//    }

//    public List<Persona> getLspersona() {
//        return lspersona;
//    }
//
//    public void setLspersona(List<Persona> lspersona) {
//        this.lspersona = lspersona;
//    }

//    public Grupo getIdGrupoSeleccionado() {
//        return grupoSeleccionado;
//    }
//
//    public void setIdGrupoSeleccionado(Grupo idGrupo) {
//        this.grupoSeleccionado = idGrupo;
//    }
//
//    public Persona getIdPersonaSeleccionada() {
//        return personaSeleccionada;
//    }
//
//    public void setIdPersonaSeleccionada(Persona idPersona) {
//        this.personaSeleccionada = idPersona;
//    }

//    public String getIdGrupo() {
//        if(idGrupo == null){
//            setIdGrupo(new String());
//        }
//        return idGrupo;
//    }

//    public void setIdGrupo(String idGrupo) {
//        this.idGrupo = idGrupo;
//    }
//
//    public String getIdPersona() {
//        return idPersona;
//    }
//
//    public void setIdPersona(String idPersona) {
//        this.idPersona = idPersona;
//    }

//    public String getPassMatch() {
//        return passMatch;
//    }
//
//    public void setPassMatch(String passMatch) {
//        this.passMatch = passMatch;
//    }
//
//    public boolean isHabilitarCambioPass() {
//        return habilitarCambioPass;
//    }

//    public void setHabilitarCambioPass(boolean habilitarCambioPass) {
//        this.habilitarCambioPass = habilitarCambioPass;
//    }
//
//    public boolean isHabilitado() {
//        return habilitado;
//    }
//
//    public void setHabilitado(boolean habilitado) {
//        this.habilitado = habilitado;
//    }

    public Long getValorCuenta() {
        return valorCuenta;
    }

    public void setValorCuenta(Long valorCuenta) {
        this.valorCuenta = valorCuenta;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Directiva;
import org.sacsaapp.interfaces.DirectivaFacadeLocal;

/**
 *
 * @author deguevara
 * @date 11-24-2015
 * @time 07:55:44 PM
 */
@ManagedBean(name = "directivaMB")
@ViewScoped
public class DirectivaMB {

    @EJB
    private DirectivaFacadeLocal directivaFacade;

    private List<Directiva> listaDirectivas;

    private Directiva directivaNueva;

    private Directiva directivaSeleccionada;

    @PostConstruct
    public void init() {
        listaDirectivas = new ArrayList<>();
        directivaNueva = new Directiva();
        directivaSeleccionada = new Directiva();
        obtenerDirectivas();
    }

    public void obtenerDirectivas() {
        System.out.println("Obteniendo lista de directivas");
        listaDirectivas = directivaFacade.findAll();
        System.out.println("Tamaño de lista: " + listaDirectivas.size());
    }

    public void agregarDirectiva() {
        try {
            System.out.println("Add:" + directivaNueva);
            directivaFacade.create(directivaNueva);
            obtenerDirectivas();
            this.aplicaEventosPagina("Éxito", "Directiva " + getDirectivaNueva().getCargo() + " Agregada satisfactoriamente", "frmDirectiva:tabDirectivas", "PF('directivaDialogo').hide();", FacesMessage.SEVERITY_INFO);
            this.aplicaEventosPagina(null, null, "dialogdd", null, null);
        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Directiva " + getDirectivaNueva().getCargo() + " no se pudo crear!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('directivaDialogo').hide();");
            context.update("frmDirectiva:tabDirectivas");
            e.printStackTrace();
        }
    }

    public void abrirDialogoModificar(Directiva directivaVista) {
        System.out.println("Cargando obj a modificar");
        directivaSeleccionada = new Directiva();
        directivaSeleccionada = directivaVista;
        System.out.println("-------" + directivaSeleccionada);
    }

        public void modificarDirectiva() {

        try {
            directivaFacade.edit(directivaSeleccionada);
            obtenerDirectivas();
            this.aplicaEventosPagina("Éxito", "Directiva Modificada satisfactoriamente", "frmDirectiva:tabDirectivas", "PF('usDirectivaMod').hide();",FacesMessage.SEVERITY_INFO);
        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Directiva no se pudo modificar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('usDirectivaMod').hide();");
            context.update("frmDirectiva:tabDirectivas");
        } finally {
            directivaSeleccionada = new Directiva();
        }

    }
    /*
     *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
     *   msgA = mensaje encabezado
     *   msgB = descripción del mensaje
     *   msgC = componente a actualizar en la página
     *   msgD = codigo a ejecutar
     *   
     */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {

        if (msgA != null && msgB != null) {
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        org.primefaces.context.RequestContext context = org.primefaces.context.RequestContext.getCurrentInstance();
        if (msgC != null) {
            context.update(msgC);
        }
        if (msgD != null) {
            context.execute(msgD);
        }
    }

    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD, FacesMessage.Severity severity) {

        if (msgA != null && msgB != null) {
            FacesMessage msg = new FacesMessage(severity, msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        RequestContext context = RequestContext.getCurrentInstance();
        if (msgC != null) {
            context.update(msgC);
        }
        if (msgD != null) {
            context.execute(msgD);
        }

    }

    public List<Directiva> getListaDirectivas() {
        return listaDirectivas;
    }

    public void setListaDirectivas(List<Directiva> listaDirectivas) {
        this.listaDirectivas = listaDirectivas;
    }

    public Directiva getDirectivaNueva() {
        return directivaNueva;
    }

    public void setDirectivaNueva(Directiva directivaNueva) {
        this.directivaNueva = directivaNueva;
    }

    public Directiva getDirectivaSeleccionada() {
        return directivaSeleccionada;
    }

    public void setDirectivaSeleccionada(Directiva directivaSeleccionada) {
        this.directivaSeleccionada = directivaSeleccionada;
    }
}

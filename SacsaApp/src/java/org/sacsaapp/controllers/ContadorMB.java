/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Contador;
import org.sacsaapp.entities.CuentaPorCobrar;
import org.sacsaapp.interfaces.ContadorFacadeLocal;
import org.sacsaapp.interfaces.CuentaPorCobrarFacadeLocal;

/**
 *
 * @author deguevara
 * @date 11-01-2015
 * @time 06:25:03 PM
 */
@ManagedBean(name = "contadorMB")
@ViewScoped
public class ContadorMB implements Serializable {

    @EJB
    private ContadorFacadeLocal contadorFacade;
    @EJB
    private CuentaPorCobrarFacadeLocal cuentaPorCobrarFacade;

    private List<Contador> listaContadores;
    private List<CuentaPorCobrar> listaCuentasPorCobrar;

    private Contador contadorNuevo;

    private Contador contadorSeleccionado;

    private String idCuenta;

    public ContadorMB() {
    }
    

    @PostConstruct
    public void init() {
        listaCuentasPorCobrar = new ArrayList<>();
        listaContadores = new ArrayList<>();
        contadorNuevo = new Contador();
        contadorSeleccionado = new Contador();
        obtenerContadores();
        obtenerCuentasPorCobrar();
    }

    public void agregarContador() {
        System.out.println("cuenta ID  --: " + getIdCuenta());
        try {
            if (validarCuentaSelects()) {
                CuentaPorCobrar cobrar = new CuentaPorCobrar();
                cobrar.setNumeroCuenta(getIdCuenta());
                System.out.println("buscar: " + cobrar);
                cuentaPorCobrarFacade.setNumQuery(2);
                cobrar = cuentaPorCobrarFacade.findOnewithParameters(cobrar);
                System.out.println("encontrada: " + cobrar);
                contadorNuevo.setCuentasporCobrarclienteCuenta(cobrar);
                contadorNuevo.setEstado(true);
                contadorFacade.create(contadorNuevo);
                obtenerContadores();
                this.aplicaEventosPagina("Éxito", "Departamento " + getContadorNuevo().getNumeroContador() + " Agregado satisfactoriamente", "frmDepartamentos:tblDepartamentos", "PF('deptoDialog').hide();", FacesMessage.SEVERITY_INFO);
                this.aplicaEventosPagina(null, null, "dialogdd", null, null);
                this.aplicaEventosPagina(null, null, "frmAddDepartamento:selectPais", null, null);
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Contador no se pudo crear!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('deptoDialog').hide();");
            context.update("frmDepartamentos:tblDepartamentos");
        }

    }

    public void abrirDialogoModificar(Contador contadorVista) {

        contadorSeleccionado = new Contador();
        System.out.println("conta a setear: " + contadorVista.toString());
        contadorSeleccionado = contadorVista;
        System.out.println("conta a seteado: " + contadorSeleccionado.toString());

        this.listaCuentasPorCobrar = this.obtenerListaCuentasMod();
    }

    public List<CuentaPorCobrar> obtenerListaCuentasMod() {
        List<CuentaPorCobrar> lsgpMod = new ArrayList<>();
        int valorPosicion = 0;
        CuentaPorCobrar grupoPosicionA = new CuentaPorCobrar();
        CuentaPorCobrar gpSeleccionado = new CuentaPorCobrar();

        lsgpMod = cuentaPorCobrarFacade.findAll();
        grupoPosicionA = lsgpMod.get(0);
        gpSeleccionado = contadorSeleccionado.getCuentasporCobrarclienteCuenta();

        /*
         *   Recorremos la lista de grupos para saber la posición del grupo al que el usuario pertenece.
         *   Si está en primer lugar dejamos la lista igual.
         *   Si está en otra posición guardamos el valor de esa posción en una variable (valorPosicion).
         */
        for (int i = 0; i < lsgpMod.size(); i++) {

            if (i == 0 && lsgpMod.get(i).getNumeroCuenta() == gpSeleccionado.getNumeroCuenta()) {
                break;
            }
            if (lsgpMod.get(i).getNumeroCuenta() == gpSeleccionado.getNumeroCuenta()) {
                valorPosicion = i;
            }
        }
        // validamos que el valor de la posicion del objeto buscado sea distinta de la posicion inicial.
        if (!(valorPosicion == 0)) {

            lsgpMod.remove(valorPosicion); // removemos el objeto duplicado.
            lsgpMod.add(grupoPosicionA); // agregamos el objeto de la posicion inicial a la posicion final de la lista.
            lsgpMod.set(0, gpSeleccionado); // modificamos el objeto de la posicion inicial para setear el objeto que relaciona al grupo con el usuario.
        }

        return lsgpMod;
    }

    public void modificarCuenta() {

        try {
            contadorFacade.edit(contadorSeleccionado);
            obtenerContadores();
            this.aplicaEventosPagina("Éxito", "Departamento Modificado satisfactoriamente", "frmDepartamentos:tblDepartamentos", "PF('deptoDialogMod').hide();", FacesMessage.SEVERITY_INFO);

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Departamento no se pudo modificar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('deptoDialogMod').hide();");
            context.update("frmDepartamentos:tblDepartamentos");
        } finally {
            contadorSeleccionado = new Contador();
        }

    }

    public void eliminarContador(Contador conta) {
        System.out.println("Hola mundo ");
        try {
            contadorSeleccionado = conta;
            contadorSeleccionado.setEstado(false);
            contadorFacade.edit(contadorSeleccionado);
            obtenerContadores();
            FacesMessage msg = new FacesMessage("Éxito", "Contador Eliminado satisfactoriamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("frmDepartamentos:tblDepartamentos");
        } catch (Exception e) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Contador no se pudo eliminar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("frmDepartamentos:tblDepartamentos");
        }

    }

    public void obtenerCuentasPorCobrar() {
        System.out.println("Obteniendo lista de cuentas");
        listaCuentasPorCobrar = cuentaPorCobrarFacade.findAll();
        System.out.println("Tamaño de lista: " + listaCuentasPorCobrar.size());
    }

    public void obtenerContadores() {
        listaContadores = contadorFacade.findAll();
    }

    public void cambiarCuentaSeleccion() {
        System.out.println("Valor:: " + this.idCuenta);
        setIdCuenta(getIdCuenta());

    }

    public boolean validarCuentaSelects() {

        if (this.idCuenta.equalsIgnoreCase("0")) {
            this.aplicaEventosPagina("Campo Cuenta", "El campo cuenta es requerido", null, "PF('deptoDialog').show();", FacesMessage.SEVERITY_ERROR);

        } else {
            return true;
        }

        return false;
    }

    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD, FacesMessage.Severity severity) {

        if (msgA != null && msgB != null) {
            FacesMessage msg = new FacesMessage(severity, msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        RequestContext context = RequestContext.getCurrentInstance();
        if (msgC != null) {
            context.update(msgC);
        }
        if (msgD != null) {
            context.execute(msgD);
        }

    }

    public List<Contador> getListaContadores() {
        return listaContadores;
    }

    public void setListaContadores(List<Contador> listaContadores) {
        this.listaContadores = listaContadores;
    }

    public List<CuentaPorCobrar> getListaCuentasPorCobrar() {
        return listaCuentasPorCobrar;
    }

    public void setListaCuentasPorCobrar(List<CuentaPorCobrar> listaCuentasPorCobrar) {
        this.listaCuentasPorCobrar = listaCuentasPorCobrar;
    }

    public Contador getContadorNuevo() {
        return contadorNuevo;
    }

    public void setContadorNuevo(Contador contadorNuevo) {
        this.contadorNuevo = contadorNuevo;
    }

    public Contador getContadorSeleccionado() {
        return contadorSeleccionado;
    }

    public void setContadorSeleccionado(Contador contadorSeleccionado) {
        this.contadorSeleccionado = contadorSeleccionado;
    }

    public String getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Colecturia;
import org.sacsaapp.interfaces.ColecturiaFacadeLocal;

/**
 *
 * @author guillermo
 */
@ManagedBean
@ViewScoped
public class PagosMB {

    @EJB
    private ColecturiaFacadeLocal colfl;

    private String correlativoBusqueda;
    private Colecturia c;
    private boolean botonHabilado;
    private boolean MensajeHabilado;

    /**
     * Creates a new instance of PagosMB
     */
    public PagosMB() {
    }
    
    @PostConstruct
    public void init(){
        botonHabilado = true;
        MensajeHabilado = false;
    }

    public void buscarFactura() {
        System.out.println("Correlativo" + correlativoBusqueda);
        c = new Colecturia();
        c.setCorrelativoFactura(correlativoBusqueda);
        colfl.setNumQuery(1);
        System.out.println("Valor c: " + c.getCorrelativoFactura());
        c = colfl.findOnewithParameters(c);
        if (c.getPagoPendiente()){
            botonHabilado = false;
            MensajeHabilado = false;
        }else {
            botonHabilado = true;
            MensajeHabilado = true;
        }

        this.aplicaEventosPagina(null, null, "frmFacturas:panelFacturas", null);
        
    }
    
    public void pagarFactura(){
        c.setPagoPendiente(false);
        botonHabilado = true;
        c.setPagoCancelado(new Date());
        colfl.edit(c);
        this.aplicaEventosPagina("Éxito", "Factura Cancelada !!", "frmFacturas:panelFacturas", null);
        //this.aplicaEventosPagina(null, null, "frmFacturas:panelFacturas", null);
    } 

    /*
     *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
     *   msgA = mensaje encabezado
     *   msgB = descripción del mensaje
     *   msgC = componente a actualizar en la página
     *   msgD = codigo a ejecutar
     *   
     */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {

        if (msgA != null && msgB != null) {
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        RequestContext context = RequestContext.getCurrentInstance();
        if (msgC != null) {
            context.update(msgC);
        }
        if (msgD != null) {
            context.execute(msgD);
        }

    }

    public String getCorrelativoBusqueda() {
        return correlativoBusqueda;
    }

    public void setCorrelativoBusqueda(String correlativoBusqueda) {
        this.correlativoBusqueda = correlativoBusqueda;
    }

    public Colecturia getC() {
        return c;
    }

    public void setC(Colecturia c) {
        this.c = c;
    }

    public boolean isBotonHabilado() {
        return botonHabilado;
    }

    public void setBotonHabilado(boolean botonHabilado) {
        this.botonHabilado = botonHabilado;
    }

    public boolean isMensajeHabilado() {
        return MensajeHabilado;
    }

    public void setMensajeHabilado(boolean MensajeHabilado) {
        this.MensajeHabilado = MensajeHabilado;
    }

}

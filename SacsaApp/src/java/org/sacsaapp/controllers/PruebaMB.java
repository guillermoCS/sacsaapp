/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.sacsaapp.entities.Persona;
import org.sacsaapp.interfaces.PersonaFacadeLocal;

/**
 *
 * @author guillermo
 */
@ManagedBean
@RequestScoped
public class PruebaMB {
    
    @EJB
    PersonaFacadeLocal personafl;
    
    
    Persona p;
    List<Persona> lsPersonas;
    
    

    /**
     * Creates a new instance of PruebaMB
     */
    public PruebaMB() {
    }
    
    public void llenarLista(){
        personafl.setNumQuery(13);
        Persona px = new Persona();
        ArrayList<String> indices = new ArrayList<String>();
        ArrayList values = new ArrayList();
        indices.add("name");
        indices.add("fechaB");
        values.add("Guillermo");
        Date dfecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dfecha = format.parse("1989-07-25");
        } catch (ParseException ex) {
            Logger.getLogger(PruebaMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        values.add(dfecha);
        lsPersonas = personafl.findwithParameters(px, indices, values);
        
        for (Persona p : lsPersonas) {
            System.out.println("persona: " + p.getNombre1() + " " + p.getApellido1() + " - " + p.getPersonaFechNacimiento());
        }
    }
    
}

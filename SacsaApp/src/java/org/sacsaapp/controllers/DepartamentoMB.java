/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Departamento;
import org.sacsaapp.entities.Pais;
import org.sacsaapp.interfaces.DepartamentoFacadeLocal;
import org.sacsaapp.interfaces.PaisFacadeLocal;

/**
 *
 * @author deguevara
 * @date 11-01-2015
 * @time 06:25:03 PM
 */
@ManagedBean(name = "departamentoMB")
@ViewScoped
public class DepartamentoMB implements Serializable {

    @EJB
    private DepartamentoFacadeLocal deptoFacade;
    @EJB
    private PaisFacadeLocal paisFacade;

    private List<Pais> listaPaises;
    private List<Departamento> listaDepartamentos;

    private Departamento departamentoNuevo;

    private Departamento departamentoSeleccionado;

    private int idPais;

    @PostConstruct
    public void init() {
        listaDepartamentos = new ArrayList<>();
        listaPaises = new ArrayList<>();
        departamentoNuevo = new Departamento();
        departamentoSeleccionado = new Departamento();
        obtenerDepartamentos();
        obtenerPaises();
    }

    public void agregarDepartamento() {
        System.out.println("Pais ID  --: " + getIdPais());
        try {
            if (validarPaisSelects()) {
                Pais p = new Pais(getIdPais());
                departamentoNuevo.setPaisID(p);
                deptoFacade.create(departamentoNuevo);
                obtenerDepartamentos();
                this.aplicaEventosPagina("Éxito", "Departamento " + getDepartamentoNuevo().getDeptoNombre() + " Agregado satisfactoriamente", "frmDepartamentos:tblDepartamentos", "PF('deptoDialog').hide();",FacesMessage.SEVERITY_INFO);
                this.aplicaEventosPagina(null, null, "dialogdd", null,null);
                this.aplicaEventosPagina(null, null, "frmAddDepartamento:selectPais", null,null);
            }

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Departamento " + getDepartamentoNuevo().getDeptoNombre() + " no se pudo crear!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('deptoDialog').hide();");
            context.update("frmDepartamentos:tblDepartamentos");
        }

    }

    public void abrirDialogoModificar(Departamento deptoVista) {

        departamentoSeleccionado = new Departamento();
        System.out.println("Depto a setear: " + deptoVista.toString());
        departamentoSeleccionado = deptoVista;
        System.out.println("Depto a seteado: " + departamentoSeleccionado.toString());

        this.listaPaises = this.obtenerListaGruposMod();
    }

    public List<Pais> obtenerListaGruposMod() {
        List<Pais> lsgpMod = new ArrayList<>();
        int valorPosicion = 0;
        Pais grupoPosicionA = new Pais();
        Pais gpSeleccionado = new Pais();

        lsgpMod = paisFacade.findAll();
        grupoPosicionA = lsgpMod.get(0);
        gpSeleccionado = departamentoSeleccionado.getPaisID();

        /*
         *   Recorremos la lista de grupos para saber la posición del grupo al que el usuario pertenece.
         *   Si está en primer lugar dejamos la lista igual.
         *   Si está en otra posición guardamos el valor de esa posción en una variable (valorPosicion).
         */
        for (int i = 0; i < lsgpMod.size(); i++) {

            if (i == 0 && lsgpMod.get(i).getPaisID() == gpSeleccionado.getPaisID()) {
                break;
            }
            if (lsgpMod.get(i).getPaisID() == gpSeleccionado.getPaisID()) {
                valorPosicion = i;
            }
        }
        // validamos que el valor de la posicion del objeto buscado sea distinta de la posicion inicial.
        if (!(valorPosicion == 0)) {

            lsgpMod.remove(valorPosicion); // removemos el objeto duplicado.
            lsgpMod.add(grupoPosicionA); // agregamos el objeto de la posicion inicial a la posicion final de la lista.
            lsgpMod.set(0, gpSeleccionado); // modificamos el objeto de la posicion inicial para setear el objeto que relaciona al grupo con el usuario.
        }

        return lsgpMod;
    }

    public void modificarDepartamento() {

        try {
            deptoFacade.edit(departamentoSeleccionado);
            obtenerDepartamentos();
            this.aplicaEventosPagina("Éxito", "Departamento Modificado satisfactoriamente", "frmDepartamentos:tblDepartamentos", "PF('deptoDialogMod').hide();",FacesMessage.SEVERITY_INFO);

        } catch (Exception e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Departamento no se pudo modificar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('deptoDialogMod').hide();");
            context.update("frmDepartamentos:tblDepartamentos");
        } finally {
            departamentoSeleccionado = new Departamento();
        }

    }

    public void eliminarDepartamento(Departamento depto) {
        System.out.println("Hola mundo ");
        try {
            departamentoSeleccionado = depto;
            deptoFacade.remove(departamentoSeleccionado);
            obtenerDepartamentos();
            FacesMessage msg = new FacesMessage("Éxito", "Departamento Eliminado satisfactoriamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("frmDepartamentos:tblDepartamentos");
        } catch (Exception e) {

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Departamento no se pudo eliminar");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("frmDepartamentos:tblDepartamentos");
        }

    }

    public void obtenerDepartamentos() {
        System.out.println("Obteniendo lista de deptos");
        listaDepartamentos = deptoFacade.findAll();
        System.out.println("Tamaño de lista: " + listaDepartamentos.size());
    }

    public void obtenerPaises() {
        listaPaises = paisFacade.findAll();
    }

    public void cambiarPaisSeleccion() {
        System.out.println("Valor:: " + this.idPais);
        setIdPais(getIdPais());

    }

    public boolean validarPaisSelects() {

        if (this.idPais == 0) {
            this.aplicaEventosPagina("Campo Pais", "El campo pais es requerido", null, "PF('deptoDialog').show();",FacesMessage.SEVERITY_ERROR);

        } else {
            return true;
        }

        return false;
    }

    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD,FacesMessage.Severity severity) {

        if (msgA != null && msgB != null) {
            FacesMessage msg = new FacesMessage(severity,msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        RequestContext context = RequestContext.getCurrentInstance();
        if (msgC != null) {
            context.update(msgC);
        }
        if (msgD != null) {
            context.execute(msgD);
        }

    }

    /**
     * @return the listaPaises
     */
    public List<Pais> getListaPaises() {
        return listaPaises;
    }

    /**
     * @param listaPaises the listaPaises to set
     */
    public void setListaPaises(List<Pais> listaPaises) {
        this.listaPaises = listaPaises;
    }

    /**
     * @return the listaDepartamentos
     */
    public List<Departamento> getListaDepartamentos() {
        return listaDepartamentos;
    }

    /**
     * @param listaDepartamentos the listaDepartamentos to set
     */
    public void setListaDepartamentos(List<Departamento> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }

    /**
     * @return the departamentoNuevo
     */
    public Departamento getDepartamentoNuevo() {
        return departamentoNuevo;
    }

    /**
     * @param departamentoNuevo the departamentoNuevo to set
     */
    public void setDepartamentoNuevo(Departamento departamentoNuevo) {
        this.departamentoNuevo = departamentoNuevo;
    }

    /**
     * @return the departamentoSeleccionado
     */
    public Departamento getDepartamentoSeleccionado() {
        return departamentoSeleccionado;
    }

    /**
     * @param departamentoSeleccionado the departamentoSeleccionado to set
     */
    public void setDepartamentoSeleccionado(Departamento departamentoSeleccionado) {
        this.departamentoSeleccionado = departamentoSeleccionado;
    }

    /**
     * @return the idPais
     */
    public int getIdPais() {
        return idPais;
    }

    /**
     * @param idPais the idPais to set
     */
    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

}

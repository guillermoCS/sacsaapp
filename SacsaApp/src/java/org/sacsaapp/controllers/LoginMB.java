/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import com.sun.faces.context.SessionMap;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.ProjectStage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Grupo;
import org.sacsaapp.entities.Usuario;
import org.sacsaapp.filters.MenuFilter;
import org.sacsaapp.interfaces.GrupoFacadeLocal;
import org.sacsaapp.interfaces.UsuarioFacadeLocal;
import org.sacsaapp.sessionBean.SessionBean;

/**
 *
 * @author guillermo
 */
@ManagedBean
@SessionScoped
public class LoginMB implements Serializable {

    @EJB
    private UsuarioFacadeLocal usuariofl;

    @EJB
    private GrupoFacadeLocal grupofl;

    private Usuario usuario;

    private String paginaRedireccionar;
    protected SessionMap mapSesion;
    private MenuFilter menu;

    /**
     * Creates a new instance of LoginMB
     */
    public LoginMB() {
    }

    @PostConstruct
    public void init() {
        usuario = new Usuario();
        menu = new MenuFilter();
    }

    public void verificarAccesoUsuario() {
               
        if(usuario.getUsuarioNombre().isEmpty()){
            this.aplicaEventosPagina("Campo Usuario", "El Campo de Usuario es requerido", null, null);
        }else if (usuario.getUsuarioPassword().isEmpty()) {
            this.aplicaEventosPagina("Campo Contraseña", "El Campo de Contraseña es requerido", null, null);
        } else{
            if (validarUsuario()) {
                verificarGrupo();
                HttpSession session = SessionBean.getSession();
                session.setAttribute("SessionUsuario", usuario.getUsuarioNombre());
                session.setAttribute("Data", usuario);
                FacesContext fc = FacesContext.getCurrentInstance();
                fc.getApplication().getNavigationHandler().handleNavigation(fc, null, paginaRedireccionar);
            }else{
                this.aplicaEventosPagina("Usuario no es válido", "El usuario o contraseña ingresados no son válidos", null, null);
            }
        }                 

    }

    public boolean validarUsuario() {
//        Usuario usValidado = new Usuario();
        try {
            usuariofl.setNumQuery(3);
            Usuario usValidado = usuariofl.findOnewithParameters(usuario);
            if (usValidado != null) {
                System.out.println("Usuario validado: " + usValidado.getUsuarioNombre());
                usuario = usValidado;
                return true;
            }
        } catch (Exception e) {
            paginaRedireccionar = "login";
            e.printStackTrace();
        }

        return false;
    }

    public void verificarGrupo() {

        System.out.println("Grupo: " + usuario.getGrupoID().getGrupoNombre());
        String grupoNmobre = usuario.getGrupoID().getGrupoNombre();

        if (grupoNmobre.equalsIgnoreCase("Administradores")){
            menu.menuAdmin();
        } else if (grupoNmobre.equalsIgnoreCase("Lectores")) {
            menu.menuLectores();
        }
        this.paginaRedireccionar = "index";
        
    }
    
    public String cerrarSesion(){
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        
        return "/faces/login";
    }
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
        
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public MenuFilter getMenu() {
        return menu;
    }

    public void setMenu(MenuFilter menu) {
        this.menu = menu;
    }

}

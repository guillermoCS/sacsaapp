/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.sacsaapp.entities.Abono;
import org.sacsaapp.entities.Cliente;
import org.sacsaapp.entities.CuentaPorCobrar;
import org.sacsaapp.interfaces.AbonoFacadeLocal;
import org.sacsaapp.interfaces.ClienteFacadeLocal;
import org.sacsaapp.interfaces.CuentaPorCobrarFacadeLocal;

/**
 *
 * @author deguevara
 * @date 11-25-2015
 * @time 08:26:46 PM
 */
@ManagedBean(name = "cuentaPorCobrarMB")
@ViewScoped
public class CuentaPorCobrarMB {

    @EJB
    private ClienteFacadeLocal clienteFacade;
    @EJB
    private CuentaPorCobrarFacadeLocal cuentaPorCobrarFacade;

    @EJB
    private AbonoFacadeLocal abonoFacade;
    private List<CuentaPorCobrar> listaCuentasPorCobrar;
    private List<Cliente> listaClientes;
    private List<Abono> listaAbonosCliente;
    private CuentaPorCobrar cuentaNueva;
    private CuentaPorCobrar cuentaSeleccionada;
    private Cliente clienteAdd;
    private Cliente clienteDetalle;
    private boolean mostrarInfoCuentas;

    private String idCliente;

    public CuentaPorCobrarMB() {

    }

    @PostConstruct
    public void init() {
        clienteAdd = new Cliente();
        listaCuentasPorCobrar = new ArrayList<>();
        listaClientes = new ArrayList<>();
        cuentaNueva = new CuentaPorCobrar();
        cuentaSeleccionada = new CuentaPorCobrar();
        mostrarInfoCuentas = false;
        clienteDetalle = new Cliente();
//        obtenerContadores();
//        obtenerCuentasPorCobrar();
    }

    public void obtenerCuentasCliente() {
        mostrarInfoCuentas = true;
        List<Cliente> allClients = clienteFacade.findAll();
        System.out.println("todos los clinetes: " + allClients.size());
        Cliente clienteAbuscarCuentas = new Cliente();
        for (int i = 0; i < allClients.size(); i++) {
            Cliente cliente = allClients.get(i);
            if (cliente.getPersonaID().toString().toLowerCase().contains(idCliente.toLowerCase())) {
                System.out.println("Agregando: " + cliente.getPersonaID());
                clienteAbuscarCuentas = cliente;
            }
        }
        clienteDetalle = clienteAbuscarCuentas;
        CuentaPorCobrar cuentaFiltro = new CuentaPorCobrar();
        ArrayList<String> arrayOne = new ArrayList();
        arrayOne.add("numCuenta");
        ArrayList arrayTwo = new ArrayList();
        arrayTwo.add(clienteAbuscarCuentas.getClienteNumeroCuenta());
        System.out.println("array " + arrayTwo);
        System.out.println("array " + arrayOne);
        cuentaPorCobrarFacade.setNumQuery(3);
        listaCuentasPorCobrar = cuentaPorCobrarFacade.findwithParameters(cuentaFiltro, arrayOne, arrayTwo);
        System.out.println("Total de cuentas cliente: " + listaCuentasPorCobrar.size());
    }

    public List<Cliente> completeClientes(String query) {
        List<Cliente> allClients = clienteFacade.findAll();
        List<Cliente> filteredClients = new ArrayList<Cliente>();
        for (int i = 0; i < allClients.size(); i++) {
            Cliente cliente = allClients.get(i);
            if (cliente.getPersonaID().toString().toLowerCase().startsWith(query)) {
                filteredClients.add(cliente);
            }
        }
        return filteredClients;
    }

    public List<String> completeText(String query) {
        System.out.println("Completando con: " + query);
        List<Cliente> allClients = clienteFacade.findAll();
        System.out.println("todos los clinetes: " + allClients.size());
        List<Cliente> filteredClients = new ArrayList<Cliente>();
        for (int i = 0; i < allClients.size(); i++) {
            Cliente cliente = allClients.get(i);
            System.out.println("Cliente: " + cliente);
            System.out.println("Persona: " + cliente.getPersonaID());
            if (cliente.getPersonaID().toString().toLowerCase().contains(query.toLowerCase())) {
                System.out.println("Agregando: " + cliente.getPersonaID());
                filteredClients.add(cliente);
            }
        }
        System.out.println("Clientes filtrados: " + filteredClients.size());
        List<String> results = new ArrayList<String>();
        for (Cliente string : filteredClients) {
            results.add(string.getPersonaID().toString());
        }
        System.out.println("resultados " + results.size());
        return results;
    }

    public void verAbonos(CuentaPorCobrar cobrar) {
        cuentaSeleccionada = cobrar;
        Abono abonoBuscar = new Abono();
        abonoBuscar.setCuentasporCobrarClientescuenta(cobrar);
        listaAbonosCliente = abonoFacade.filtrarPorNumeroCuenta(abonoBuscar);
        System.out.println("Total de abonos: " + listaAbonosCliente.size() + " para cuenta " + cobrar);
    }

    public void agergarCuenta() {
        cuentaNueva.setClientesclienteNumeroCuenta(clienteDetalle);
        cuentaNueva.setEstadoCuenta(true);
        try {
            cuentaPorCobrarFacade.create(cuentaNueva);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agregada", "Cuenta agregada exitosamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CuentaPorCobrar> getListaCuentasPorCobrar() {
        return listaCuentasPorCobrar;
    }

    public void setListaCuentasPorCobrar(List<CuentaPorCobrar> listaCuentasPorCobrar) {
        this.listaCuentasPorCobrar = listaCuentasPorCobrar;
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public CuentaPorCobrar getCuentaNueva() {
        return cuentaNueva;
    }

    public void setCuentaNueva(CuentaPorCobrar cuentaNueva) {
        this.cuentaNueva = cuentaNueva;
    }

    public CuentaPorCobrar getCuentaSeleccionada() {
        return cuentaSeleccionada;
    }

    public void setCuentaSeleccionada(CuentaPorCobrar cuentaSeleccionada) {
        this.cuentaSeleccionada = cuentaSeleccionada;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the clienteAdd
     */
    public Cliente getClienteAdd() {
        return clienteAdd;
    }

    /**
     * @param clienteAdd the clienteAdd to set
     */
    public void setClienteAdd(Cliente clienteAdd) {
        this.clienteAdd = clienteAdd;
    }

    public boolean isMostrarInfoCuentas() {
        return mostrarInfoCuentas;
    }

    public void setMostrarInfoCuentas(boolean mostrarInfoCuentas) {
        this.mostrarInfoCuentas = mostrarInfoCuentas;
    }

    public List<Abono> getListaAbonosCliente() {
        return listaAbonosCliente;
    }

    public void setListaAbonosCliente(List<Abono> listaAbonosCliente) {
        this.listaAbonosCliente = listaAbonosCliente;
    }

    public Cliente getClienteDetalle() {
        return clienteDetalle;
    }

    public void setClienteDetalle(Cliente clienteDetalle) {
        this.clienteDetalle = clienteDetalle;
    }

}

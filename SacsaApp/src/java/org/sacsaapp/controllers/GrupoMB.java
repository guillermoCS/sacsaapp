/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.sacsaapp.entities.Grupo;
import org.sacsaapp.interfaces.GrupoFacadeLocal;

/**
 *
 * @author deguevara
 * @date 11-06-2015
 * @time 08:41:29 PM
 */
@ManagedBean
@ViewScoped
public class GrupoMB {
    
    
    @EJB
    private GrupoFacadeLocal grupolf;
    
    private Grupo grupo;
    private Grupo grupoSeleccionado;
    private List<Grupo> lsGrupos;
    
    
    
    /**
     * Creates a new instance of GrupoMB
     */
    public GrupoMB() {
    }
    
    @PostConstruct
    public void init(){
        setGrupo(new Grupo());
        setGrupoSeleccionado(new Grupo());
        lsGrupos = new ArrayList<>();
        llenarListaGrupos();
        
    }
    
    public void llenarListaGrupos(){
        setLsGrupos(grupolf.findAll());
    }

    
    public void agregarGrupo(){
        System.out.println("Agregando Grupo");
        
        try {
            grupo.setGrupoEstado(true);
            grupolf.create(grupo);
            setGrupo(new Grupo());
            llenarListaGrupos();
        } catch (Exception e) {
            
        }
        this.aplicaEventosPagina("Éxito", "Servicio Agregado con Satisfacción", "frmGrupos:tabGrupos", "PF('grupoDialog').hide();");        
        this.aplicaEventosPagina(null, null, "frmAddGrupo:GrupoDetail", null);
    }
    
    public void modificarServicio(Grupo s){
        System.out.println("Modificando Servicio");
    }
    
    public void aplicandoCmabiosServicio(){
        
    }
    
    /*
    *   Método que envía mensajes a la página y actualiza componentes además puede ejecutar javascript.
    *   msgA = mensaje encabezado
    *   msgB = descripción del mensaje
    *   msgC = componente a actualizar en la página
    *   msgD = codigo a ejecutar
    *   
    */
    public void aplicaEventosPagina(String msgA, String msgB, String msgC, String msgD) {
        
        if(msgA != null && msgB != null){
            FacesMessage msg = new FacesMessage(msgA, msgB);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }      
        
        RequestContext context = RequestContext.getCurrentInstance();
        if(msgC != null){
            context.update(msgC);
        }
        if(msgD != null){
            context.execute(msgD);
        }
        
    }

    /**
     * @return the grupo
     */
    public Grupo getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    /**
     * @return the grupoSeleccionado
     */
    public Grupo getGrupoSeleccionado() {
        return grupoSeleccionado;
    }

    /**
     * @param grupoSeleccionado the grupoSeleccionado to set
     */
    public void setGrupoSeleccionado(Grupo grupoSeleccionado) {
        this.grupoSeleccionado = grupoSeleccionado;
    }

    /**
     * @return the lsGrupos
     */
    public List<Grupo> getLsGrupos() {
        return lsGrupos;
    }

    /**
     * @param lsGrupos the lsGrupos to set
     */
    public void setLsGrupos(List<Grupo> lsGrupos) {
        this.lsGrupos = lsGrupos;
    }
    

}

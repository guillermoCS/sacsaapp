/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.sacsaapp.entities.Abono;
import org.sacsaapp.entities.Persona;
import org.sacsaapp.entities.TipoDocumento;
import org.sacsaapp.interfaces.TipoDocumentoFacadeLocal;

/**
 *
 * @author Diana
 */
@ManagedBean
@RequestScoped
public class TipoDocumentoMB {

    @EJB
    private TipoDocumentoFacadeLocal tipoDocumentoFacade;

    TipoDocumento tipoDoc;
    private TipoDocumento tipoDocSeleccionado;
    private TipoDocumento dSeleccionado;

    private List<TipoDocumento> listaDoc;
    private boolean skip;

    /**
     * Creates a new instance of TipoDocumentoMB
     */
    public TipoDocumentoMB() {
    }

    @PostConstruct
    public void init() {
        dSeleccionado = new TipoDocumento();
        tipoDocSeleccionado = new TipoDocumento();
        tipoDoc = new TipoDocumento();
        listaDoc = new ArrayList<>();
        ConsultarTipoDocumento();
        
    }

    private void ConsultarTipoDocumento() {
        listaDoc = tipoDocumentoFacade.findAll();

    }

    public void llenarLista() {
        listaDoc = tipoDocumentoFacade.findAll();
    }

    public void agregarTipoDocumento() {
        System.out.println("datos: " + tipoDoc.getDocumentoID() + " , " + tipoDoc.getTipo());
        tipoDocumentoFacade.create(tipoDoc);
        tipoDoc = new TipoDocumento();

        llenarLista();

        FacesMessage msg = new FacesMessage("Éxito", "´Tipo de documento ingresado satisfactoriamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("frmTipoDoc:tabTipoDocumento");
        context.execute("PF('dlg1').hide();");

    }

    public void cargarPersona() {
        System.out.println("Cargamos un OBJ");
        System.out.println("pseleccionada" + tipoDocSeleccionado.toString());
        tipoDocSeleccionado = tipoDocSeleccionado;
        System.out.println("pseleccionada" + tipoDoc.toString());
    }

    /*
     public void modificartipoDocumento() {
        
     tipoDocumentoFacade.edit(tipoDocSeleccionado);
     tipoDocSeleccionado = new TipoDocumento();
     llenarLista();

     FacesMessage msg = new FacesMessage("Éxito", "Tipo de documento modificado correctamente");
     FacesContext.getCurrentInstance().addMessage(null, msg);
     RequestContext context = RequestContext.getCurrentInstance();
     context.execute("PF('dlg2').hide();");
     context.update("frmTipoDoc:tabTipoDocumento");

     }
     */
    public void modificartipoDocumento() {

        tipoDocumentoFacade.edit(tipoDoc);
        tipoDoc = new TipoDocumento();
        llenarLista();

        FacesMessage msg = new FacesMessage("Éxito", "Tipo de documento modificado correctamente");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('dlg2').hide();");
        context.update("frmTipoDoc:tabTipoDocumento");

    }
    
        public void eliminarTipoDocumento() {
        System.out.println("persona selected:" + dSeleccionado.toString());
        tipoDocumentoFacade.remove(dSeleccionado);
        dSeleccionado = new TipoDocumento();
    }
        

    public List<TipoDocumento> getListaDoc() {
        return listaDoc;
    }

    public void setListaDoc(List<TipoDocumento> listaDoc) {
        this.listaDoc = listaDoc;
    }

    public TipoDocumento getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(TipoDocumento tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public TipoDocumento getTipoDocSeleccionado() {
        return tipoDocSeleccionado;
    }

    public void setTipoDocSeleccionado(TipoDocumento tipoDocSeleccionado) {
        this.tipoDocSeleccionado = tipoDocSeleccionado;
    }

    public TipoDocumento getdSeleccionado() {
        return dSeleccionado;
    }

    public void setdSeleccionado(TipoDocumento dSeleccionado) {
        this.dSeleccionado = dSeleccionado;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Contador;

/**
 *
 * @author guillermo
 */
@Local
public interface ContadorFacadeLocal {

    void create(Contador contador);

    void edit(Contador contador);

    void remove(Contador contador);

    Contador find(Object id);

    List<Contador> findAll();

    List<Contador> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Contador> findwithParameters(Contador contador);
    
    List<Contador> findwithParameters(Contador contador, ArrayList<String> indices, ArrayList valores);
    
    Contador findOnewithParameters(Contador contador); 
    
}

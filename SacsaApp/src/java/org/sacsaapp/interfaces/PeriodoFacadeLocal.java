/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Periodo;

/**
 *
 * @author guillermo
 */
@Local
public interface PeriodoFacadeLocal {

    void create(Periodo periodo);

    void edit(Periodo periodo);

    void remove(Periodo periodo);

    Periodo find(Object id);

    List<Periodo> findAll();

    List<Periodo> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Periodo> findwithParameters(Periodo periodo);
    
    List<Periodo> findwithParameters(Periodo periodo, ArrayList<String> indices, ArrayList valores);
    
    Periodo findOnewithParameters(Periodo periodo); 
    
}

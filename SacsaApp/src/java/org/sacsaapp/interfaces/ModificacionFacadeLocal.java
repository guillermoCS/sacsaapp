/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Modificacion;

/**
 *
 * @author guillermo
 */
@Local
public interface ModificacionFacadeLocal {

    void create(Modificacion modificacion);

    void edit(Modificacion modificacion);

    void remove(Modificacion modificacion);

    Modificacion find(Object id);

    List<Modificacion> findAll();

    List<Modificacion> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Modificacion> findwithParameters(Modificacion modificacion);
    
    List<Modificacion> findwithParameters(Modificacion modificacion, ArrayList<String> indices, ArrayList valores);
    
    Modificacion findOnewithParameters(Modificacion modificacion); 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.CuentaPorCobrar;

/**
 *
 * @author guillermo
 */
@Local
public interface CuentaPorCobrarFacadeLocal {

    void create(CuentaPorCobrar cuentaPorCobrar);

    void edit(CuentaPorCobrar cuentaPorCobrar);

    void remove(CuentaPorCobrar cuentaPorCobrar);

    CuentaPorCobrar find(Object id);

    List<CuentaPorCobrar> findAll();

    List<CuentaPorCobrar> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<CuentaPorCobrar> findwithParameters(CuentaPorCobrar cuentaPorCobrar);
    
    List<CuentaPorCobrar> findwithParameters(CuentaPorCobrar cuentaPorCobrar, ArrayList<String> indices, ArrayList valores);
    
    CuentaPorCobrar findOnewithParameters(CuentaPorCobrar cuentaPorCobrar); 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Parametro;

/**
 *
 * @author guillermo
 */
@Local
public interface ParametroFacadeLocal {

    void create(Parametro parametro);

    void edit(Parametro parametro);

    void remove(Parametro parametro);

    Parametro find(Object id);

    List<Parametro> findAll();

    List<Parametro> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Parametro> findwithParameters(Parametro parametro);
    
    List<Parametro> findwithParameters(Parametro parametro, ArrayList<String> indices, ArrayList valores);
    
    Parametro findOnewithParameters(Parametro parametro); 
    
}

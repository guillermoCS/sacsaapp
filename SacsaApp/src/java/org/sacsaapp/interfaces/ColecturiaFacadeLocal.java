/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Colecturia;

/**
 *
 * @author guillermo
 */
@Local
public interface ColecturiaFacadeLocal {

    void create(Colecturia colecturia);

    void edit(Colecturia colecturia);

    void remove(Colecturia colecturia);

    Colecturia find(Object id);

    List<Colecturia> findAll();

    List<Colecturia> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Colecturia> findwithParameters(Colecturia colecturia);
    
    List<Colecturia> findwithParameters(Colecturia colecturia, ArrayList<String> indices, ArrayList valores);
    
    Colecturia findOnewithParameters(Colecturia colecturia); 
}

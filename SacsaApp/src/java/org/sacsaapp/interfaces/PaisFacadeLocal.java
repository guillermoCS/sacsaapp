/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Pais;

/**
 *
 * @author guillermo
 */
@Local
public interface PaisFacadeLocal {

    void create(Pais pais);

    void edit(Pais pais);

    void remove(Pais pais);

    Pais find(Object id);

    List<Pais> findAll();

    List<Pais> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Pais> findwithParameters(Pais pais);
    
    List<Pais> findwithParameters(Pais pais, ArrayList<String> indices, ArrayList valores);
    
    Pais findOnewithParameters(Pais pais); 
    
}

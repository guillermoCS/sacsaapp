/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.HistoricoLectura;

/**
 *
 * @author guillermo
 */
@Local
public interface HistoricoLecturaFacadeLocal {

    void create(HistoricoLectura historicoLectura);

    void edit(HistoricoLectura historicoLectura);

    void remove(HistoricoLectura historicoLectura);

    HistoricoLectura find(Object id);

    List<HistoricoLectura> findAll();

    List<HistoricoLectura> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<HistoricoLectura> findwithParameters(HistoricoLectura historicoLectura);
    
    List<HistoricoLectura> findwithParameters(HistoricoLectura historicoLectura, ArrayList<String> indices, ArrayList valores);
    
    HistoricoLectura findOnewithParameters(HistoricoLectura historicoLectura); 
    
}

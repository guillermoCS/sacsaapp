/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.TipoDocumento;

/**
 *
 * @author guillermo
 */
@Local
public interface TipoDocumentoFacadeLocal {

    void create(TipoDocumento tipoDocumento);

    void edit(TipoDocumento tipoDocumento);

    void remove(TipoDocumento tipoDocumento);

    TipoDocumento find(Object id);

    List<TipoDocumento> findAll();

    List<TipoDocumento> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<TipoDocumento> findwithParameters(TipoDocumento tipoDocumento);
    
    List<TipoDocumento> findwithParameters(TipoDocumento tipoDocumento, ArrayList<String> indices, ArrayList valores);
    
    TipoDocumento findOnewithParameters(TipoDocumento tipoDocumento); 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Grupo;

/**
 *
 * @author guillermo
 */
@Local
public interface GrupoFacadeLocal {

    void create(Grupo grupo);

    void edit(Grupo grupo);

    void remove(Grupo grupo);

    Grupo find(Object id);

    List<Grupo> findAll();

    List<Grupo> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Grupo> findwithParameters(Grupo grupo);
    
    List<Grupo> findwithParameters(Grupo grupo, ArrayList<String> indices, ArrayList valores);
    
    Grupo findOnewithParameters(Grupo grupo); 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Persona;

/**
 *
 * @author guillermo
 */
@Local
public interface PersonaFacadeLocal {

    void create(Persona persona);

    void edit(Persona persona);

    void remove(Persona persona);

    Persona find(Object id);

    List<Persona> findAll();
    
    List<Persona> findByNameQuery(String nameQuery);

    List<Persona> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Persona> findwithParameters(Persona persona);
    
    List<Persona> findwithParameters(Persona persona, ArrayList<String> indices, ArrayList valores);
    
    Persona findOnewithParameters(Persona persona); 
    
}

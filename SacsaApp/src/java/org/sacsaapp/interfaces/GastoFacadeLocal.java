/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Gasto;

/**
 *
 * @author guillermo
 */
@Local
public interface GastoFacadeLocal {

    void create(Gasto gasto);

    void edit(Gasto gasto);

    void remove(Gasto gasto);

    Gasto find(Object id);

    List<Gasto> findAll();

    List<Gasto> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Gasto> findwithParameters(Gasto gasto);
    
    List<Gasto> findwithParameters(Gasto gasto, ArrayList<String> indices, ArrayList valores);
    
    Gasto findOnewithParameters(Gasto gasto); 
    
}

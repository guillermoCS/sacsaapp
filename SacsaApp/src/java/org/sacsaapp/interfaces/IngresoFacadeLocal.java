/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Ingreso;

/**
 *
 * @author guillermo
 */
@Local
public interface IngresoFacadeLocal {

    void create(Ingreso ingreso);

    void edit(Ingreso ingreso);

    void remove(Ingreso ingreso);

    Ingreso find(Object id);

    List<Ingreso> findAll();

    List<Ingreso> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Ingreso> findwithParameters(Ingreso ingreso);
    
    List<Ingreso> findwithParameters(Ingreso ingreso, ArrayList<String> indices, ArrayList valores);
    
    Ingreso findOnewithParameters(Ingreso ingreso); 
    
}

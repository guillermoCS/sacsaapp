/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Lectura;

/**
 *
 * @author guillermo
 */
@Local
public interface LecturaFacadeLocal {

    void create(Lectura lectura);

    void edit(Lectura lectura);

    void remove(Lectura lectura);

    Lectura find(Object id);

    List<Lectura> findAll();

    List<Lectura> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Lectura> findwithParameters(Lectura lectura);
    
    List<Lectura> findwithParameters(Lectura lectura, ArrayList<String> indices, ArrayList valores);
    
    Lectura findOnewithParameters(Lectura lectura); 
    
}

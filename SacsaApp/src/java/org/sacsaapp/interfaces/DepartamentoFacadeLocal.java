/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Departamento;

/**
 *
 * @author guillermo
 */
@Local
public interface DepartamentoFacadeLocal {

    void create(Departamento departamento);

    void edit(Departamento departamento);

    void remove(Departamento departamento);

    Departamento find(Object id);

    List<Departamento> findAll();

    List<Departamento> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Departamento> findwithParameters(Departamento depto);
    
    List<Departamento> findwithParameters(Departamento depto, ArrayList<String> indices, ArrayList valores);
    
    Departamento findOnewithParameters(Departamento depto); 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Directiva;

/**
 *
 * @author guillermo
 */
@Local
public interface DirectivaFacadeLocal {

    void create(Directiva directiva);

    void edit(Directiva directiva);

    void remove(Directiva directiva);

    Directiva find(Object id);

    List<Directiva> findAll();

    List<Directiva> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Directiva> findwithParameters(Directiva directiva);
    
    List<Directiva> findwithParameters(Directiva directiva, ArrayList<String> indices, ArrayList valores);
    
    Directiva findOnewithParameters(Directiva directiva); 
    
}

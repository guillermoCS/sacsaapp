/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Municipio;

/**
 *
 * @author guillermo
 */
@Local
public interface MunicipioFacadeLocal {

    void create(Municipio municipio);

    void edit(Municipio municipio);

    void remove(Municipio municipio);

    Municipio find(Object id);

    List<Municipio> findAll();

    List<Municipio> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Municipio> findwithParameters(Municipio municipio);
    
    List<Municipio> findwithParameters(Municipio municipio, ArrayList<String> indices, ArrayList valores);
    
    Municipio findOnewithParameters(Municipio municipio); 
    
}

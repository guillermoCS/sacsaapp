/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Servicio;

/**
 *
 * @author guillermo
 */
@Local
public interface ServicioFacadeLocal {

    void create(Servicio servicio);

    void edit(Servicio servicio);

    void remove(Servicio servicio);

    Servicio find(Object id);

    List<Servicio> findAll();

    List<Servicio> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Servicio> findwithParameters(Servicio servicio);
    
    List<Servicio> findwithParameters(Servicio servicio, ArrayList<String> indices, ArrayList valores);
    
    Servicio findOnewithParameters(Servicio servicio); 
    
}

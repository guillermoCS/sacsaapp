/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Direccion;

/**
 *
 * @author guillermo
 */
@Local
public interface DireccionFacadeLocal {

    void create(Direccion direccion);

    void edit(Direccion direccion);

    void remove(Direccion direccion);

    Direccion find(Object id);

    List<Direccion> findAll();

    List<Direccion> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Direccion> findwithParameters(Direccion direccion);
    
    List<Direccion> findwithParameters(Direccion direccion, ArrayList<String> indices, ArrayList valores);
    
    Direccion findOnewithParameters(Direccion direccion); 
    
}

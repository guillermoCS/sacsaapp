/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Abono;

/**
 *
 * @author guillermo
 */
@Local
public interface AbonoFacadeLocal {

    void create(Abono abono);

    void edit(Abono abono);

    void remove(Abono abono);

    Abono find(Object id);

    List<Abono> findAll();

    List<Abono> findRange(int[] range);
    
    public List<Abono> filtrarPorNumeroCuenta(Abono ob);

    int count();
    
    void setNumQuery(int valor);
    
    List<Abono> findwithParameters(Abono abono);
    
    List<Abono> findwithParameters(Abono abono, ArrayList<String> indices, ArrayList valores);
    
    Abono findOnewithParameters(Abono abono);    
    
}

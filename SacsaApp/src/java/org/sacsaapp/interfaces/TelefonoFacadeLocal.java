/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Telefono;

/**
 *
 * @author guillermo
 */
@Local
public interface TelefonoFacadeLocal {

    void create(Telefono telefono);

    void edit(Telefono telefono);

    void remove(Telefono telefono);

    Telefono find(Object id);

    List<Telefono> findAll();

    List<Telefono> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Telefono> findwithParameters(Telefono telefono);
    
    List<Telefono> findwithParameters(Telefono telefono, ArrayList<String> indices, ArrayList valores);
    
    Telefono findOnewithParameters(Telefono telefono); 
    
}

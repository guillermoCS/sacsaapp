/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.Afiliado;

/**
 *
 * @author guillermo
 */
@Local
public interface AfiliadoFacadeLocal {

    public List<Afiliado> filtrarPorIDAfiliado(Afiliado ob);

    void create(Afiliado afiliado);

    void edit(Afiliado afiliado);

    void remove(Afiliado afiliado);

    Afiliado find(Object id);

    List<Afiliado> findAll();

    List<Afiliado> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<Afiliado> findwithParameters(Afiliado afiliado);
    
    List<Afiliado> findwithParameters(Afiliado afiliado, ArrayList<String> indices, ArrayList valores);
    
    Afiliado findOnewithParameters(Afiliado afiliado); 

}

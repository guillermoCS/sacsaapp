/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.DetalleGasto;

/**
 *
 * @author guillermo
 */
@Local
public interface DetalleGastoFacadeLocal {

    void create(DetalleGasto detalleGasto);

    void edit(DetalleGasto detalleGasto);

    void remove(DetalleGasto detalleGasto);

    DetalleGasto find(Object id);

    List<DetalleGasto> findAll();

    List<DetalleGasto> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<DetalleGasto> findwithParameters(DetalleGasto detalleGasto);
    
    List<DetalleGasto> findwithParameters(DetalleGasto detalleGasto, ArrayList<String> indices, ArrayList valores);
    
    DetalleGasto findOnewithParameters(DetalleGasto detalleGasto); 
    
}

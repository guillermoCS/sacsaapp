/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;
import org.sacsaapp.entities.DetalleServicio;

/**
 *
 * @author guillermo
 */
@Local
public interface DetalleServicioFacadeLocal {

    void create(DetalleServicio detalleServicio);

    void edit(DetalleServicio detalleServicio);

    void remove(DetalleServicio detalleServicio);

    DetalleServicio find(Object id);

    List<DetalleServicio> findAll();

    List<DetalleServicio> findRange(int[] range);

    int count();
    
    void setNumQuery(int valor);
    
    List<DetalleServicio> findwithParameters(DetalleServicio detalleServicio);
    
    List<DetalleServicio> findwithParameters(DetalleServicio detalleServicio, ArrayList<String> indices, ArrayList valores);
    
    DetalleServicio findOnewithParameters(DetalleServicio detalleServicio); 
    
}

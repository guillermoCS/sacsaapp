/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.procesos;

import javax.ejb.Local;

/**
 *
 * @author guillermo
 */
@Local
public interface ProcesoFacturacionAutoLocal {
    
    public void setDiaDelMes(int diaDelMes);
    
    public void setHoraInicio(int horaInicio);
    
    public void programarProceso();
    
    public boolean terminarProceso(String nomProceso);
    
}

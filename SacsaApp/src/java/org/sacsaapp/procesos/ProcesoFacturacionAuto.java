/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.procesos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import org.sacsaapp.entities.Colecturia;
import org.sacsaapp.entities.DetalleServicio;
import org.sacsaapp.entities.DetalleServicioPK;
import org.sacsaapp.entities.HistoricoLectura;
import org.sacsaapp.entities.Lectura;
import org.sacsaapp.entities.Parametro;
import org.sacsaapp.entities.Servicio;
import org.sacsaapp.interfaces.ColecturiaFacadeLocal;
import org.sacsaapp.interfaces.DetalleServicioFacadeLocal;
import org.sacsaapp.interfaces.HistoricoLecturaFacadeLocal;
import org.sacsaapp.interfaces.LecturaFacadeLocal;
import org.sacsaapp.interfaces.ParametroFacadeLocal;
import org.sacsaapp.interfaces.ServicioFacadeLocal;

/**
 *
 * @author guillermo
 */
@Singleton
@LocalBean
public class ProcesoFacturacionAuto implements ProcesoFacturacionAutoLocal {

    @Resource
    private TimerService timerService;
    
    @EJB
    private LecturaFacadeLocal lecturaF;
    
    @EJB
    private ColecturiaFacadeLocal colecturiaF;
    
    @EJB
    private DetalleServicioFacadeLocal detalleServicioF;
    
    @EJB
    private ServicioFacadeLocal servicosF;
    
    @EJB
    private ParametroFacadeLocal paramsF;
    
    @EJB
    private HistoricoLecturaFacadeLocal historicoF;
    
    private int diaDelMes;
    private int horaInicio;
    
    private List<Lectura> lsLecturas;
    private Lectura lectura;
    
    private HistoricoLectura historico;
    
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    private String fechaActual;
    private String anyoActual;
    private String mesActual;
    
    
    // CORRELATIVO
    Date fechaAct;
    String fechaFormat;
    ArrayList<String> correlativo;
    
    @Override
    public void programarProceso(){
        TimerConfig timerConfig = new TimerConfig();
        timerConfig.setInfo("ProcesoCobrosFacturas");
        ScheduleExpression schedule = new ScheduleExpression();
        schedule.dayOfMonth(diaDelMes);
        System.out.println("Hora " + horaInicio);
        schedule.hour(horaInicio); //.minute("*").second("13,34,57");
        timerService.createCalendarTimer(schedule, timerConfig);
    }

    @Timeout
    public void execute(Timer timer) {
        System.out.println("Timer Service : " + timer.getInfo());
        System.out.println("Iniciando la ejecucion de cobros Automáticos");
        imprimirDatosEnConsola();
        System.out.println("____________________________________________");   
    }


    @Override
    public void setDiaDelMes(int diaDelMes) {
        this.diaDelMes = diaDelMes;
    }   
    
    public boolean terminarProceso(String nomProceso){
        boolean resp = false;
        for (Object obj : timerService.getTimers()) {
            Timer t = (Timer) obj;
            if (t.getInfo().equals(nomProceso)){
                t.cancel();
                resp = true;
            }
        }
        return resp;
    }
    
    public void imprimirDatosEnConsola(){
        lsLecturas = new ArrayList();
        ArrayList<String> arrayOne = new ArrayList();
        arrayOne.add("anyo");
        arrayOne.add("mes");
        ArrayList arrayTwo = new ArrayList();
        Date fecha = new Date();
        fechaActual = format.format(fecha);
        System.out.println("fecha Actual 1 : " + fechaActual);
        anyoActual = fechaActual.substring(0, 4);
        mesActual = fechaActual.substring(5, 7);
        System.out.println("Fecha Lectura: " + anyoActual + " - " + mesActual);
        arrayTwo.add(anyoActual);
        arrayTwo.add(mesActual);
        lectura = new Lectura();
        lecturaF.setNumQuery(7);
        lsLecturas = lecturaF.findwithParameters(lectura, arrayOne, arrayTwo);
        
        Parametro p = paramsF.find(1);
        correlativo = new ArrayList();
        
        for (Lectura l : lsLecturas) {
            System.out.println("ID: " + l.getLecturaID());
            System.out.println("fecha Final: " + l.getPeriodoFinal());
            System.out.println("Cuenta: " + l.getClienteCuenta());
            try {
                // Correlativo 
                String correlativoFinal = "";
                while (correlativoFinal.equalsIgnoreCase("")){
                    correlativoFinal = generarCodigo();
                }
                Colecturia c = new Colecturia(obtenerValorCobro( p, obtenerDeudaCliente(l), obtenerMesesSinPagar(l)), true, obtenerDeudaCliente(l), obtenerMesesSinPagar(l), fecha, correlativoFinal);
                c.setLecturaID(l);
                colecturiaF.create(c);

                DetalleServicio detServicios = new DetalleServicio();
                detServicios.setDetalleServicioPK(new DetalleServicioPK(1, c.getColecturiaID()));
                detServicios.setColecturia(c);
                detServicios.setServicio(new Servicio(1));
                detServicios.setValorCobro(p.getCuotaBase());
                detalleServicioF.create(detServicios);
                
                if ( obtenerDeudaCliente(l) != 0 && obtenerMesesSinPagar(l) != 0 ){
                    detServicios = new DetalleServicio();
                    detServicios.setDetalleServicioPK(new DetalleServicioPK(3, c.getColecturiaID()));
                    detServicios.setColecturia(c);
                    detServicios.setServicio(new Servicio(3));
                    detServicios.setValorCobro(obtenerDeudaCliente(l));
                    detalleServicioF.create(detServicios);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
                
        }
        
    }
    
    public double obtenerDeudaCliente(Lectura lec){
        double valor = 0;
        historico = new HistoricoLectura();
        historico = obtenerHistorialMesAnterior(lec);
        if (historico != null){
            valor = historico.getClienteMDeuda();
        }
        return valor;
    } 
    
    public int obtenerMesesSinPagar(Lectura lec){
        int valor = 0;
        historico = new HistoricoLectura();
        historico = obtenerHistorialMesAnterior(lec);
        if (historico != null){
            valor = historico.getClienteMNumMsPagar();
        }
        return valor;
    } 
    
    public double obtenerValorCobro(Parametro par, double valorD, int valorI){        
        double valor = 0;
       if ( valorI != 0 && valorD != 0 ){
           valor = par.getCuotaBase() + valorD;
       }else {
           valor = par.getCuotaBase();
       }       
        
        return valor;
    } 

    public HistoricoLectura obtenerHistorialMesAnterior(Lectura lec){
        HistoricoLectura h = new HistoricoLectura();
        h.setLecturaID(lec.getLecturaID());
        historicoF.setNumQuery(3);
        
        Date fecha = new Date();
        fechaActual = format.format(fecha);
        System.out.println("fecha Actual 2 : " + fechaActual);
        anyoActual = fechaActual.substring(0, 4);
        mesActual = fechaActual.substring(5, 7);
        
        if (Integer.parseInt(mesActual) == 1){
            mesActual = String.valueOf(12); // Si el mes es 1 debe guardar el mes 12 ....
        }else {
            mesActual = String.valueOf(Integer.parseInt(mesActual)-1); // al mes se le resta uno para obtener el mes anterior.
        }
        
        System.out.println("Fecha a Usar: " + anyoActual + " - " + mesActual);
        // Lista de nombres de parámetros en la named query que se usaran.
        ArrayList<String> arrayOne = new ArrayList();
        arrayOne.add("anyo");
        arrayOne.add("mes");
        arrayOne.add("numCuenta");
        // Lista de valores a poner en la named Query
        ArrayList arrayTwo = new ArrayList();
        arrayTwo.add(anyoActual);
        arrayTwo.add(mesActual);
        arrayTwo.add(lec.getClienteCuenta().getClienteNumeroCuenta());
        System.out.println("Array " + arrayTwo);
        // Se manda a llamar el objeto historia del mes anterior para obtener los datos necesarios...
        List<HistoricoLectura> lisHistoricos = new ArrayList<>();
        lisHistoricos = historicoF.findwithParameters(h, arrayOne, arrayTwo);
        System.out.println("Tamaño Lista: " + lisHistoricos.size());
        h = new HistoricoLectura();
        for (HistoricoLectura his : lisHistoricos) {
            h = his;
        }
        return h;
    }
    //CORRELATIVO
    public String generarCodigo() {
        fechaAct = new Date();
        fechaFormat = format.format(fechaAct);
        String anyo = fechaFormat.substring(0, 4);
        String mes = fechaFormat.substring(5, 7);
        String dia = fechaFormat.substring(8);

        System.out.println("Fecha: " + anyo + "-" + mes + "-" + dia);

        String dato = generarAlfanum();
        if (dato.equalsIgnoreCase("")) {
            System.out.println("El valor salio repetido");
            return "";
        } else {
            System.out.println("El valor es: " + dato.concat(generarFecha(anyo, mes, dia)));
            return dato.concat(generarFecha(anyo, mes, dia));
        }
        
    }

    public String generarAlfanum() {
        String valor;
        valor = String.valueOf(obtenerNumeroAleatorio())
                .concat(obtenerLetraAleatoria())
                .concat(String.valueOf(obtenerNumeroAleatorio()))
                .concat(obtenerLetraAleatoria())
                .concat(String.valueOf(obtenerNumeroAleatorio()));

        if (correlativo.size() == 0) {
            correlativo.add(valor);
            return valor;
        } else {
            if (validarNumero(valor)) {
                correlativo.add(valor);
                return valor;
            }
        }
        return "";
    }

    public int obtenerNumeroAleatorio() {
        return (int) (Math.random() * (9 - 1 + 1) + 1);

    }

    public String obtenerLetraAleatoria() {
        char caracter;
        caracter = (char) (int) (Math.random() * (97 - 122 + 1) + 122);
        return String.valueOf(caracter).toUpperCase();
    }

    public boolean validarNumero(String valor) {
        for (String data : correlativo) {
            if (valor.equalsIgnoreCase(data)) {
                return false;
            }
        }
        return true;
    }

    public String generarFecha(String anyo, String mes, String dia) {
        return anyo.concat(valorMes(Integer.valueOf(mes)).concat(dia));
    }

    public String valorMes(int mes) {
        String mesString = "";
        switch (mes) {
            case 1:
                mesString = "ENE";
                break;
            case 2:
                mesString = "FEB";
                break;
            case 3:
                mesString = "MAR";
                break;
            case 4:
                mesString = "ABR";
                break;
            case 5:
                mesString = "MAY";
                break;
            case 6:
                mesString = "JUN";
                break;
            case 7:
                mesString = "JUL";
                break;
            case 8:
                mesString = "AGO";
                break;
            case 9:
                mesString = "SEP";
                break;
            case 10:
                mesString = "OCT";
                break;
            case 11:
                mesString = "NOV";
                break;
            case 12:
                mesString = "DIC";
                break;
            default:
                break;
        }
        return mesString;
    }
    
    public List<Lectura> getLsLecturas() {
        return lsLecturas;
    }

    public void setLsLecturas(List<Lectura> lsLecturas) {
        this.lsLecturas = lsLecturas;
    } 

    public int getHoraInicio() {
        return horaInicio;
    }

    @Override
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }
    
}

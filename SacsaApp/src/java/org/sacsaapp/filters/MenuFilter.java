/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.filters;

/**
 *
 * @author guillermo
 */
public class MenuFilter {
    
    private boolean personas;
    private boolean afiliados;
    private boolean contadores;
    private boolean clientes;
    private boolean cuentasxcobrar;
    private boolean lecturas;
    private boolean historico;
    private boolean directivas;
    private boolean configuraciones;
    private boolean reportes;
    
    public MenuFilter(){
        
    }
    
    public void menuAdmin(){
        personas = true;
        afiliados = true;
        configuraciones = true;
        contadores = true;
        clientes = true;
        cuentasxcobrar = true;
        lecturas = true;
        historico = true;
        directivas = true;
        reportes = true;
    }
    
    public void menuLectores(){
        personas = false;
        afiliados = false;
        configuraciones = false;
        contadores = false;
        clientes = false;
        cuentasxcobrar = false;
        lecturas = true;
        historico = false;
        directivas = false;
        reportes = false;
    }
    
    public void menuCajero(){
        personas = false;
        afiliados = false;
        configuraciones = false;
        contadores = false;
        clientes = false;
        cuentasxcobrar = false;
        lecturas = true;
        historico = false;
        directivas = false;
        reportes = false;
    }

    public boolean isPersonas() {
        return personas;
    }

    public void setPersonas(boolean personas) {
        this.personas = personas;
    }

    public boolean isAfiliados() {
        return afiliados;
    }

    public void setAfiliados(boolean afiliados) {
        this.afiliados = afiliados;
    }

    public boolean isContadores() {
        return contadores;
    }

    public void setContadores(boolean contadores) {
        this.contadores = contadores;
    }

    public boolean isClientes() {
        return clientes;
    }

    public void setClientes(boolean clientes) {
        this.clientes = clientes;
    }

    public boolean isCuentasxcobrar() {
        return cuentasxcobrar;
    }

    public void setCuentasxcobrar(boolean cuentasxcobrar) {
        this.cuentasxcobrar = cuentasxcobrar;
    }

    public boolean isLecturas() {
        return lecturas;
    }

    public void setLecturas(boolean lecturas) {
        this.lecturas = lecturas;
    }

    public boolean isHistorico() {
        return historico;
    }

    public void setHistorico(boolean historico) {
        this.historico = historico;
    }

    public boolean isDirectivas() {
        return directivas;
    }

    public void setDirectivas(boolean directivas) {
        this.directivas = directivas;
    }

    public boolean isConfiguraciones() {
        return configuraciones;
    }

    public void setConfiguraciones(boolean configuraciones) {
        this.configuraciones = configuraciones;
    }

    public boolean isReportes() {
        return reportes;
    }

    public void setReportes(boolean reportes) {
        this.reportes = reportes;
    }
    
}

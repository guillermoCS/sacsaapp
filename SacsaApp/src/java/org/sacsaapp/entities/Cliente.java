/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByClienteNumeroCuenta", query = "SELECT c FROM Cliente c WHERE c.clienteNumeroCuenta = :clienteNumeroCuenta"),
    @NamedQuery(name = "Cliente.findByClienteFechaRegistro", query = "SELECT c FROM Cliente c WHERE c.clienteFechaRegistro = :clienteFechaRegistro"),
    @NamedQuery(name = "Cliente.findByEstado", query = "SELECT c FROM Cliente c WHERE c.estado = :estado"),
    @NamedQuery(name = "Cliente.findByFechaRetiro", query = "SELECT c FROM Cliente c WHERE c.fechaRetiro = :fechaRetiro"),
@NamedQuery(name = "Cliente.findByBetweenFechaRegistro", query = "SELECT c FROM Cliente c WHERE c.clienteFechaRegistro BETWEEN :clienteFechaRegistro AND :fechaRetiro ")})
public class Cliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteNumeroCuenta")
    @TableGenerator(name = "ClientesGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ClientesSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ClientesGenID")
    private Long clienteNumeroCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteFechaRegistro")
    @Temporal(TemporalType.DATE)
    private Date clienteFechaRegistro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @Column(name = "fechaRetiro")
    @Temporal(TemporalType.DATE)
    private Date fechaRetiro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientesclienteNumeroCuenta")
    private List<CuentaPorCobrar> cuentaPorCobrarList;
    @JoinColumn(name = "personaID", referencedColumnName = "personaID")
    @ManyToOne(optional = false)
    private Persona personaID;

    public Cliente() {
    }

    public Cliente(Long clienteNumeroCuenta) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
    }

    public Cliente(Long clienteNumeroCuenta, Date clienteFechaRegistro, boolean estado) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
        this.clienteFechaRegistro = clienteFechaRegistro;
        this.estado = estado;
    }

    public Long getClienteNumeroCuenta() {
        return clienteNumeroCuenta;
    }

    public void setClienteNumeroCuenta(Long clienteNumeroCuenta) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
    }

    public Date getClienteFechaRegistro() {
        return clienteFechaRegistro;
    }

    public void setClienteFechaRegistro(Date clienteFechaRegistro) {
        this.clienteFechaRegistro = clienteFechaRegistro;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Date getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(Date fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    @XmlTransient
    public List<CuentaPorCobrar> getCuentaPorCobrarList() {
        return cuentaPorCobrarList;
    }

    public void setCuentaPorCobrarList(List<CuentaPorCobrar> cuentaPorCobrarList) {
        this.cuentaPorCobrarList = cuentaPorCobrarList;
    }

    public Persona getPersonaID() {
        return personaID;
    }

    public void setPersonaID(Persona personaID) {
        this.personaID = personaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteNumeroCuenta != null ? clienteNumeroCuenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.clienteNumeroCuenta == null && other.clienteNumeroCuenta != null) || (this.clienteNumeroCuenta != null && !this.clienteNumeroCuenta.equals(other.clienteNumeroCuenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Cliente[ clienteNumeroCuenta=" + clienteNumeroCuenta + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Servicios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s"),
    @NamedQuery(name = "Servicio.findByCobroID", query = "SELECT s FROM Servicio s WHERE s.cobroID = :cobroID"),
    @NamedQuery(name = "Servicio.findByCobroTipo", query = "SELECT s FROM Servicio s WHERE s.cobroTipo = :cobroTipo"),
    @NamedQuery(name = "Servicio.findByCobroDescripcion", query = "SELECT s FROM Servicio s WHERE s.cobroDescripcion = :cobroDescripcion")})
public class Servicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobroID")
    @TableGenerator(name = "ServicioGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ServiciosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ServicioGenID")
    private Integer cobroID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "cobroTipo")
    private String cobroTipo;
    @Size(max = 255)
    @Column(name = "cobroDescripcion")
    private String cobroDescripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "servicio")
    private List<DetalleServicio> detalleServicioList;

    public Servicio() {
    }

    public Servicio(Integer cobroID) {
        this.cobroID = cobroID;
    }

    public Servicio(Integer cobroID, String cobroTipo) {
        this.cobroID = cobroID;
        this.cobroTipo = cobroTipo;
    }

    public Integer getCobroID() {
        return cobroID;
    }

    public void setCobroID(Integer cobroID) {
        this.cobroID = cobroID;
    }

    public String getCobroTipo() {
        return cobroTipo;
    }

    public void setCobroTipo(String cobroTipo) {
        this.cobroTipo = cobroTipo;
    }

    public String getCobroDescripcion() {
        return cobroDescripcion;
    }

    public void setCobroDescripcion(String cobroDescripcion) {
        this.cobroDescripcion = cobroDescripcion;
    }

    @XmlTransient
    public List<DetalleServicio> getDetalleServicioList() {
        return detalleServicioList;
    }

    public void setDetalleServicioList(List<DetalleServicio> detalleServicioList) {
        this.detalleServicioList = detalleServicioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cobroID != null ? cobroID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.cobroID == null && other.cobroID != null) || (this.cobroID != null && !this.cobroID.equals(other.cobroID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Servicio[ cobroID=" + cobroID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Contadores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contador.findAll", query = "SELECT c FROM Contador c"),
    @NamedQuery(name = "Contador.findByContadorID", query = "SELECT c FROM Contador c WHERE c.contadorID = :contadorID"),
    @NamedQuery(name = "Contador.findByNumeroContador", query = "SELECT c FROM Contador c WHERE c.numeroContador = :numeroContador"),
    @NamedQuery(name = "Contador.findByEstado", query = "SELECT c FROM Contador c WHERE c.estado = :estado")})
public class Contador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "contadorID")
    @TableGenerator(name = "ContadorGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ContadoresSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ContadorGenID")
    private Long contadorID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "numeroContador")
    private String numeroContador;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @JoinColumn(name = "cuentasporCobrarclienteCuenta", referencedColumnName = "clienteNumeroCuenta")
    @ManyToOne(optional = false)
    private CuentaPorCobrar cuentasporCobrarclienteCuenta;

    public Contador() {
    }

    public Contador(Long contadorID) {
        this.contadorID = contadorID;
    }

    public Contador(Long contadorID, String numeroContador, boolean estado) {
        this.contadorID = contadorID;
        this.numeroContador = numeroContador;
        this.estado = estado;
    }

    public Long getContadorID() {
        return contadorID;
    }

    public void setContadorID(Long contadorID) {
        this.contadorID = contadorID;
    }

    public String getNumeroContador() {
        return numeroContador;
    }

    public void setNumeroContador(String numeroContador) {
        this.numeroContador = numeroContador;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public CuentaPorCobrar getCuentasporCobrarclienteCuenta() {
        return cuentasporCobrarclienteCuenta;
    }

    public void setCuentasporCobrarclienteCuenta(CuentaPorCobrar cuentasporCobrarclienteCuenta) {
        this.cuentasporCobrarclienteCuenta = cuentasporCobrarclienteCuenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contadorID != null ? contadorID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contador)) {
            return false;
        }
        Contador other = (Contador) object;
        if ((this.contadorID == null && other.contadorID != null) || (this.contadorID != null && !this.contadorID.equals(other.contadorID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Contador{" + "contadorID=" + contadorID + ", numeroContador=" + numeroContador + ", estado=" + estado + ", cuentasporCobrarclienteCuenta=" + cuentasporCobrarclienteCuenta + '}';
    }
    
    
}

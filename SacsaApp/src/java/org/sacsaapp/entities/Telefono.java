/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Telefonos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Telefono.findAll", query = "SELECT t FROM Telefono t"),
    @NamedQuery(name = "Telefono.findByTelID", query = "SELECT t FROM Telefono t WHERE t.telID = :telID"),
    @NamedQuery(name = "Telefono.findByTelNumero", query = "SELECT t FROM Telefono t WHERE t.telNumero = :telNumero"),
    @NamedQuery(name = "Telefono.findByTelEstado", query = "SELECT t FROM Telefono t WHERE t.telEstado = :telEstado")})
public class Telefono implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "telID")
    @TableGenerator(name = "TelefonoGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "TelefonosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TelefonoGenID")
    private Long telID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "telNumero")
    private String telNumero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telEstado")
    private boolean telEstado;
    @JoinColumn(name = "personaID", referencedColumnName = "personaID")
    @ManyToOne(optional = false)
    private Persona personaID;

    public Telefono() {
    }

    public Telefono(Long telID) {
        this.telID = telID;
    }

    public Telefono(Long telID, String telNumero, boolean telEstado) {
        this.telID = telID;
        this.telNumero = telNumero;
        this.telEstado = telEstado;
    }

    public Long getTelID() {
        return telID;
    }

    public void setTelID(Long telID) {
        this.telID = telID;
    }

    public String getTelNumero() {
        return telNumero;
    }

    public void setTelNumero(String telNumero) {
        this.telNumero = telNumero;
    }

    public boolean getTelEstado() {
        return telEstado;
    }

    public void setTelEstado(boolean telEstado) {
        this.telEstado = telEstado;
    }

    public Persona getPersonaID() {
        return personaID;
    }

    public void setPersonaID(Persona personaID) {
        this.personaID = personaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (telID != null ? telID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telefono)) {
            return false;
        }
        Telefono other = (Telefono) object;
        if ((this.telID == null && other.telID != null) || (this.telID != null && !this.telID.equals(other.telID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Telefono[ telID=" + telID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Grupos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupo.findAll", query = "SELECT g FROM Grupo g"),
    @NamedQuery(name = "Grupo.findByGrupoID", query = "SELECT g FROM Grupo g WHERE g.grupoID = :grupoID"),
    @NamedQuery(name = "Grupo.findByGrupoNombre", query = "SELECT g FROM Grupo g WHERE g.grupoNombre = :grupoNombre"),
    @NamedQuery(name = "Grupo.findByGrupoDescripcion", query = "SELECT g FROM Grupo g WHERE g.grupoDescripcion = :grupoDescripcion"),
    @NamedQuery(name = "Grupo.findByGrupoEstado", query = "SELECT g FROM Grupo g WHERE g.grupoEstado = :grupoEstado")})
public class Grupo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "grupoID")
    @TableGenerator(name = "GrupoGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "GruposSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "GrupoGenID")
    private Integer grupoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "grupoNombre")
    private String grupoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "grupoDescripcion")
    private String grupoDescripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "grupoEstado")
    private boolean grupoEstado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoID")
    private List<Usuario> usuarioList;

    public Grupo() {
    }

    public Grupo(Integer grupoID) {
        this.grupoID = grupoID;
    }

    public Grupo(Integer grupoID, String grupoNombre, String grupoDescripcion, boolean grupoEstado) {
        this.grupoID = grupoID;
        this.grupoNombre = grupoNombre;
        this.grupoDescripcion = grupoDescripcion;
        this.grupoEstado = grupoEstado;
    }

    public Integer getGrupoID() {
        return grupoID;
    }

    public void setGrupoID(Integer grupoID) {
        this.grupoID = grupoID;
    }

    public String getGrupoNombre() {
        return grupoNombre;
    }

    public void setGrupoNombre(String grupoNombre) {
        this.grupoNombre = grupoNombre;
    }

    public String getGrupoDescripcion() {
        return grupoDescripcion;
    }

    public void setGrupoDescripcion(String grupoDescripcion) {
        this.grupoDescripcion = grupoDescripcion;
    }

    public boolean getGrupoEstado() {
        return grupoEstado;
    }

    public void setGrupoEstado(boolean grupoEstado) {
        this.grupoEstado = grupoEstado;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupoID != null ? grupoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.grupoID == null && other.grupoID != null) || (this.grupoID != null && !this.grupoID.equals(other.grupoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Grupo[ grupoID=" + grupoID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Lecturas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lectura.findAll", query = "SELECT l FROM Lectura l"),
    @NamedQuery(name = "Lectura.findByLecturaID", query = "SELECT l FROM Lectura l WHERE l.lecturaID = :lecturaID"),
    @NamedQuery(name = "Lectura.findByFechaLectura", query = "SELECT l FROM Lectura l WHERE l.fechaLectura = :fechaLectura"),
    @NamedQuery(name = "Lectura.findByValorLectura", query = "SELECT l FROM Lectura l WHERE l.valorLectura = :valorLectura"),
    @NamedQuery(name = "Lectura.findByPeriodoInicio", query = "SELECT l FROM Lectura l WHERE l.periodoInicio = :periodoInicio"),
    @NamedQuery(name = "Lectura.findByPeriodoFinal", query = "SELECT l FROM Lectura l WHERE l.periodoFinal = :periodoFinal"),
    @NamedQuery(name = "Lectura.findToMesAPagar", query = "SELECT l FROM Lectura l WHERE SUBSTRING(l.periodoFinal, 1,4) = :anyo AND SUBSTRING(l.periodoFinal, 6,2) = :mes"),
    @NamedQuery(name = "Lectura.findByIDLector", query = "SELECT l FROM Lectura l WHERE l.iDLector = :iDLector")})
public class Lectura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "lecturaID")
    @TableGenerator(name = "LecturaGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "LecturasSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "LecturaGenID")
    private Long lecturaID;
    @Column(name = "fechaLectura")
    @Temporal(TemporalType.DATE)
    private Date fechaLectura;
    @Column(name = "valorLectura")
    private Integer valorLectura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoInicio")
    @Temporal(TemporalType.DATE)
    private Date periodoInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoFinal")
    @Temporal(TemporalType.DATE)
    private Date periodoFinal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "iDLector")
    private int iDLector;
    @JoinColumn(name = "clienteCuenta", referencedColumnName = "clienteNumeroCuenta")
    @ManyToOne(optional = false)
    private CuentaPorCobrar clienteCuenta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lecturaID")
    private List<Colecturia> colecturiaList;

    public Lectura() {
    }

    public Lectura(Long lecturaID) {
        this.lecturaID = lecturaID;
    }

    public Lectura(Long lecturaID, Date periodoInicio, Date periodoFinal, int iDLector) {
        this.lecturaID = lecturaID;
        this.periodoInicio = periodoInicio;
        this.periodoFinal = periodoFinal;
        this.iDLector = iDLector;
    }

    public Long getLecturaID() {
        return lecturaID;
    }

    public void setLecturaID(Long lecturaID) {
        this.lecturaID = lecturaID;
    }

    public Date getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(Date fechaLectura) {
        this.fechaLectura = fechaLectura;
    }

    public Integer getValorLectura() {
        return valorLectura;
    }

    public void setValorLectura(Integer valorLectura) {
        this.valorLectura = valorLectura;
    }

    public Date getPeriodoInicio() {
        return periodoInicio;
    }

    public void setPeriodoInicio(Date periodoInicio) {
        this.periodoInicio = periodoInicio;
    }

    public Date getPeriodoFinal() {
        return periodoFinal;
    }

    public void setPeriodoFinal(Date periodoFinal) {
        this.periodoFinal = periodoFinal;
    }

    public int getIDLector() {
        return iDLector;
    }

    public void setIDLector(int iDLector) {
        this.iDLector = iDLector;
    }

    public CuentaPorCobrar getClienteCuenta() {
        return clienteCuenta;
    }

    public void setClienteCuenta(CuentaPorCobrar clienteCuenta) {
        this.clienteCuenta = clienteCuenta;
    }

    @XmlTransient
    public List<Colecturia> getColecturiaList() {
        return colecturiaList;
    }

    public void setColecturiaList(List<Colecturia> colecturiaList) {
        this.colecturiaList = colecturiaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lecturaID != null ? lecturaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lectura)) {
            return false;
        }
        Lectura other = (Lectura) object;
        if ((this.lecturaID == null && other.lecturaID != null) || (this.lecturaID != null && !this.lecturaID.equals(other.lecturaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Lectura[ lecturaID=" + lecturaID + " ]";
    }
    
}

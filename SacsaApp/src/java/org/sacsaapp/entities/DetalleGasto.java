/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "DetalleGastos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleGasto.findAll", query = "SELECT d FROM DetalleGasto d"),
    @NamedQuery(name = "DetalleGasto.findByGastosID", query = "SELECT d FROM DetalleGasto d WHERE d.detalleGastoPK.gastosID = :gastosID"),
    @NamedQuery(name = "DetalleGasto.findByTipoDocumentoID", query = "SELECT d FROM DetalleGasto d WHERE d.detalleGastoPK.tipoDocumentoID = :tipoDocumentoID"),
    @NamedQuery(name = "DetalleGasto.findByNumeroDocumento", query = "SELECT d FROM DetalleGasto d WHERE d.numeroDocumento = :numeroDocumento"),
    @NamedQuery(name = "DetalleGasto.findByEmision", query = "SELECT d FROM DetalleGasto d WHERE d.emision = :emision"),
    @NamedQuery(name = "DetalleGasto.findByFechaRegistro", query = "SELECT d FROM DetalleGasto d WHERE d.fechaRegistro = :fechaRegistro"),
    @NamedQuery(name = "DetalleGasto.findByProveedor", query = "SELECT d FROM DetalleGasto d WHERE d.proveedor = :proveedor"),
    @NamedQuery(name = "DetalleGasto.findByDebe", query = "SELECT d FROM DetalleGasto d WHERE d.debe = :debe"),
    @NamedQuery(name = "DetalleGasto.findByHaber", query = "SELECT d FROM DetalleGasto d WHERE d.haber = :haber")})
public class DetalleGasto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetalleGastoPK detalleGastoPK;
    @Size(max = 150)
    @Column(name = "numeroDocumento")
    private String numeroDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "emision")
    @Temporal(TemporalType.DATE)
    private Date emision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaRegistro")
    @Temporal(TemporalType.DATE)
    private Date fechaRegistro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "proveedor")
    private String proveedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "debe")
    private double debe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "haber")
    private double haber;
    @JoinColumn(name = "tipoDocumentoID", referencedColumnName = "documentoID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDocumento tipoDocumento;
    @JoinColumn(name = "gastosID", referencedColumnName = "gastosID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Gasto gasto;

    public DetalleGasto() {
    }

    public DetalleGasto(DetalleGastoPK detalleGastoPK) {
        this.detalleGastoPK = detalleGastoPK;
    }

    public DetalleGasto(DetalleGastoPK detalleGastoPK, Date emision, Date fechaRegistro, String proveedor, double debe, double haber) {
        this.detalleGastoPK = detalleGastoPK;
        this.emision = emision;
        this.fechaRegistro = fechaRegistro;
        this.proveedor = proveedor;
        this.debe = debe;
        this.haber = haber;
    }

    public DetalleGasto(long gastosID, int tipoDocumentoID) {
        this.detalleGastoPK = new DetalleGastoPK(gastosID, tipoDocumentoID);
    }

    public DetalleGastoPK getDetalleGastoPK() {
        return detalleGastoPK;
    }

    public void setDetalleGastoPK(DetalleGastoPK detalleGastoPK) {
        this.detalleGastoPK = detalleGastoPK;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Date getEmision() {
        return emision;
    }

    public void setEmision(Date emision) {
        this.emision = emision;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Gasto getGasto() {
        return gasto;
    }

    public void setGasto(Gasto gasto) {
        this.gasto = gasto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleGastoPK != null ? detalleGastoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleGasto)) {
            return false;
        }
        DetalleGasto other = (DetalleGasto) object;
        if ((this.detalleGastoPK == null && other.detalleGastoPK != null) || (this.detalleGastoPK != null && !this.detalleGastoPK.equals(other.detalleGastoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.DetalleGasto[ detalleGastoPK=" + detalleGastoPK + " ]";
    }
    
}

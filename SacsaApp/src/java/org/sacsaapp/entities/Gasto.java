/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Gastos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gasto.findAll", query = "SELECT g FROM Gasto g"),
    @NamedQuery(name = "Gasto.findByGastosID", query = "SELECT g FROM Gasto g WHERE g.gastosID = :gastosID"),
    @NamedQuery(name = "Gasto.findByGastoMensual", query = "SELECT g FROM Gasto g WHERE g.gastoMensual = :gastoMensual")})
public class Gasto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "gastosID")
    private Long gastosID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gastoMensual")
    private double gastoMensual;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gasto")
    private List<DetalleGasto> detalleGastoList;
    @JoinColumn(name = "periodoID", referencedColumnName = "periodoID")
    @ManyToOne(optional = false)
    private Periodo periodoID;

    public Gasto() {
    }

    public Gasto(Long gastosID) {
        this.gastosID = gastosID;
    }

    public Gasto(Long gastosID, double gastoMensual) {
        this.gastosID = gastosID;
        this.gastoMensual = gastoMensual;
    }

    public Long getGastosID() {
        return gastosID;
    }

    public void setGastosID(Long gastosID) {
        this.gastosID = gastosID;
    }

    public double getGastoMensual() {
        return gastoMensual;
    }

    public void setGastoMensual(double gastoMensual) {
        this.gastoMensual = gastoMensual;
    }

    @XmlTransient
    public List<DetalleGasto> getDetalleGastoList() {
        return detalleGastoList;
    }

    public void setDetalleGastoList(List<DetalleGasto> detalleGastoList) {
        this.detalleGastoList = detalleGastoList;
    }

    public Periodo getPeriodoID() {
        return periodoID;
    }

    public void setPeriodoID(Periodo periodoID) {
        this.periodoID = periodoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gastosID != null ? gastosID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gasto)) {
            return false;
        }
        Gasto other = (Gasto) object;
        if ((this.gastosID == null && other.gastosID != null) || (this.gastosID != null && !this.gastosID.equals(other.gastosID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Gasto[ gastosID=" + gastosID + " ]";
    }
    
}

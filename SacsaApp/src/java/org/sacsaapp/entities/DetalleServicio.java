/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "DetalleServicios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleServicio.findAll", query = "SELECT d FROM DetalleServicio d"),
    @NamedQuery(name = "DetalleServicio.findByCobroID", query = "SELECT d FROM DetalleServicio d WHERE d.detalleServicioPK.cobroID = :cobroID"),
    @NamedQuery(name = "DetalleServicio.findByColecturiasLecturaID", query = "SELECT d FROM DetalleServicio d WHERE d.detalleServicioPK.colecturiasLecturaID = :colecturiasLecturaID"),
    @NamedQuery(name = "DetalleServicio.findByValorCobro", query = "SELECT d FROM DetalleServicio d WHERE d.valorCobro = :valorCobro")})
public class DetalleServicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetalleServicioPK detalleServicioPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valorCobro")
    private double valorCobro;
    @JoinColumn(name = "colecturiasLecturaID", referencedColumnName = "colecturiaID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Colecturia colecturia;
    @JoinColumn(name = "cobroID", referencedColumnName = "cobroID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;

    public DetalleServicio() {
    }

    public DetalleServicio(DetalleServicioPK detalleServicioPK) {
        this.detalleServicioPK = detalleServicioPK;
    }

    public DetalleServicio(DetalleServicioPK detalleServicioPK, double valorCobro) {
        this.detalleServicioPK = detalleServicioPK;
        this.valorCobro = valorCobro;
    }

    public DetalleServicio(int cobroID, long colecturiasLecturaID) {
        this.detalleServicioPK = new DetalleServicioPK(cobroID, colecturiasLecturaID);
    }

    public DetalleServicioPK getDetalleServicioPK() {
        return detalleServicioPK;
    }

    public void setDetalleServicioPK(DetalleServicioPK detalleServicioPK) {
        this.detalleServicioPK = detalleServicioPK;
    }

    public double getValorCobro() {
        return valorCobro;
    }

    public void setValorCobro(double valorCobro) {
        this.valorCobro = valorCobro;
    }

    public Colecturia getColecturia() {
        return colecturia;
    }

    public void setColecturia(Colecturia colecturia) {
        this.colecturia = colecturia;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleServicioPK != null ? detalleServicioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleServicio)) {
            return false;
        }
        DetalleServicio other = (DetalleServicio) object;
        if ((this.detalleServicioPK == null && other.detalleServicioPK != null) || (this.detalleServicioPK != null && !this.detalleServicioPK.equals(other.detalleServicioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.DetalleServicio[ detalleServicioPK=" + detalleServicioPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Afiliados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Afiliado.findAll", query = "SELECT a FROM Afiliado a"),
    @NamedQuery(name = "Afiliado.findByAfiliadoID", query = "SELECT a FROM Afiliado a WHERE a.afiliadoID = :afiliadoID"),
    @NamedQuery(name = "Afiliado.findByFechaAfiliado", query = "SELECT a FROM Afiliado a WHERE a.fechaAfiliado = :fechaAfiliado"),
    @NamedQuery(name = "Afiliado.findByFechaRetiro", query = "SELECT a FROM Afiliado a WHERE a.fechaRetiro = :fechaRetiro"),
    @NamedQuery(name = "Afiliado.findByNumeroIDpersona", query = "SELECT a FROM Afiliado a WHERE a.personaspersonaID = :personaspersonaID"),
    @NamedQuery(name = "Afiliado.findByEstado", query = "SELECT a FROM Afiliado a WHERE a.estado = :estado")})

public class Afiliado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "afiliadoID")
    private Long afiliadoID;
    @Column(name = "fechaAfiliado")
    @Temporal(TemporalType.DATE)
    private Date fechaAfiliado;
    @Column(name = "fechaRetiro")
    @Temporal(TemporalType.DATE)
    private Date fechaRetiro;
    @Column(name = "estado")
    private Boolean estado;
    @ManyToMany(mappedBy = "afiliadoList")
    private List<Directiva> directivaList;
    @JoinColumn(name = "personaspersonaID", referencedColumnName = "personaID")
    @ManyToOne(optional = false)
    private Persona personaspersonaID;

    public Afiliado() {
    }

    public Afiliado(Long afiliadoID) {
        this.afiliadoID = afiliadoID;
    }

    public Long getAfiliadoID() {
        return afiliadoID;
    }

    public void setAfiliadoID(Long afiliadoID) {
        this.afiliadoID = afiliadoID;
    }

    public Date getFechaAfiliado() {
        return fechaAfiliado;
    }

    public void setFechaAfiliado(Date fechaAfiliado) {
        this.fechaAfiliado = fechaAfiliado;
    }

    public Date getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(Date fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Directiva> getDirectivaList() {
        return directivaList;
    }

    public void setDirectivaList(List<Directiva> directivaList) {
        this.directivaList = directivaList;
    }

    public Persona getPersonaspersonaID() {
        return personaspersonaID;
    }

    public void setPersonaspersonaID(Persona personaspersonaID) {
        this.personaspersonaID = personaspersonaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (afiliadoID != null ? afiliadoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Afiliado)) {
            return false;
        }
        Afiliado other = (Afiliado) object;
        if ((this.afiliadoID == null && other.afiliadoID != null) || (this.afiliadoID != null && !this.afiliadoID.equals(other.afiliadoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Afiliado[ afiliadoID=" + afiliadoID + " ]";
    }

}

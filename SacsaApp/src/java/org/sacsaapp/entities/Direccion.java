/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Direcciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findAll", query = "SELECT d FROM Direccion d"),
    @NamedQuery(name = "Direccion.findByDireccionID", query = "SELECT d FROM Direccion d WHERE d.direccionID = :direccionID"),
    @NamedQuery(name = "Direccion.findByDireccionEstado", query = "SELECT d FROM Direccion d WHERE d.direccionEstado = :direccionEstado"),
    @NamedQuery(name = "Direccion.findByDireccionMunicipio", query = "SELECT d FROM Direccion d WHERE d.direccionMunicipio = :direccionMunicipio")})
public class Direccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "direccionID")
    @TableGenerator(name = "DireccionGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "DireccionesSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "DireccionGenID")
    private Long direccionID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "direccionDir")
    private String direccionDir;
    @Basic(optional = false)
    @NotNull
    @Column(name = "direccionEstado")
    private boolean direccionEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "direccionMunicipio")
    private String direccionMunicipio;
    @JoinColumn(name = "personaID", referencedColumnName = "personaID")
    @ManyToOne(optional = false)
    private Persona personaID;
    @JoinColumn(name = "municipioID", referencedColumnName = "municipioID")
    @ManyToOne(optional = false)
    private Municipio municipioID;

    public Direccion() {
    }

    public Direccion(Long direccionID) {
        this.direccionID = direccionID;
    }

    public Direccion(Long direccionID, String direccionDir, boolean direccionEstado, String direccionMunicipio) {
        this.direccionID = direccionID;
        this.direccionDir = direccionDir;
        this.direccionEstado = direccionEstado;
        this.direccionMunicipio = direccionMunicipio;
    }

    public Long getDireccionID() {
        return direccionID;
    }

    public void setDireccionID(Long direccionID) {
        this.direccionID = direccionID;
    }

    public String getDireccionDir() {
        return direccionDir;
    }

    public void setDireccionDir(String direccionDir) {
        this.direccionDir = direccionDir;
    }

    public boolean getDireccionEstado() {
        return direccionEstado;
    }

    public void setDireccionEstado(boolean direccionEstado) {
        this.direccionEstado = direccionEstado;
    }

    public String getDireccionMunicipio() {
        return direccionMunicipio;
    }

    public void setDireccionMunicipio(String direccionMunicipio) {
        this.direccionMunicipio = direccionMunicipio;
    }

    public Persona getPersonaID() {
        return personaID;
    }

    public void setPersonaID(Persona personaID) {
        this.personaID = personaID;
    }

    public Municipio getMunicipioID() {
        return municipioID;
    }

    public void setMunicipioID(Municipio municipioID) {
        this.municipioID = municipioID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (direccionID != null ? direccionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.direccionID == null && other.direccionID != null) || (this.direccionID != null && !this.direccionID.equals(other.direccionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Direccion[ direccionID=" + direccionID + " ]";
    }
    
}

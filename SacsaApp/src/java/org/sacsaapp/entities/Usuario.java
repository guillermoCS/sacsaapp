/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByUsuarioID", query = "SELECT u FROM Usuario u WHERE u.usuarioID = :usuarioID"),
    @NamedQuery(name = "Usuario.findByUsuarioNombre", query = "SELECT u FROM Usuario u WHERE u.usuarioNombre = :usuarioNombre"),
    @NamedQuery(name = "Usuario.findByUsuarioPassword", query = "SELECT u FROM Usuario u WHERE u.usuarioPassword = :usuarioPassword"),
    @NamedQuery(name = "Usuario.ValidateUsuario", query = "SELECT u FROM Usuario u WHERE u.usuarioNombre = :usuarioNombre AND u.usuarioPassword = :usuarioPassword"),
    @NamedQuery(name = "Usuario.findByUsuarioEstado", query = "SELECT u FROM Usuario u WHERE u.usuarioEstado = :usuarioEstado")})
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuarioID")
    @TableGenerator(name = "UsuarioGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "UsuariosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "UsuarioGenID")
    private Long usuarioID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "usuarioNombre")
    private String usuarioNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "usuarioPassword")
    private String usuarioPassword;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuarioEstado")
    private boolean usuarioEstado;
    @JoinColumn(name = "Personas_personaID", referencedColumnName = "personaID")
    @ManyToOne(optional = false)
    private Persona personaspersonaID;
    @JoinColumn(name = "grupoID", referencedColumnName = "grupoID")
    @ManyToOne(optional = false)
    private Grupo grupoID;

    public Usuario() {
    }

    public Usuario(Long usuarioID) {
        this.usuarioID = usuarioID;
    }

    public Usuario(Long usuarioID, String usuarioNombre, String usuarioPassword, boolean usuarioEstado) {
        this.usuarioID = usuarioID;
        this.usuarioNombre = usuarioNombre;
        this.usuarioPassword = usuarioPassword;
        this.usuarioEstado = usuarioEstado;
    }

    public Long getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Long usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getUsuarioPassword() {
        return usuarioPassword;
    }

    public void setUsuarioPassword(String usuarioPassword) {
        this.usuarioPassword = usuarioPassword;
    }

    public boolean getUsuarioEstado() {
        return usuarioEstado;
    }

    public void setUsuarioEstado(boolean usuarioEstado) {
        this.usuarioEstado = usuarioEstado;
    }

    public Persona getPersonaspersonaID() {
        return personaspersonaID;
    }

    public void setPersonaspersonaID(Persona personaspersonaID) {
        this.personaspersonaID = personaspersonaID;
    }

    public Grupo getGrupoID() {
        return grupoID;
    }

    public void setGrupoID(Grupo grupoID) {
        this.grupoID = grupoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioID != null ? usuarioID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioID == null && other.usuarioID != null) || (this.usuarioID != null && !this.usuarioID.equals(other.usuarioID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Usuario[ usuarioID=" + usuarioID + " ]";
    }
    
}

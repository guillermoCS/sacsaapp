/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Directiva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Directiva.findAll", query = "SELECT d FROM Directiva d"),
    @NamedQuery(name = "Directiva.findByIdCargo", query = "SELECT d FROM Directiva d WHERE d.idCargo = :idCargo"),
    @NamedQuery(name = "Directiva.findByCargo", query = "SELECT d FROM Directiva d WHERE d.cargo = :cargo")})
public class Directiva implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCargo")
     @TableGenerator(name = "DirectivaGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "DirectivasSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "DirectivaGenID")
    private Integer idCargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "cargo")
    private String cargo;
    @Lob
    @Size(max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinTable(name = "DirectivaAfiliados", joinColumns = {
        @JoinColumn(name = "directivaIDCargo", referencedColumnName = "idCargo")}, inverseJoinColumns = {
        @JoinColumn(name = "afiliadosafiliadoID", referencedColumnName = "afiliadoID")})
    @ManyToMany
    private List<Afiliado> afiliadoList;

    public Directiva() {
    }

    public Directiva(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Directiva(Integer idCargo, String cargo) {
        this.idCargo = idCargo;
        this.cargo = cargo;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Afiliado> getAfiliadoList() {
        return afiliadoList;
    }

    public void setAfiliadoList(List<Afiliado> afiliadoList) {
        this.afiliadoList = afiliadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCargo != null ? idCargo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Directiva)) {
            return false;
        }
        Directiva other = (Directiva) object;
        if ((this.idCargo == null && other.idCargo != null) || (this.idCargo != null && !this.idCargo.equals(other.idCargo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Directiva{" + "idCargo=" + idCargo + ", cargo=" + cargo + ", descripcion=" + descripcion + ", afiliadoList=" + afiliadoList + '}';
    }


    
}

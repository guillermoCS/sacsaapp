/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author guillermo
 */
@Embeddable
public class DetalleGastoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "gastosID")
    private long gastosID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipoDocumentoID")
    private int tipoDocumentoID;

    public DetalleGastoPK() {
    }

    public DetalleGastoPK(long gastosID, int tipoDocumentoID) {
        this.gastosID = gastosID;
        this.tipoDocumentoID = tipoDocumentoID;
    }

    public long getGastosID() {
        return gastosID;
    }

    public void setGastosID(long gastosID) {
        this.gastosID = gastosID;
    }

    public int getTipoDocumentoID() {
        return tipoDocumentoID;
    }

    public void setTipoDocumentoID(int tipoDocumentoID) {
        this.tipoDocumentoID = tipoDocumentoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) gastosID;
        hash += (int) tipoDocumentoID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleGastoPK)) {
            return false;
        }
        DetalleGastoPK other = (DetalleGastoPK) object;
        if (this.gastosID != other.gastosID) {
            return false;
        }
        if (this.tipoDocumentoID != other.tipoDocumentoID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.DetalleGastoPK[ gastosID=" + gastosID + ", tipoDocumentoID=" + tipoDocumentoID + " ]";
    }
    
}

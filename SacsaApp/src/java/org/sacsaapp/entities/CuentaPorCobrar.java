/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "CuentasporCobrar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuentaPorCobrar.findAll", query = "SELECT c FROM CuentaPorCobrar c"),
    @NamedQuery(name = "CuentaPorCobrar.findByClienteNumeroCuenta", query = "SELECT c FROM CuentaPorCobrar c WHERE c.clienteNumeroCuenta = :clienteNumeroCuenta"),
    @NamedQuery(name = "CuentaPorCobrar.findByNumeroCuenta", query = "SELECT c FROM CuentaPorCobrar c WHERE c.numeroCuenta = :numeroCuenta"),
    @NamedQuery(name = "CuentaPorCobrar.findByEstadoCuenta", query = "SELECT c FROM CuentaPorCobrar c WHERE c.estadoCuenta = :estadoCuenta"),

    @NamedQuery(name = "CuentaPorCobrar.findBySaldo", query = "SELECT c FROM CuentaPorCobrar c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "CuentaPorCobrar.findCliente", query = "SELECT c FROM CuentaPorCobrar c WHERE c.clientesclienteNumeroCuenta.clienteNumeroCuenta = :numCuenta")})
public class CuentaPorCobrar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteNumeroCuenta")
    @TableGenerator(name = "CuentaGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "CuentasCobrarSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "CuentaGenID")
    private Long clienteNumeroCuenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "numeroCuenta")
    private String numeroCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estadoCuenta")
    private boolean estadoCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldo")
    private double saldo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clienteCuenta")
    private List<Lectura> lecturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentasporCobrarClientescuenta")
    private List<Abono> abonoList;
    @JoinColumn(name = "clientesclienteNumeroCuenta", referencedColumnName = "clienteNumeroCuenta")
    @ManyToOne(optional = false)
    private Cliente clientesclienteNumeroCuenta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuentasporCobrarclienteCuenta")
    private List<Contador> contadorList;

    public CuentaPorCobrar() {
    }

    public CuentaPorCobrar(Long clienteNumeroCuenta) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
    }

    public CuentaPorCobrar(Long clienteNumeroCuenta, String numeroCuenta, boolean estadoCuenta, double saldo) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
        this.numeroCuenta = numeroCuenta;
        this.estadoCuenta = estadoCuenta;
        this.saldo = saldo;
    }

    public Long getClienteNumeroCuenta() {
        return clienteNumeroCuenta;
    }

    public void setClienteNumeroCuenta(Long clienteNumeroCuenta) {
        this.clienteNumeroCuenta = clienteNumeroCuenta;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public boolean getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(boolean estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @XmlTransient
    public List<Lectura> getLecturaList() {
        return lecturaList;
    }

    public void setLecturaList(List<Lectura> lecturaList) {
        this.lecturaList = lecturaList;
    }

    @XmlTransient
    public List<Abono> getAbonoList() {
        return abonoList;
    }

    public void setAbonoList(List<Abono> abonoList) {
        this.abonoList = abonoList;
    }

    public Cliente getClientesclienteNumeroCuenta() {
        return clientesclienteNumeroCuenta;
    }

    public void setClientesclienteNumeroCuenta(Cliente clientesclienteNumeroCuenta) {
        this.clientesclienteNumeroCuenta = clientesclienteNumeroCuenta;
    }

    @XmlTransient
    public List<Contador> getContadorList() {
        return contadorList;
    }

    public void setContadorList(List<Contador> contadorList) {
        this.contadorList = contadorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteNumeroCuenta != null ? clienteNumeroCuenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuentaPorCobrar)) {
            return false;
        }
        CuentaPorCobrar other = (CuentaPorCobrar) object;
        if ((this.clienteNumeroCuenta == null && other.clienteNumeroCuenta != null) || (this.clienteNumeroCuenta != null && !this.clienteNumeroCuenta.equals(other.clienteNumeroCuenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.CuentaPorCobrar[ clienteNumeroCuenta=" + clienteNumeroCuenta + " ]";
    }

}

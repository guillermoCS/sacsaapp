/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Abonos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Abono.findAll", query = "SELECT a FROM Abono a"),
    @NamedQuery(name = "Abono.findByAbonoID", query = "SELECT a FROM Abono a WHERE a.abonoID = :abonoID"),
    @NamedQuery(name = "Abono.findByAbonoActual", query = "SELECT a FROM Abono a WHERE a.abonoActual = :abonoActual"),
    @NamedQuery(name = "Abono.findByAbonoAnterior", query = "SELECT a FROM Abono a WHERE a.abonoAnterior = :abonoAnterior"),
    @NamedQuery(name = "Abono.findAllAbonoFechaAnterior", query = "SELECT a FROM Abono a WHERE a.abonoFechaRegistro < :abonoActual"),
    @NamedQuery(name = "Abono.findAllAbonoByCuenta", query = "SELECT a FROM Abono a WHERE a.cuentasporCobrarClientescuenta =:cuentasporCobrarClientescuenta"),

    // @NamedQuery(name = "Abono.findOneByCuenta", query = "SELECT a FROM Abono a WHERE a.cuentasporCobrarClientescuenta = :cuenta ORDER BY A.abonoFechaRegistro DESC LIMIT :=1"),
    @NamedQuery(name = "Abono.findByAbonoFechaRegistro", query = "SELECT a FROM Abono a WHERE a.abonoFechaRegistro = :abonoFechaRegistro")})

public class Abono implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "abonoID")
    private Long abonoID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "abonoActual")
    private double abonoActual;
    @Basic(optional = false)
    @NotNull
    @Column(name = "abonoAnterior")
    private double abonoAnterior;
    @Basic(optional = false)
    @NotNull
    @Column(name = "abonoFechaRegistro")
    @Temporal(TemporalType.DATE)
    private Date abonoFechaRegistro;
    @JoinColumn(name = "cuentasporCobrarClientescuenta", referencedColumnName = "clienteNumeroCuenta")
    @ManyToOne(optional = false)
    private CuentaPorCobrar cuentasporCobrarClientescuenta;

    public Abono() {
    }

    public Abono(Long abonoID) {
        this.abonoID = abonoID;
    }

    public Abono(Long abonoID, double abonoActual, double abonoAnterior, Date abonoFechaRegistro) {
        this.abonoID = abonoID;
        this.abonoActual = abonoActual;
        this.abonoAnterior = abonoAnterior;
        this.abonoFechaRegistro = abonoFechaRegistro;
    }

    public Long getAbonoID() {
        return abonoID;
    }

    public void setAbonoID(Long abonoID) {
        this.abonoID = abonoID;
    }

    public double getAbonoActual() {
        return abonoActual;
    }

    public void setAbonoActual(double abonoActual) {
        this.abonoActual = abonoActual;
    }

    public double getAbonoAnterior() {
        return abonoAnterior;
    }

    public void setAbonoAnterior(double abonoAnterior) {
        this.abonoAnterior = abonoAnterior;
    }

    public Date getAbonoFechaRegistro() {
        return abonoFechaRegistro;
    }

    public void setAbonoFechaRegistro(Date abonoFechaRegistro) {
        this.abonoFechaRegistro = abonoFechaRegistro;
    }

    public CuentaPorCobrar getCuentasporCobrarClientescuenta() {
        return cuentasporCobrarClientescuenta;
    }

    public void setCuentasporCobrarClientescuenta(CuentaPorCobrar cuentasporCobrarClientescuenta) {
        this.cuentasporCobrarClientescuenta = cuentasporCobrarClientescuenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (abonoID != null ? abonoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abono)) {
            return false;
        }
        Abono other = (Abono) object;
        if ((this.abonoID == null && other.abonoID != null) || (this.abonoID != null && !this.abonoID.equals(other.abonoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Abono[ abonoID=" + abonoID + " ]";
    }

}

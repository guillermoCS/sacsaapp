/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "TiposDocumento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDocumento.findAll", query = "SELECT t FROM TipoDocumento t"),
    @NamedQuery(name = "TipoDocumento.findByDocumentoID", query = "SELECT t FROM TipoDocumento t WHERE t.documentoID = :documentoID"),
    @NamedQuery(name = "TipoDocumento.findByTipo", query = "SELECT t FROM TipoDocumento t WHERE t.tipo = :tipo")})
public class TipoDocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "documentoID")
    @TableGenerator(name = "DocGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "TiposDocsSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "DocGenID")
    private Integer documentoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "tipo")
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDocumento")
    private List<DetalleGasto> detalleGastoList;

    public TipoDocumento() {
    }

    public TipoDocumento(Integer documentoID) {
        this.documentoID = documentoID;
    }

    public TipoDocumento(Integer documentoID, String tipo) {
        this.documentoID = documentoID;
        this.tipo = tipo;
    }

    public Integer getDocumentoID() {
        return documentoID;
    }

    public void setDocumentoID(Integer documentoID) {
        this.documentoID = documentoID;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<DetalleGasto> getDetalleGastoList() {
        return detalleGastoList;
    }

    public void setDetalleGastoList(List<DetalleGasto> detalleGastoList) {
        this.detalleGastoList = detalleGastoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoID != null ? documentoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDocumento)) {
            return false;
        }
        TipoDocumento other = (TipoDocumento) object;
        if ((this.documentoID == null && other.documentoID != null) || (this.documentoID != null && !this.documentoID.equals(other.documentoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.TipoDocumento[ documentoID=" + documentoID + " ]";
    }
    
}

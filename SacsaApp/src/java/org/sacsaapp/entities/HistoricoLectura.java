/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "HistoricoLectura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoLectura.findAll", query = "SELECT h FROM HistoricoLectura h"),
    @NamedQuery(name = "HistoricoLectura.findByPeriodoID", query = "SELECT h FROM HistoricoLectura h WHERE h.periodoID = :periodoID"),
    @NamedQuery(name = "HistoricoLectura.findByPeriodoInicio", query = "SELECT h FROM HistoricoLectura h WHERE h.periodoInicio = :periodoInicio"),
    @NamedQuery(name = "HistoricoLectura.findByPeriodoFinal", query = "SELECT h FROM HistoricoLectura h WHERE h.periodoFinal = :periodoFinal"),
    @NamedQuery(name = "HistoricoLectura.findByLecturaAnterior", query = "SELECT h FROM HistoricoLectura h WHERE h.lecturaAnterior = :lecturaAnterior"),
    @NamedQuery(name = "HistoricoLectura.findByLecturaActual", query = "SELECT h FROM HistoricoLectura h WHERE h.lecturaActual = :lecturaActual"),
    @NamedQuery(name = "HistoricoLectura.findByCorrelativoFactura", query = "SELECT h FROM HistoricoLectura h WHERE h.correlativoFactura = :correlativoFactura"),
    @NamedQuery(name = "HistoricoLectura.findByClienteMPeriodoID", query = "SELECT h FROM HistoricoLectura h WHERE h.clienteMPeriodoID = :clienteMPeriodoID"),
    @NamedQuery(name = "HistoricoLectura.findByClienteMDeuda", query = "SELECT h FROM HistoricoLectura h WHERE h.clienteMDeuda = :clienteMDeuda"),
    @NamedQuery(name = "HistoricoLectura.findByClienteMAplicaCorte", query = "SELECT h FROM HistoricoLectura h WHERE h.clienteMAplicaCorte = :clienteMAplicaCorte"),
    @NamedQuery(name = "HistoricoLectura.findByClienteMNumMsPagar", query = "SELECT h FROM HistoricoLectura h WHERE h.clienteMNumMsPagar = :clienteMNumMsPagar"),
    @NamedQuery(name = "HistoricoLectura.findBySaldo", query = "SELECT h FROM HistoricoLectura h WHERE h.saldo = :saldo"),
    @NamedQuery(name = "HistoricoLectura.findByFechaLectura", query = "SELECT h FROM HistoricoLectura h WHERE h.fechaLectura = :fechaLectura"),
    @NamedQuery(name = "HistoricoLectura.findLikeFechaLectura", query = "SELECT h FROM HistoricoLectura h WHERE SUBSTRING(h.fechaLectura, 1,4) = :anyo AND SUBSTRING(h.fechaLectura, 6,2) = :mes AND h.numCuenta = :numCuenta"),
    @NamedQuery(name = "HistoricoLectura.findByAbono", query = "SELECT h FROM HistoricoLectura h WHERE h.abono = :abono"),
    @NamedQuery(name = "HistoricoLectura.findByCargo", query = "SELECT h FROM HistoricoLectura h WHERE h.cargo = :cargo"),
    @NamedQuery(name = "HistoricoLectura.findByPagoCancelado", query = "SELECT h FROM HistoricoLectura h WHERE h.pagoCancelado = :pagoCancelado"),
    @NamedQuery(name = "HistoricoLectura.findByClienteNumCuenta", query = "SELECT h FROM HistoricoLectura h WHERE h.clienteNumCuenta = :clienteNumCuenta"),
    @NamedQuery(name = "HistoricoLectura.findByNumCuenta", query = "SELECT h FROM HistoricoLectura h WHERE h.numCuenta = :numCuenta"),
    @NamedQuery(name = "HistoricoLectura.findByLecturaID", query = "SELECT h FROM HistoricoLectura h WHERE h.lecturaID = :lecturaID"),
@NamedQuery(name = "HistoricoLectura.findBetweenFechaLectura", query = "SELECT h FROM HistoricoLectura h WHERE h.numCuenta = :numCuenta AND  h.fechaLectura BETWEEN :desde AND :hasta")
})
public class HistoricoLectura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoID")
    private Long periodoID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoInicio")
    @Temporal(TemporalType.DATE)
    private Date periodoInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoFinal")
    @Temporal(TemporalType.DATE)
    private Date periodoFinal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lecturaAnterior")
    private double lecturaAnterior;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lecturaActual")
    private double lecturaActual;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CorrelativoFactura")
    private String correlativoFactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMPeriodoID")
    private int clienteMPeriodoID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMDeuda")
    private double clienteMDeuda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMAplicaCorte")
    private boolean clienteMAplicaCorte;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMNumMsPagar")
    private int clienteMNumMsPagar;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "saldo")
    private Double saldo;
    @Column(name = "fechaLectura")
    @Temporal(TemporalType.DATE)
    private Date fechaLectura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "abono")
    private double abono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cargo")
    private double cargo;
    @Column(name = "pagoCancelado")
    @Temporal(TemporalType.DATE)
    private Date pagoCancelado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteNumCuenta")
    private long clienteNumCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numCuenta")
    private long numCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lecturaID")
    private long lecturaID;

    public HistoricoLectura() {
    }

    public HistoricoLectura(Long periodoID) {
        this.periodoID = periodoID;
    }

    public HistoricoLectura(Long periodoID, Date periodoInicio, Date periodoFinal, double lecturaAnterior, double lecturaActual, String correlativoFactura, int clienteMPeriodoID, double clienteMDeuda, boolean clienteMAplicaCorte, int clienteMNumMsPagar, double abono, double cargo, long clienteNumCuenta, long numCuenta, long lecturaID) {
        this.periodoID = periodoID;
        this.periodoInicio = periodoInicio;
        this.periodoFinal = periodoFinal;
        this.lecturaAnterior = lecturaAnterior;
        this.lecturaActual = lecturaActual;
        this.correlativoFactura = correlativoFactura;
        this.clienteMPeriodoID = clienteMPeriodoID;
        this.clienteMDeuda = clienteMDeuda;
        this.clienteMAplicaCorte = clienteMAplicaCorte;
        this.clienteMNumMsPagar = clienteMNumMsPagar;
        this.abono = abono;
        this.cargo = cargo;
        this.clienteNumCuenta = clienteNumCuenta;
        this.numCuenta = numCuenta;
        this.lecturaID = lecturaID;
    }

    public Long getPeriodoID() {
        return periodoID;
    }

    public void setPeriodoID(Long periodoID) {
        this.periodoID = periodoID;
    }

    public Date getPeriodoInicio() {
        return periodoInicio;
    }

    public void setPeriodoInicio(Date periodoInicio) {
        this.periodoInicio = periodoInicio;
    }

    public Date getPeriodoFinal() {
        return periodoFinal;
    }

    public void setPeriodoFinal(Date periodoFinal) {
        this.periodoFinal = periodoFinal;
    }

    public double getLecturaAnterior() {
        return lecturaAnterior;
    }

    public void setLecturaAnterior(double lecturaAnterior) {
        this.lecturaAnterior = lecturaAnterior;
    }

    public double getLecturaActual() {
        return lecturaActual;
    }

    public void setLecturaActual(double lecturaActual) {
        this.lecturaActual = lecturaActual;
    }

    public String getCorrelativoFactura() {
        return correlativoFactura;
    }

    public void setCorrelativoFactura(String correlativoFactura) {
        this.correlativoFactura = correlativoFactura;
    }

    public int getClienteMPeriodoID() {
        return clienteMPeriodoID;
    }

    public void setClienteMPeriodoID(int clienteMPeriodoID) {
        this.clienteMPeriodoID = clienteMPeriodoID;
    }

    public double getClienteMDeuda() {
        return clienteMDeuda;
    }

    public void setClienteMDeuda(double clienteMDeuda) {
        this.clienteMDeuda = clienteMDeuda;
    }

    public boolean getClienteMAplicaCorte() {
        return clienteMAplicaCorte;
    }

    public void setClienteMAplicaCorte(boolean clienteMAplicaCorte) {
        this.clienteMAplicaCorte = clienteMAplicaCorte;
    }

    public int getClienteMNumMsPagar() {
        return clienteMNumMsPagar;
    }

    public void setClienteMNumMsPagar(int clienteMNumMsPagar) {
        this.clienteMNumMsPagar = clienteMNumMsPagar;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Date getFechaLectura() {
        return fechaLectura;
    }

    public void setFechaLectura(Date fechaLectura) {
        this.fechaLectura = fechaLectura;
    }

    public double getAbono() {
        return abono;
    }

    public void setAbono(double abono) {
        this.abono = abono;
    }

    public double getCargo() {
        return cargo;
    }

    public void setCargo(double cargo) {
        this.cargo = cargo;
    }

    public Date getPagoCancelado() {
        return pagoCancelado;
    }

    public void setPagoCancelado(Date pagoCancelado) {
        this.pagoCancelado = pagoCancelado;
    }

    public long getClienteNumCuenta() {
        return clienteNumCuenta;
    }

    public void setClienteNumCuenta(long clienteNumCuenta) {
        this.clienteNumCuenta = clienteNumCuenta;
    }

    public long getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(long numCuenta) {
        this.numCuenta = numCuenta;
    }

    public long getLecturaID() {
        return lecturaID;
    }

    public void setLecturaID(long lecturaID) {
        this.lecturaID = lecturaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periodoID != null ? periodoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoLectura)) {
            return false;
        }
        HistoricoLectura other = (HistoricoLectura) object;
        if ((this.periodoID == null && other.periodoID != null) || (this.periodoID != null && !this.periodoID.equals(other.periodoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.HistoricoLectura[ periodoID=" + periodoID + " ]";
    }
    
}

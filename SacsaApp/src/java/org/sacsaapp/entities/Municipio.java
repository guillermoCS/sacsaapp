/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Municipios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Municipio.findAll", query = "SELECT m FROM Municipio m"),
    @NamedQuery(name = "Municipio.findByMunicipioID", query = "SELECT m FROM Municipio m WHERE m.municipioID = :municipioID"),
    @NamedQuery(name = "Municipio.findByDepto", query = "SELECT m FROM Municipio m WHERE m.deptoID = :deptoID"),
    @NamedQuery(name = "Municipio.findByMuniNombre", query = "SELECT m FROM Municipio m WHERE m.muniNombre = :muniNombre")})
public class Municipio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "municipioID")
    @TableGenerator(name = "MunicipioGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "MunicipiosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "MunicipioGenID")
    private Integer municipioID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "muniNombre")
    private String muniNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "municipioID")
    private List<Direccion> direccionList;
    @JoinColumn(name = "deptoID", referencedColumnName = "deptoID")
    @ManyToOne(optional = false)
    private Departamento deptoID;

    public Municipio() {
    }

    public Municipio(Integer municipioID) {
        this.municipioID = municipioID;
    }

    public Municipio(Integer municipioID, String muniNombre) {
        this.municipioID = municipioID;
        this.muniNombre = muniNombre;
    }

    public Integer getMunicipioID() {
        return municipioID;
    }

    public void setMunicipioID(Integer municipioID) {
        this.municipioID = municipioID;
    }

    public String getMuniNombre() {
        return muniNombre;
    }

    public void setMuniNombre(String muniNombre) {
        this.muniNombre = muniNombre;
    }

    @XmlTransient
    public List<Direccion> getDireccionList() {
        return direccionList;
    }

    public void setDireccionList(List<Direccion> direccionList) {
        this.direccionList = direccionList;
    }

    public Departamento getDeptoID() {
        return deptoID;
    }

    public void setDeptoID(Departamento deptoID) {
        this.deptoID = deptoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (municipioID != null ? municipioID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Municipio)) {
            return false;
        }
        Municipio other = (Municipio) object;
        if ((this.municipioID == null && other.municipioID != null) || (this.municipioID != null && !this.municipioID.equals(other.municipioID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Municipio[ municipioID=" + municipioID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Paises")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p"),
    @NamedQuery(name = "Pais.findByPaisID", query = "SELECT p FROM Pais p WHERE p.paisID = :paisID"),
    @NamedQuery(name = "Pais.findByPaisNombre", query = "SELECT p FROM Pais p WHERE p.paisNombre = :paisNombre"),
    @NamedQuery(name = "Pais.findByPaisMoneda", query = "SELECT p FROM Pais p WHERE p.paisMoneda = :paisMoneda"),
    @NamedQuery(name = "Pais.findByPaisMonedaSimbolo", query = "SELECT p FROM Pais p WHERE p.paisMonedaSimbolo = :paisMonedaSimbolo")})
public class Pais implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "paisID")
    @TableGenerator(name = "PaisGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "PaisesSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PaisGenID")
    private Integer paisID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "paisNombre")
    private String paisNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 90)
    @Column(name = "paisMoneda")
    private String paisMoneda;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "paisMonedaSimbolo")
    private String paisMonedaSimbolo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "paisID")
    private List<Departamento> departamentoList;

    public Pais() {
    }

    public Pais(Integer paisID) {
        this.paisID = paisID;
    }

    public Pais(Integer paisID, String paisNombre, String paisMoneda, String paisMonedaSimbolo) {
        this.paisID = paisID;
        this.paisNombre = paisNombre;
        this.paisMoneda = paisMoneda;
        this.paisMonedaSimbolo = paisMonedaSimbolo;
    }

    public Integer getPaisID() {
        return paisID;
    }

    public void setPaisID(Integer paisID) {
        this.paisID = paisID;
    }

    public String getPaisNombre() {
        return paisNombre;
    }

    public void setPaisNombre(String paisNombre) {
        this.paisNombre = paisNombre;
    }

    public String getPaisMoneda() {
        return paisMoneda;
    }

    public void setPaisMoneda(String paisMoneda) {
        this.paisMoneda = paisMoneda;
    }

    public String getPaisMonedaSimbolo() {
        return paisMonedaSimbolo;
    }

    public void setPaisMonedaSimbolo(String paisMonedaSimbolo) {
        this.paisMonedaSimbolo = paisMonedaSimbolo;
    }

    @XmlTransient
    public List<Departamento> getDepartamentoList() {
        return departamentoList;
    }

    public void setDepartamentoList(List<Departamento> departamentoList) {
        this.departamentoList = departamentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paisID != null ? paisID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.paisID == null && other.paisID != null) || (this.paisID != null && !this.paisID.equals(other.paisID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Pais[ paisID=" + paisID + " ]";
    }
    
}

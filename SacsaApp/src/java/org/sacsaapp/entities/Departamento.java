/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Departamentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d"),
    @NamedQuery(name = "Departamento.findByDeptoID", query = "SELECT d FROM Departamento d WHERE d.deptoID = :deptoID"),
    @NamedQuery(name = "Departamento.findByIDPais", query = "SELECT d FROM Departamento d WHERE d.paisID = :paisID"),
    @NamedQuery(name = "Departamento.findByDeptoNombre", query = "SELECT d FROM Departamento d WHERE d.deptoNombre = :deptoNombre")})
public class Departamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "deptoID")
    @TableGenerator(name = "DeptoGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "DeptosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "DeptoGenID")
    private Integer deptoID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "deptoNombre")
    private String deptoNombre;
    @JoinColumn(name = "paisID", referencedColumnName = "paisID")
    @ManyToOne(optional = false)
    private Pais paisID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "deptoID")
    private List<Municipio> municipioList;

    public Departamento() {
    }

    public Departamento(Integer deptoID) {
        this.deptoID = deptoID;
    }

    public Departamento(Integer deptoID, String deptoNombre) {
        this.deptoID = deptoID;
        this.deptoNombre = deptoNombre;
    }

    public Integer getDeptoID() {
        return deptoID;
    }

    public void setDeptoID(Integer deptoID) {
        this.deptoID = deptoID;
    }

    public String getDeptoNombre() {
        return deptoNombre;
    }

    public void setDeptoNombre(String deptoNombre) {
        this.deptoNombre = deptoNombre;
    }

    public Pais getPaisID() {
        return paisID;
    }

    public void setPaisID(Pais paisID) {
        this.paisID = paisID;
    }

    @XmlTransient
    public List<Municipio> getMunicipioList() {
        return municipioList;
    }

    public void setMunicipioList(List<Municipio> municipioList) {
        this.municipioList = municipioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deptoID != null ? deptoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departamento)) {
            return false;
        }
        Departamento other = (Departamento) object;
        if ((this.deptoID == null && other.deptoID != null) || (this.deptoID != null && !this.deptoID.equals(other.deptoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Departamento[ deptoID=" + deptoID + " ]";
    }

}

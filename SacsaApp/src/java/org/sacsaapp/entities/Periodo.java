/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Periodos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periodo.findAll", query = "SELECT p FROM Periodo p"),
    @NamedQuery(name = "Periodo.findByPeriodoID", query = "SELECT p FROM Periodo p WHERE p.periodoID = :periodoID"),
    @NamedQuery(name = "Periodo.findByMes", query = "SELECT p FROM Periodo p WHERE p.mes = :mes"),
    @NamedQuery(name = "Periodo.findByAnyo", query = "SELECT p FROM Periodo p WHERE p.anyo = :anyo"),
    @NamedQuery(name = "Periodo.findBySaldo", query = "SELECT p FROM Periodo p WHERE p.saldo = :saldo")})
public class Periodo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "periodoID")
    @TableGenerator(name = "PeriodoGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "PeriodosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PeriodoGenID")
    private Long periodoID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mes")
    private int mes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anyo")
    private int anyo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Saldo")
    private double saldo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "periodoID")
    private List<Ingreso> ingresoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "periodoID")
    private List<Gasto> gastoList;

    public Periodo() {
    }

    public Periodo(Long periodoID) {
        this.periodoID = periodoID;
    }

    public Periodo(Long periodoID, int mes, int anyo, double saldo) {
        this.periodoID = periodoID;
        this.mes = mes;
        this.anyo = anyo;
        this.saldo = saldo;
    }

    public Long getPeriodoID() {
        return periodoID;
    }

    public void setPeriodoID(Long periodoID) {
        this.periodoID = periodoID;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @XmlTransient
    public List<Ingreso> getIngresoList() {
        return ingresoList;
    }

    public void setIngresoList(List<Ingreso> ingresoList) {
        this.ingresoList = ingresoList;
    }

    @XmlTransient
    public List<Gasto> getGastoList() {
        return gastoList;
    }

    public void setGastoList(List<Gasto> gastoList) {
        this.gastoList = gastoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periodoID != null ? periodoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periodo)) {
            return false;
        }
        Periodo other = (Periodo) object;
        if ((this.periodoID == null && other.periodoID != null) || (this.periodoID != null && !this.periodoID.equals(other.periodoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Periodo[ periodoID=" + periodoID + " ]";
    }
    
}

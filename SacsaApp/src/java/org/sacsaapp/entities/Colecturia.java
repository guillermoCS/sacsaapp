/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Colecturias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Colecturia.findAll", query = "SELECT c FROM Colecturia c"),
    @NamedQuery(name = "Colecturia.findByColecturiaID", query = "SELECT c FROM Colecturia c WHERE c.colecturiaID = :colecturiaID"),
    @NamedQuery(name = "Colecturia.findByCargo", query = "SELECT c FROM Colecturia c WHERE c.cargo = :cargo"),
    @NamedQuery(name = "Colecturia.findByPagoPendiente", query = "SELECT c FROM Colecturia c WHERE c.pagoPendiente = :pagoPendiente"),
    @NamedQuery(name = "Colecturia.findByPagoCancelado", query = "SELECT c FROM Colecturia c WHERE c.pagoCancelado = :pagoCancelado"),
    @NamedQuery(name = "Colecturia.findByClienteMDeuda", query = "SELECT c FROM Colecturia c WHERE c.clienteMDeuda = :clienteMDeuda"),
    @NamedQuery(name = "Colecturia.findByClienteMNumMsPagar", query = "SELECT c FROM Colecturia c WHERE c.clienteMNumMsPagar = :clienteMNumMsPagar"),
    @NamedQuery(name = "Colecturia.findByRegistroFactura", query = "SELECT c FROM Colecturia c WHERE c.registroFactura = :registroFactura"),
    @NamedQuery(name = "Colecturia.findByCorrelativoFactura", query = "SELECT c FROM Colecturia c WHERE c.correlativoFactura = :correlativoFactura")})
public class Colecturia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "colecturiaID")
    @TableGenerator(name = "ColecturiaGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ColecturiasSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ColecturiaGenID")
    private Long colecturiaID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cargo")
    private double cargo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pagoPendiente")
    private boolean pagoPendiente;
    @Column(name = "pagoCancelado")
    @Temporal(TemporalType.DATE)
    private Date pagoCancelado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMDeuda")
    private double clienteMDeuda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clienteMNumMsPagar")
    private int clienteMNumMsPagar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "registroFactura")
    @Temporal(TemporalType.DATE)
    private Date registroFactura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "correlativoFactura")
    private String correlativoFactura;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "colecturia")
    private List<DetalleServicio> detalleServicioList;
    @JoinColumn(name = "lecturaID", referencedColumnName = "lecturaID")
    @ManyToOne(optional = false)
    private Lectura lecturaID;

    public Colecturia() {
    }

    public Colecturia(Long colecturiaID) {
        this.colecturiaID = colecturiaID;
    }

    public Colecturia(Long colecturiaID, double cargo, boolean pagoPendiente, double clienteMDeuda, int clienteMNumMsPagar, Date registroFactura, String correlativoFactura) {
        this.colecturiaID = colecturiaID;
        this.cargo = cargo;
        this.pagoPendiente = pagoPendiente;
        this.clienteMDeuda = clienteMDeuda;
        this.clienteMNumMsPagar = clienteMNumMsPagar;
        this.registroFactura = registroFactura;
        this.correlativoFactura = correlativoFactura;
    }
    
    public Colecturia(double cargo, boolean pagoPendiente, double clienteMDeuda, int clienteMNumMsPagar, Date registroFactura, String correlativoFactura) {
        this.cargo = cargo;
        this.pagoPendiente = pagoPendiente;
        this.clienteMDeuda = clienteMDeuda;
        this.clienteMNumMsPagar = clienteMNumMsPagar;
        this.registroFactura = registroFactura;
        this.correlativoFactura = correlativoFactura;
    }

    public Long getColecturiaID() {
        return colecturiaID;
    }

    public void setColecturiaID(Long colecturiaID) {
        this.colecturiaID = colecturiaID;
    }

    public double getCargo() {
        return cargo;
    }

    public void setCargo(double cargo) {
        this.cargo = cargo;
    }

    public boolean getPagoPendiente() {
        return pagoPendiente;
    }

    public void setPagoPendiente(boolean pagoPendiente) {
        this.pagoPendiente = pagoPendiente;
    }

    public Date getPagoCancelado() {
        return pagoCancelado;
    }

    public void setPagoCancelado(Date pagoCancelado) {
        this.pagoCancelado = pagoCancelado;
    }

    public double getClienteMDeuda() {
        return clienteMDeuda;
    }

    public void setClienteMDeuda(double clienteMDeuda) {
        this.clienteMDeuda = clienteMDeuda;
    }

    public int getClienteMNumMsPagar() {
        return clienteMNumMsPagar;
    }

    public void setClienteMNumMsPagar(int clienteMNumMsPagar) {
        this.clienteMNumMsPagar = clienteMNumMsPagar;
    }

    public Date getRegistroFactura() {
        return registroFactura;
    }

    public void setRegistroFactura(Date registroFactura) {
        this.registroFactura = registroFactura;
    }

    public String getCorrelativoFactura() {
        return correlativoFactura;
    }

    public void setCorrelativoFactura(String correlativoFactura) {
        this.correlativoFactura = correlativoFactura;
    }

    @XmlTransient
    public List<DetalleServicio> getDetalleServicioList() {
        return detalleServicioList;
    }

    public void setDetalleServicioList(List<DetalleServicio> detalleServicioList) {
        this.detalleServicioList = detalleServicioList;
    }

    public Lectura getLecturaID() {
        return lecturaID;
    }

    public void setLecturaID(Lectura lecturaID) {
        this.lecturaID = lecturaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colecturiaID != null ? colecturiaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colecturia)) {
            return false;
        }
        Colecturia other = (Colecturia) object;
        if ((this.colecturiaID == null && other.colecturiaID != null) || (this.colecturiaID != null && !this.colecturiaID.equals(other.colecturiaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Colecturia[ colecturiaID=" + colecturiaID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Personas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p"),
    @NamedQuery(name = "Persona.findByPersonaID", query = "SELECT p FROM Persona p WHERE p.personaID = :personaID"),
    @NamedQuery(name = "Persona.findByNombre1", query = "SELECT p FROM Persona p WHERE p.nombre1 = :nombre1"),
    @NamedQuery(name = "Persona.findByApellido1", query = "SELECT p FROM Persona p WHERE p.apellido1 = :apellido1"),
    @NamedQuery(name = "Persona.findByPersonaFechNacimiento", query = "SELECT p FROM Persona p WHERE p.personaFechNacimiento = :personaFechNacimiento"),
    @NamedQuery(name = "Persona.findByPersonaCorreo", query = "SELECT p FROM Persona p WHERE p.personaCorreo = :personaCorreo"),
    @NamedQuery(name = "Persona.findByPersonaFechaRegistro", query = "SELECT p FROM Persona p WHERE p.personaFechaRegistro = :personaFechaRegistro"),
    @NamedQuery(name = "Persona.findByPersonaEstado", query = "SELECT p FROM Persona p WHERE p.personaEstado = :personaEstado"),
    @NamedQuery(name = "Persona.findByIdDocumento", query = "SELECT p FROM Persona p WHERE p.idDocumento = :idDocumento"),
    @NamedQuery(name = "Persona.findByNombre2", query = "SELECT p FROM Persona p WHERE p.nombre2 = :nombre2"),
    @NamedQuery(name = "Persona.findByApellido2", query = "SELECT p FROM Persona p WHERE p.apellido2 = :apellido2"),
    @NamedQuery(name = "Persona.findByEstadoCivil", query = "SELECT p FROM Persona p WHERE p.estadoCivil = :estadoCivil"),
    @NamedQuery(name = "Persona.findByGenero", query = "SELECT p FROM Persona p WHERE p.genero = :genero"),    
    @NamedQuery(name = "Persona.findByPrueba", query = "SELECT p FROM Persona p WHERE p.nombre1 = :name OR p.personaFechNacimiento = :fechaB"),
    @NamedQuery(name = "Persona.findByUsuario", query = "SELECT p FROM Persona p WHERE p NOT IN (SELECT u.personaspersonaID FROM Usuario u)"),
    @NamedQuery(name = "Persona.findByFechaBaja", query = "SELECT p FROM Persona p WHERE p.fechaBaja = :fechaBaja")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "personaID")
    @TableGenerator(name = "PersonaGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "PersonasSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PersonaGenID")
    private Long personaID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre1")
    private String nombre1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellido1")
    private String apellido1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "personaFechNacimiento")
    @Temporal(TemporalType.DATE)
    private Date personaFechNacimiento;
    @Size(max = 155)
    @Column(name = "personaCorreo")
    private String personaCorreo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "personaFechaRegistro")
    @Temporal(TemporalType.DATE)
    private Date personaFechaRegistro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "personaEstado")
    private boolean personaEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "idDocumento")
    private String idDocumento;
    @Size(max = 100)
    @Column(name = "nombre2")
    private String nombre2;
    @Size(max = 100)
    @Column(name = "apellido2")
    private String apellido2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "estadoCivil")
    private String estadoCivil;
    @Size(max = 1)
    @Column(name = "genero")
    private String genero;
    @Column(name = "fechaBaja")
    @Temporal(TemporalType.DATE)
    private Date fechaBaja;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaspersonaID")
    private List<Usuario> usuarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaspersonaID")
    private List<Afiliado> afiliadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaID")
    private List<Direccion> direccionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaID")
    private List<Cliente> clienteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personaID")
    private List<Telefono> telefonoList;

    public Persona() {
    }

    public Persona(Long personaID) {
        this.personaID = personaID;
    }

    public Persona(Long personaID, String nombre1, String apellido1, Date personaFechNacimiento, Date personaFechaRegistro, boolean personaEstado, String idDocumento, String estadoCivil) {
        this.personaID = personaID;
        this.nombre1 = nombre1;
        this.apellido1 = apellido1;
        this.personaFechNacimiento = personaFechNacimiento;
        this.personaFechaRegistro = personaFechaRegistro;
        this.personaEstado = personaEstado;
        this.idDocumento = idDocumento;
        this.estadoCivil = estadoCivil;
    }

    public Long getPersonaID() {
        return personaID;
    }

    public void setPersonaID(Long personaID) {
        this.personaID = personaID;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public Date getPersonaFechNacimiento() {
        
        return personaFechNacimiento;
    }

    public void setPersonaFechNacimiento(Date personaFechNacimiento) {
        this.personaFechNacimiento = personaFechNacimiento;
    }

    public String getPersonaCorreo() {
        return personaCorreo;
    }

    public void setPersonaCorreo(String personaCorreo) {
        this.personaCorreo = personaCorreo;
    }

    public Date getPersonaFechaRegistro() {
        return personaFechaRegistro;
    }

    public void setPersonaFechaRegistro(Date personaFechaRegistro) {
        this.personaFechaRegistro = personaFechaRegistro;
    }

    public boolean getPersonaEstado() {
        return personaEstado;
    }

    public void setPersonaEstado(boolean personaEstado) {
        this.personaEstado = personaEstado;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @XmlTransient
    public List<Afiliado> getAfiliadoList() {
        return afiliadoList;
    }

    public void setAfiliadoList(List<Afiliado> afiliadoList) {
        this.afiliadoList = afiliadoList;
    }

    @XmlTransient
    public List<Direccion> getDireccionList() {
        return direccionList;
    }

    public void setDireccionList(List<Direccion> direccionList) {
        this.direccionList = direccionList;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @XmlTransient
    public List<Telefono> getTelefonoList() {
        return telefonoList;
    }

    public void setTelefonoList(List<Telefono> telefonoList) {
        this.telefonoList = telefonoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personaID != null ? personaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.personaID == null && other.personaID != null) || (this.personaID != null && !this.personaID.equals(other.personaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String nombreCompleto = (this.nombre1.trim().length() < 0) ? "" : this.nombre1+" ";
        nombreCompleto += (this.nombre2.trim().length() < 0) ? "" : this.nombre2+" ";
        nombreCompleto += (this.apellido1.trim().length() < 0) ? "" : this.apellido1+" ";
        nombreCompleto += (this.apellido2.trim().length() < 0) ? "" : this.apellido2;
        return nombreCompleto;
    }

}

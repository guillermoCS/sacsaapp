/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Modificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modificacion.findAll", query = "SELECT m FROM Modificacion m"),
    @NamedQuery(name = "Modificacion.findByModID", query = "SELECT m FROM Modificacion m WHERE m.modID = :modID"),
    @NamedQuery(name = "Modificacion.findByModTabla", query = "SELECT m FROM Modificacion m WHERE m.modTabla = :modTabla"),
    @NamedQuery(name = "Modificacion.findByModFechaActualizacion", query = "SELECT m FROM Modificacion m WHERE m.modFechaActualizacion = :modFechaActualizacion"),
    @NamedQuery(name = "Modificacion.findByModUsuarioEditor", query = "SELECT m FROM Modificacion m WHERE m.modUsuarioEditor = :modUsuarioEditor"),
    @NamedQuery(name = "Modificacion.findByModDescripcion", query = "SELECT m FROM Modificacion m WHERE m.modDescripcion = :modDescripcion")})
public class Modificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "modID")
    @TableGenerator(name = "ModificacionGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ModificacionesSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ModificacionGenID")
    private Long modID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "modTabla")
    private String modTabla;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modFechaActualizacion")
    @Temporal(TemporalType.DATE)
    private Date modFechaActualizacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modUsuarioEditor")
    private int modUsuarioEditor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "modDescripcion")
    private String modDescripcion;

    public Modificacion() {
    }

    public Modificacion(Long modID) {
        this.modID = modID;
    }

    public Modificacion(Long modID, String modTabla, Date modFechaActualizacion, int modUsuarioEditor, String modDescripcion) {
        this.modID = modID;
        this.modTabla = modTabla;
        this.modFechaActualizacion = modFechaActualizacion;
        this.modUsuarioEditor = modUsuarioEditor;
        this.modDescripcion = modDescripcion;
    }

    public Long getModID() {
        return modID;
    }

    public void setModID(Long modID) {
        this.modID = modID;
    }

    public String getModTabla() {
        return modTabla;
    }

    public void setModTabla(String modTabla) {
        this.modTabla = modTabla;
    }

    public Date getModFechaActualizacion() {
        return modFechaActualizacion;
    }

    public void setModFechaActualizacion(Date modFechaActualizacion) {
        this.modFechaActualizacion = modFechaActualizacion;
    }

    public int getModUsuarioEditor() {
        return modUsuarioEditor;
    }

    public void setModUsuarioEditor(int modUsuarioEditor) {
        this.modUsuarioEditor = modUsuarioEditor;
    }

    public String getModDescripcion() {
        return modDescripcion;
    }

    public void setModDescripcion(String modDescripcion) {
        this.modDescripcion = modDescripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (modID != null ? modID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modificacion)) {
            return false;
        }
        Modificacion other = (Modificacion) object;
        if ((this.modID == null && other.modID != null) || (this.modID != null && !this.modID.equals(other.modID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Modificacion[ modID=" + modID + " ]";
    }
    
}

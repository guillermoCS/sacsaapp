/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author guillermo
 */
@Embeddable
public class DetalleServicioPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobroID")
    private int cobroID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "colecturiasLecturaID")
    private long colecturiasLecturaID;

    public DetalleServicioPK() {
    }

    public DetalleServicioPK(int cobroID, long colecturiasLecturaID) {
        this.cobroID = cobroID;
        this.colecturiasLecturaID = colecturiasLecturaID;
    }

    public int getCobroID() {
        return cobroID;
    }

    public void setCobroID(int cobroID) {
        this.cobroID = cobroID;
    }

    public long getColecturiasLecturaID() {
        return colecturiasLecturaID;
    }

    public void setColecturiasLecturaID(long colecturiasLecturaID) {
        this.colecturiasLecturaID = colecturiasLecturaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) cobroID;
        hash += (int) colecturiasLecturaID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleServicioPK)) {
            return false;
        }
        DetalleServicioPK other = (DetalleServicioPK) object;
        if (this.cobroID != other.cobroID) {
            return false;
        }
        if (this.colecturiasLecturaID != other.colecturiasLecturaID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.DetalleServicioPK[ cobroID=" + cobroID + ", colecturiasLecturaID=" + colecturiasLecturaID + " ]";
    }
    
}

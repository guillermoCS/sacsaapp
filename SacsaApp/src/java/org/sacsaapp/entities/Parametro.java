/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Parametros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parametro.findAll", query = "SELECT p FROM Parametro p"),
    @NamedQuery(name = "Parametro.findByParametroID", query = "SELECT p FROM Parametro p WHERE p.parametroID = :parametroID"),
    @NamedQuery(name = "Parametro.findByInicioLectura", query = "SELECT p FROM Parametro p WHERE p.inicioLectura = :inicioLectura"),
    @NamedQuery(name = "Parametro.findByFinLectura", query = "SELECT p FROM Parametro p WHERE p.finLectura = :finLectura"),
    @NamedQuery(name = "Parametro.findByCuotaBase", query = "SELECT p FROM Parametro p WHERE p.cuotaBase = :cuotaBase"),
    @NamedQuery(name = "Parametro.findByNombreInstitucion", query = "SELECT p FROM Parametro p WHERE p.nombreInstitucion = :nombreInstitucion"),
    @NamedQuery(name = "Parametro.findByFechaSistema", query = "SELECT p FROM Parametro p WHERE p.fechaSistema = :fechaSistema"),
    @NamedQuery(name = "Parametro.findByFechaAperturaSistema", query = "SELECT p FROM Parametro p WHERE p.fechaAperturaSistema = :fechaAperturaSistema"),
    @NamedQuery(name = "Parametro.findByLimitedeConsumo", query = "SELECT p FROM Parametro p WHERE p.limitedeConsumo = :limitedeConsumo"),
    @NamedQuery(name = "Parametro.findByValorIncremento", query = "SELECT p FROM Parametro p WHERE p.valorIncremento = :valorIncremento"),
    @NamedQuery(name = "Parametro.findByDiaLimitePago", query = "SELECT p FROM Parametro p WHERE p.diaLimitePago = :diaLimitePago"),
    @NamedQuery(name = "Parametro.findByNumFactura", query = "SELECT p FROM Parametro p WHERE p.numFactura = :numFactura")})
public class Parametro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "parametroID")
    @TableGenerator(name = "ParametroGenID", table = "Secuencias", pkColumnName = "nombreSeq", pkColumnValue = "ParametrosSecuencia", valueColumnName = "valorSeq")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ParametroGenID")
    private Integer parametroID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "InicioLectura")
    private int inicioLectura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FinLectura")
    private int finLectura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cuotaBase")
    private double cuotaBase;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombreInstitucion")
    private String nombreInstitucion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaSistema")
    @Temporal(TemporalType.DATE)
    private Date fechaSistema;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaAperturaSistema")
    @Temporal(TemporalType.DATE)
    private Date fechaAperturaSistema;
    @Basic(optional = false)
    @NotNull
    @Column(name = "limitedeConsumo")
    private int limitedeConsumo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valorIncremento")
    private double valorIncremento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "diaLimitePago")
    private int diaLimitePago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "numFactura")
    private String numFactura;

    public Parametro() {
    }

    public Parametro(Integer parametroID) {
        this.parametroID = parametroID;
    }

    public Parametro(Integer parametroID, int inicioLectura, int finLectura, double cuotaBase, String nombreInstitucion, Date fechaSistema, Date fechaAperturaSistema, int limitedeConsumo, double valorIncremento, int diaLimitePago, String numFactura) {
        this.parametroID = parametroID;
        this.inicioLectura = inicioLectura;
        this.finLectura = finLectura;
        this.cuotaBase = cuotaBase;
        this.nombreInstitucion = nombreInstitucion;
        this.fechaSistema = fechaSistema;
        this.fechaAperturaSistema = fechaAperturaSistema;
        this.limitedeConsumo = limitedeConsumo;
        this.valorIncremento = valorIncremento;
        this.diaLimitePago = diaLimitePago;
        this.numFactura = numFactura;
    }

    public Integer getParametroID() {
        return parametroID;
    }

    public void setParametroID(Integer parametroID) {
        this.parametroID = parametroID;
    }

    public int getInicioLectura() {
        return inicioLectura;
    }

    public void setInicioLectura(int inicioLectura) {
        this.inicioLectura = inicioLectura;
    }

    public int getFinLectura() {
        return finLectura;
    }

    public void setFinLectura(int finLectura) {
        this.finLectura = finLectura;
    }

    public double getCuotaBase() {
        return cuotaBase;
    }

    public void setCuotaBase(double cuotaBase) {
        this.cuotaBase = cuotaBase;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public Date getFechaSistema() {
        return fechaSistema;
    }

    public void setFechaSistema(Date fechaSistema) {
        this.fechaSistema = fechaSistema;
    }

    public Date getFechaAperturaSistema() {
        return fechaAperturaSistema;
    }

    public void setFechaAperturaSistema(Date fechaAperturaSistema) {
        this.fechaAperturaSistema = fechaAperturaSistema;
    }

    public int getLimitedeConsumo() {
        return limitedeConsumo;
    }

    public void setLimitedeConsumo(int limitedeConsumo) {
        this.limitedeConsumo = limitedeConsumo;
    }

    public double getValorIncremento() {
        return valorIncremento;
    }

    public void setValorIncremento(double valorIncremento) {
        this.valorIncremento = valorIncremento;
    }

    public int getDiaLimitePago() {
        return diaLimitePago;
    }

    public void setDiaLimitePago(int diaLimitePago) {
        this.diaLimitePago = diaLimitePago;
    }

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parametroID != null ? parametroID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parametro)) {
            return false;
        }
        Parametro other = (Parametro) object;
        if ((this.parametroID == null && other.parametroID != null) || (this.parametroID != null && !this.parametroID.equals(other.parametroID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Parametro[ parametroID=" + parametroID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author guillermo
 */
@Entity
@Table(name = "Ingresos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingreso.findAll", query = "SELECT i FROM Ingreso i"),
    @NamedQuery(name = "Ingreso.findByIngresoID", query = "SELECT i FROM Ingreso i WHERE i.ingresoID = :ingresoID"),
    @NamedQuery(name = "Ingreso.findByTotalMensual", query = "SELECT i FROM Ingreso i WHERE i.totalMensual = :totalMensual")})
public class Ingreso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ingresoID")
    private Long ingresoID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalMensual")
    private double totalMensual;
    @JoinColumn(name = "periodoID", referencedColumnName = "periodoID")
    @ManyToOne(optional = false)
    private Periodo periodoID;

    public Ingreso() {
    }

    public Ingreso(Long ingresoID) {
        this.ingresoID = ingresoID;
    }

    public Ingreso(Long ingresoID, double totalMensual) {
        this.ingresoID = ingresoID;
        this.totalMensual = totalMensual;
    }

    public Long getIngresoID() {
        return ingresoID;
    }

    public void setIngresoID(Long ingresoID) {
        this.ingresoID = ingresoID;
    }

    public double getTotalMensual() {
        return totalMensual;
    }

    public void setTotalMensual(double totalMensual) {
        this.totalMensual = totalMensual;
    }

    public Periodo getPeriodoID() {
        return periodoID;
    }

    public void setPeriodoID(Periodo periodoID) {
        this.periodoID = periodoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ingresoID != null ? ingresoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingreso)) {
            return false;
        }
        Ingreso other = (Ingreso) object;
        if ((this.ingresoID == null && other.ingresoID != null) || (this.ingresoID != null && !this.ingresoID.equals(other.ingresoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.sacsaapp.entities.Ingreso[ ingresoID=" + ingresoID + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.TipoDocumentoFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.TipoDocumento;

/**
 *
 * @author guillermo
 */
@Stateless
public class TipoDocumentoFacade extends AbstractFacade<TipoDocumento> implements TipoDocumentoFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoDocumentoFacade() {
        super(TipoDocumento.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "TipoDocumento.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "TipoDocumento.findByDocumentoID";
                params.add("documentoID");
                break;
            case 2:
                resp = "TipoDocumento.findByTipo";
                params.add("tipo");
                break;
           
        }
        this.setListNombres(params);
        return resp;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.CuentaPorCobrarFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.CuentaPorCobrar;

/**
 *
 * @author guillermo
 */
@Stateless
public class CuentaPorCobrarFacade extends AbstractFacade<CuentaPorCobrar> implements CuentaPorCobrarFacadeLocal {

    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CuentaPorCobrarFacade() {
        super(CuentaPorCobrar.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "CuentaPorCobrar.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
         *   Este switch dependerá de cada entity
         *   agreguen más case en caso de ser necesario.
         *   Quitar los case que estén demás.
         *   resp == el nombre de la NamedQuery
         *   params.add  == es para cada nombre de parámetro que se va a pasar 
         */
        switch (numQ) {
            case 1:
                resp = "CuentaPorCobrar.findByClienteNumeroCuenta";
                params.add("clienteNumeroCuenta");
                break;
            case 2:
                resp = "CuentaPorCobrar.findByNumeroCuenta";
                params.add("numeroCuenta");
                break;
            case 3:
                resp = "CuentaPorCobrar.findCliente";
                params.add("numCuenta");
                break;
        }
        this.setListNombres(params);
        return resp;
    }

}

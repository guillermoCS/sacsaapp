/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.HistoricoLecturaFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.HistoricoLectura;

/**
 *
 * @author guillermo
 */
@Stateless
public class HistoricoLecturaFacade extends AbstractFacade<HistoricoLectura> implements HistoricoLecturaFacadeLocal {

    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HistoricoLecturaFacade() {
        super(HistoricoLectura.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "HistoricoLectura.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
         *   Este switch dependerá de cada entity
         *   agreguen más case en caso de ser necesario.
         *   Quitar los case que estén demás.
         *   resp == el nombre de la NamedQuery
         *   params.add  == es para cada nombre de parámetro que se va a pasar 
         */
        switch (numQ) {
            case 1:
                resp = "HistoricoLectura.findByClienteNumCuenta";
                params.add("clienteNumCuenta");
                break;
            case 2:
                resp = "HistoricoLectura.findByLecturaID";
                params.add("lecturaID");
                break;
            case 3:
                resp = "HistoricoLectura.findLikeFechaLectura";
                params.add("anyo");
                params.add("mes");
                params.add("numCuenta");
                break;
            case 4:
                resp = "HistoricoLectura.findByNumCuenta";
                params.add("numCuenta");
                break;

            case 5:
                resp = "HistoricoLectura.findBetweenFechaLectura";
                params.add("numCuenta");
                params.add("desde");
                params.add("hasta");
                break;
        }
        this.setListNombres(params);
        return resp;
    }

}

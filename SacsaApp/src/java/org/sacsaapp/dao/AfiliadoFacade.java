/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.dao;

import java.util.List;
import org.sacsaapp.interfaces.AfiliadoFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.sacsaapp.entities.Abono;
import org.sacsaapp.entities.Afiliado;
import java.util.ArrayList;

/**
 *
 * @author guillermo
 */
@Stateless
public class AfiliadoFacade extends AbstractFacade<Afiliado> implements AfiliadoFacadeLocal {

    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AfiliadoFacade() {
        super(Afiliado.class);
    }

    @Override
    protected String getQueryFindAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected String getNamedQuery(int numQ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Afiliado> filtrarPorIDAfiliado(Afiliado ob) {
        List<Afiliado> lista = null;
        Query q = em.createNamedQuery("Afiliado.findByNumeroIDpersona");
        q.setParameter("personaspersonaID", ob.getPersonaspersonaID());
        lista = q.getResultList();
        return lista;

    }

}

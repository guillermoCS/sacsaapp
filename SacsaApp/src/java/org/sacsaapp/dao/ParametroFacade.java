/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.ParametroFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Parametro;

/**
 *
 * @author guillermo
 */
@Stateless
public class ParametroFacade extends AbstractFacade<Parametro> implements ParametroFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParametroFacade() {
        super(Parametro.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "Parametro.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Parametro.findByParametroID";
                params.add("parametroID");
                break;
            case 2:
                resp = "Parametro.findByInicioLectura";
                params.add("inicioLectura");
                break;
            case 3:
                resp = "Parametro.findByFinLectura";
                params.add("finLectura");
                break;
            case 4:
                resp = "Parametro.findByCuotaBase";
                params.add("cuotaBase");
                break;
            case 5:
                resp = "Parametro.findByNombreInstitucion";
                params.add("nombreInstitucion");
                break;
            case 6:
                resp = "Parametro.findByFechaSistema";
                params.add("fechaSistema");
                break;
            case 7:
                resp = "Parametro.findByFechaAperturaSistema";
                params.add("fechaAperturaSistema");
                break;
            case 8:
                resp = "Parametro.findByLimitedeConsumo";
                params.add("limitedeConsumo");
                break;
            case 9:
                resp = "Parametro.findByValorIncremento";
                params.add("valorIncremento");
                break;
            case 10:
                resp = "Parametro.findByDiaLimitePago";
                params.add("diaLimitePago");
                break;
            case 11:
                resp = "Parametro.findByNumFactura";
                params.add("numFactura");
                break;
            default:
                resp = "";
                this.setListNombres(null);
                break;
        }
        this.setListNombres(params);
        return resp;
    }
    
}

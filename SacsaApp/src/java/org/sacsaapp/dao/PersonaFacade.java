/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.PersonaFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Persona;

/**
 *
 * @author guillermo
 */
@Stateless
public class PersonaFacade extends AbstractFacade<Persona> implements PersonaFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonaFacade() {
        super(Persona.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "Persona.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Persona.findByNombre1";
                params.add("nombre1");
                break;
            case 2:
                resp = "Persona.findByApellido1";
                params.add("apellido1");
                break;
            case 3:
                resp = "Persona.findByNombre2";
                params.add("");
                break;
            case 4:
                resp = "Persona.findByApellido2";
                params.add("apellido2");
                break;
            case 5:
                resp = "Persona.findByPersonaFechNacimiento";
                params.add("personaFechNacimiento");
                break;
            case 6:
                resp = "Persona.findByPersonaCorreo";
                params.add("personaCorreo");
                break;
            case 7:
                resp = "Persona.findByPersonaFechaRegistro";
                params.add("personaFechaRegistro");
                break;
            case 8:
                resp = "Persona.findByIdDocumento";
                params.add("idDocumento");
                break;
            case 9:
                resp = "Persona.findByEstadoCivil";
                params.add("estadoCivil");
                break;
            case 10:
                resp = "Persona.findByGenero";
                params.add("genero");
                break;
            case 11:
                resp = "Persona.findByFechaBaja";
                params.add("fechaBaja");
                break;
            case 12:
                resp = "Persona.findByPersonaEstado";
                params.add("personaEstado");
                break;
            case 13:
                resp = "Persona.findByPrueba";
                params.add("name");
                params.add("fechaB");                
        }
        this.setListNombres(params);
        return resp;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.UsuarioFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Usuario;

/**
 *
 * @author guillermo
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "Usuario.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Usuario.findByUsuarioID";
                params.add("usuarioID");
                break;
            case 2:
                resp = "Usuario.findByUsuarioNombre";
                params.add("usuarioNombre");
                break;
            case 3:
                resp = "Usuario.ValidateUsuario";
                params.add("usuarioNombre");
                params.add("usuarioPassword");
                break;
            case 4:
                resp = "Usuario.findByUsuarioEstado";
                params.add("usuarioEstado");
                break;
        }
        this.setListNombres(params);
        return resp;
    }
    
}

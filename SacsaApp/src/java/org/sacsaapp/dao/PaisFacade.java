/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.PaisFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Pais;

/**
 *
 * @author guillermo
 */
@Stateless
public class PaisFacade extends AbstractFacade<Pais> implements PaisFacadeLocal {

    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaisFacade() {
        super(Pais.class);
    }

    @Override
    protected String getQueryFindAll() {
      return "Pais.findAll";   
    }

    @Override
    protected String getNamedQuery(int numQ) {
         List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Pais.findByPaisID";
                params.add("paisID");
                break;
            case 2:
                resp = "Pais.findByPaisNombre";
                params.add("paisNombre");
                break;
            case 3:
                resp = "Pais.findByPaisMoneda";
                params.add("paisMoneda");
                break;
            case 4:
                resp = "Pais.findByPaisMonedaSimbolo";
                params.add("paisMonedaSimbolo");
                break;
            default:
                resp = "";
                this.setListNombres(null);
                break;
        }
        this.setListNombres(params);
        return resp;
    }

}

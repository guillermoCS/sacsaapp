/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.LecturaFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Lectura;

/**
 *
 * @author guillermo
 */
@Stateless
public class LecturaFacade extends AbstractFacade<Lectura> implements LecturaFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LecturaFacade() {
        super(Lectura.class);
    }

    @Override
    protected String getQueryFindAll() {
        return "Lectura.findAll";
    }

    @Override
    protected String getNamedQuery(int numQ) {
       List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Lectura.findByLecturaID";
                params.add("lecturaID");
                break;
            case 2:
                resp = "Lectura.findByFechaLectura";
                params.add("fechaLectura");
                break;
            case 3:
                resp = "Lectura.findByValorLectura";
                params.add("valorLectura");
                break;
            case 4:
                resp = "Lectura.findByPeriodoInicio";
                params.add("periodoInicio");
                break;
            case 5:
                resp = "Lectura.findByPeriodoFinal";
                params.add("periodoFinal");
                break;
            case 6:
                resp = "Lectura.findByIDLector";
                params.add("iDLector");
                break;
            case 7:
                resp = "Lectura.findToMesAPagar";
                params.add("anyo");
                params.add("mes");
                break;                    
            default:
                resp = "";
                this.setListNombres(null);
                break;
        }
        this.setListNombres(params);
        return resp;
    }
    
}

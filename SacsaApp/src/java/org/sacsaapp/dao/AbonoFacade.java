/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.AbonoFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.sacsaapp.entities.Abono;

/**
 *
 * @author guillermo
 */
@Stateless
public class AbonoFacade extends AbstractFacade<Abono> implements AbonoFacadeLocal {

    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AbonoFacade() {
        super(Abono.class);
    }
    

    @Override
    protected String getQueryFindAll() {
        return "Abono.findAll";
    }
    
    
    @Override
    public List<Abono> filtrarPorNumeroCuenta(Abono ob){
      List<Abono> lista = null;
      Query q = em.createNamedQuery("Abono.findAllAbonoByCuenta");
      q.setParameter("cuentasporCobrarClientescuenta", ob.getCuentasporCobrarClientescuenta());
      lista = q.getResultList();
      return lista;
    
    }


    @Override
    protected String getNamedQuery(int numQ) {
        List<String> params = new ArrayList<>();
        String resp = "";
        /*
         *   Este switch dependerá de cada entity
         *   agreguen más case en caso de ser necesario.
         *   Quitar los case que estén demás.
         *   resp == el nombre de la NamedQuery
         *   params.add  == es para cada nombre de parámetro que se va a pasar 
         */
        switch (numQ) {
            case 1:
                resp = "Abono.findAll";
                params.add("Abono");
                break;
            case 2:
                resp = "Abono.findByAbonoID";
                params.add("abonoID");
                break;
            case 3:
                resp = "Abono.findByAbonoActual";
                params.add("");
                break;
            case 4:
                resp = "Abono.findByAbonoAnterior";
                params.add("abonoAnterior");
                break;
            case 5:
                resp = "Abono.findByAbonoAnterior";
                params.add("abonoAnterior");
                break;
            case 6:
                resp = "Abono.findByAbonoFechaRegistro";
                params.add("abonoFechaRegistro");
                break;
        }
        this.setListNombres(params);
        return resp;
    }

}

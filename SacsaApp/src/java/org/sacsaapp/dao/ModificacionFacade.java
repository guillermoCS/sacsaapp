/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.util.ArrayList;
import java.util.List;
import org.sacsaapp.interfaces.ModificacionFacadeLocal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.sacsaapp.entities.Modificacion;

/**
 *
 * @author guillermo
 */
@Stateless
public class ModificacionFacade extends AbstractFacade<Modificacion> implements ModificacionFacadeLocal {
    @PersistenceContext(unitName = "SacsaAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModificacionFacade() {
        super(Modificacion.class);
    }

    @Override
    protected String getQueryFindAll() {
       return "Modificacion.findAll";  
    }

    @Override
    protected String getNamedQuery(int numQ) {
         List<String> params = new ArrayList<>();
        String resp = "";
        /*
        *   Este switch dependerá de cada entity
        *   agreguen más case en caso de ser necesario.
        *   Quitar los case que estén demás.
        *   resp == el nombre de la NamedQuery
        *   params.add  == es para cada nombre de parámetro que se va a pasar 
        */
        switch (numQ) {
            case 1:
                resp = "Modificacion.findByModID";
                params.add("modID");
                break;
            case 2:
                resp = "Modificacion.findByModTabla";
                params.add("modTabla");
                break;
            case 3:
                resp = "Modificacion.findByModFechaActualizacion";
                params.add("modFechaActualizacion");
                break;
            case 4:
                resp = "Modificacion.findByModUsuarioEditor";
                params.add("modUsuarioEditor");
                break;
            case 5:
                resp = "Modificacion.findByModDescripcion";
                params.add("modDescripcion");
                break;
            default:
                resp = "";
                this.setListNombres(null);
                break;
        }
        this.setListNombres(params);
        return resp;
    }
    
}

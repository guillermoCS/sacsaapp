/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.sacsaapp.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author guillermo
 */
public abstract class AbstractFacade<T> {
    
    private Class<T> entityClass;
    
    private List<String> listNombres;
    private Map mpParams;
    private int numQuery;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();
    
    protected abstract String getQueryFindAll();

    protected abstract String getNamedQuery(int numQ);

    public void create(T entity) {
        getEntityManager().persist(entity);
        Cache cache = getEntityManager().getEntityManagerFactory().getCache();
        cache.evictAll();
        getEntityManager().flush();
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        Query q = getEntityManager().createNamedQuery("" + this.getQueryFindAll() + "");
        List<T> listObj = q.getResultList();
        return listObj;
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public List<T> findwithParameters(T entity) {

        try {
            this.mpParams = new HashMap();
            Query q = getEntityManager().createNamedQuery("" + this.getNamedQuery(this.numQuery) + "");
            this.obtieneMetodos(entity, this.listNombres);
            Iterator it = this.mpParams.keySet().iterator();
            
            while (it.hasNext()){
                String param = it.next().toString();
                String val = this.mpParams.get(param).toString();
                System.out.println("Valores:: param-> " + param + " value-> " + val);
                q.setParameter("" + param + "", this.mpParams.get(param));
            }
            
            List<T> listObj = q.getResultList();
            return listObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
    }
    
    /*
    *   Método que utiliza una nameQuery con parámetros personalizados para retornar una lista de entities.
    *   Recibe: un Objeto de tipo entitie, un hasmap con los parámetros. 
    */
    public List<T> findwithParameters(T entity, ArrayList<String> indices, ArrayList valores) {

        try {            
            Query q = getEntityManager().createNamedQuery("" + this.getNamedQuery(this.numQuery) + "");
                        
            for (int i = 0; i < indices.size(); i++) {
                q.setParameter("" + indices.get(i) + "", valores.get(i));
            }
                        
            List<T> listObj = q.getResultList();
            return listObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
    }

    /*
    *   Método que sirve para buscar una entity usando nameQueries con parámetros.
    *   
    */
    public T findOnewithParameters(T entity) {
        T obj = null;
        try {
            Query q = getEntityManager().createNamedQuery("" + this.getNamedQuery(this.numQuery) + "");

            this.obtieneMetodos(entity, this.listNombres);
            Iterator it = this.mpParams.keySet().iterator();
            
            while (it.hasNext()){
                String param = it.next().toString();
                String val = this.mpParams.get(param).toString();
                System.out.println("Valores:: param-> " + param + " value-> " + val);
                q.setParameter("" + param + "", this.mpParams.get(param));
            }

            obj = (T) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }            

        return obj;
    }
    
    /*
    *   Método que crea un Arreglo entre nombre de párametros (como llave) y los valores a enviar.
    *   Recibe: un Objeto de tipo entitie, una lista de nombres de parámetros. 
    */
    public void obtieneMetodos(Object obj, List<String> listNombres) {
        Map mp = new HashMap();

        Class cl = obj.getClass();
        Field[] listF = cl.getDeclaredFields();

        try {
            
            for (int i = 0; i < listNombres.size(); i++) {
                for (Field field : listF) {
                    field.setAccessible(true);
                    Object nObj = field.get(obj);
                    if (field.getName().trim().equals(listNombres.get(i).toString().trim())){
                        mp.put(listNombres.get(i).toString().trim(), nObj);
                    }
                }
            }
            this.setMpParams(mp);
            //Obtener e imprimir los valores del HashMap
//            Iterator it = mp.keySet().iterator();
//            
//            while (it.hasNext()){
//                String param = it.next().toString();
//                String val = mp.get(param).toString();
//                System.out.println("Valores:: param-> " + param + " value-> " + val);
//            }
 
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    /*
    *   Método que manda a llamar una nameQuery que no necesita recibir parámetros !.
    *   Retorna una Lista.
    */
    public List<T> findByNameQuery(String nameQuery) {
        Query q = getEntityManager().createNamedQuery(nameQuery);
        List<T> listObj = q.getResultList();
        return listObj;
    }

    public void setNumQuery(int valor) {
        this.numQuery = valor;
    }

    public void setListNombres(List<String> listNombres) {
        this.listNombres = listNombres;
    }

    public List<String> getListNombres() {
        return listNombres;
    }

    public Map getMpParams() {
        return mpParams;
    }

    public void setMpParams(Map mpParams) {
        this.mpParams = mpParams;
    }
    
}

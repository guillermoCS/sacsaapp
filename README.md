*****************************************************************************
*
* LEEME para SacsaApp  
*
* Utima actualizacion: Diciembre 2015
*
* Esta apliacion es un pequeño ejemplo de lo que 
* se puede hacer con Java
*
* ADVERTENCIA: Esto es solamente un software educativo
*
*****************************************************************************
* Como instalar este proyecto?
* Por favor leer el siguiente documento para instalar(solo en Español)
* https://docs.google.com/document/d/1L3LXlzRM9lXSip3l7cRDsYyLqimyAJ7HFeVuc7CZsk4/pub
* 
* Para mayor informacion contactar con:
* Guillermo Castaneda: guillermo.castasz89@gmail.com
* Daniel Guevara: deguevarasv@gmail.com twitter @DanielE_Guevara
*
*****************************************************************************

*****************************************************************************
*
* README for SacsaApp  
*
* Last update: December 2015
*
* This application is a little example of what 
* can be done with the Java programming language
*
* WARNING: This is only an educational software
*
*****************************************************************************
* How to install this proyect?
* Please read this document for install (only in Spanish)
* https://docs.google.com/document/d/1L3LXlzRM9lXSip3l7cRDsYyLqimyAJ7HFeVuc7CZsk4/pub
* For more information please contact us at:
* Guillermo Castaneda: guillermo.castasz89@gmail.com
* Daniel Guevara: deguevarasv@gmail.com twitter @DanielE_Guevara
*
*****************************************************************************